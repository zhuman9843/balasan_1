"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_views_errors_index_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/errors/index.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/errors/index.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  components: {
    axios: (axios__WEBPACK_IMPORTED_MODULE_0___default())
  },
  data: function data() {
    return {
      problem: '',
      isSend: false
    };
  },
  methods: {
    back: function back() {
      this.$router.push('/login');
    },
    sendEmail: function sendEmail() {
      var _this = this;
      axios__WEBPACK_IMPORTED_MODULE_0___default().post(window.location.origin + '/send-problem', {
        problem: this.problem
      }).then(function (response) {
        _this.problem = '';
        _this.isSend = true;
      })["catch"](function (error) {
        console.log('error');
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9.use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/errors/index.vue?vue&type=style&index=1&id=38b93ead&lang=css&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9.use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/errors/index.vue?vue&type=style&index=1&id=38b93ead&lang=css& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
// Imports

var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.id, "\n.error_input {\n    border: 1px solid aliceblue;\n    border-radius: 10px;\n}\n.done_block {\n}\n", ""]);
// Exports
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9.use[2]!./resources/css/app.css?vue&type=style&index=0&id=38b93ead&scoped=true&lang=css&":
/*!*********************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9.use[2]!./resources/css/app.css?vue&type=style&index=0&id=38b93ead&scoped=true&lang=css& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../node_modules/css-loader/dist/runtime/getUrl.js */ "./node_modules/css-loader/dist/runtime/getUrl.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _public_images_middle_logo_svg__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../public/images/middle_logo.svg */ "./public/images/middle_logo.svg");
/* harmony import */ var _public_images_middle_logo_svg__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_public_images_middle_logo_svg__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _public_images_small_logo_svg__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../public/images/small_logo.svg */ "./public/images/small_logo.svg");
/* harmony import */ var _public_images_small_logo_svg__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_public_images_small_logo_svg__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _public_images_logo_mobile_svg__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../public/images/logo_mobile.svg */ "./public/images/logo_mobile.svg");
/* harmony import */ var _public_images_logo_mobile_svg__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_public_images_logo_mobile_svg__WEBPACK_IMPORTED_MODULE_4__);
// Imports





var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(function(i){return i[1]});
var ___CSS_LOADER_URL_REPLACEMENT_0___ = _node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_1___default()((_public_images_middle_logo_svg__WEBPACK_IMPORTED_MODULE_2___default()));
var ___CSS_LOADER_URL_REPLACEMENT_1___ = _node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_1___default()((_public_images_small_logo_svg__WEBPACK_IMPORTED_MODULE_3___default()));
var ___CSS_LOADER_URL_REPLACEMENT_2___ = _node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_1___default()((_public_images_logo_mobile_svg__WEBPACK_IMPORTED_MODULE_4___default()));
// Module
___CSS_LOADER_EXPORT___.push([module.id, "html[data-v-38b93ead] {\n    font-size: 100%;\n}\n.relative[data-v-38b93ead] {\n    position: relative;\n}\n.block[data-v-38b93ead] {\n    display: block !important;\n}\n.column[data-v-38b93ead] {\n    display: flex;\n    flex-direction: column;\n}\n.row[data-v-38b93ead] {\n    display: flex;\n    flex-direction: row;\n}\n.link[data-v-38b93ead]{\n    font-size: 1rem;\n    color: white;\n}\n.light_sm_text[data-v-38b93ead] {\n    color: #FFF;\n    font-size: 0.875rem;\n    font-weight: 400;\n}\n.light_xs_text[data-v-38b93ead] {\n    color: #FFF;\n    font-size: 0.8rem;\n    font-weight: 400;\n}\n.wrap[data-v-38b93ead] {\n    display: flex;\n    flex-wrap: wrap;\n}\n@media screen and (max-width: 1600px) {\n.teachers_mobile_popup_wrapper[data-v-38b93ead] {\n        width: 70vw !important;\n}\n.logo[data-v-38b93ead] {\n        margin-right: 0 !important;\n}\n}\n@media screen and (max-width: 1440px) {\nhtml[data-v-38b93ead] {\n        font-size: 95%;\n}\n.logo[data-v-38b93ead] {\n        width: 122px !important;\n        background-image: url(" + ___CSS_LOADER_URL_REPLACEMENT_0___ + ") !important;\n        margin-right: 15px !important;\n}\n.normal_text[data-v-38b93ead] {\n        font-size: 0.9rem !important;\n}\n.login_wrapper[data-v-38b93ead] {\n        /* height: 400px; */\n        padding: 40px 50px;\n}\n.login_logo[data-v-38b93ead] {\n        padding: 20px 0;\n}\n.main_teachers_photo[data-v-38b93ead] {\n        width: 30% !important;\n        height: 60px !important;\n        border-radius: 50%;\n}\n.main_attendance_graph[data-v-38b93ead] {\n        margin-right: 15px;\n}\n.main_attendance_reason_amount[data-v-38b93ead] {\n        font-size: 1.25rem !important;\n}\n.main_attendance_amount[data-v-38b93ead] {\n        font-size: 1.75rem !important;\n}\n.main_left[data-v-38b93ead] {\n        width: 35% !important;\n}\n.main_right[data-v-38b93ead] {\n        width: 64% !important;\n}\n.gray_text[data-v-38b93ead] {\n        font-size: 0.8rem !important;\n}\n.main_childs_status[data-v-38b93ead] {\n        padding: 5px !important;\n        margin: 2px !important;\n}\n    /* analytics media styles */\n.search_table_child[data-v-38b93ead] {\n        width: 180px !important;\n}\n.group_edit_block input[data-v-38b93ead], .group_edit_block select[data-v-38b93ead], .group_edit_block option[data-v-38b93ead], .main_childs_groups select[data-v-38b93ead] {\n        padding: 7px;\n}\nnav ul li[data-v-38b93ead] {\n        margin: 5px !important;\n}\n}\n@media screen and (max-width: 1280px) {\nhtml[data-v-38b93ead] {\n        font-size: 90%;\n}\n.logo[data-v-38b93ead] {\n        width: 35px !important;\n        background-image: url(" + ___CSS_LOADER_URL_REPLACEMENT_1___ + ") !important;\n        margin-right: 15px !important;\n}\n.icon_edit[data-v-38b93ead] {\n        width: 15px !important;\n        height: 15px !important;\n}\n.icon_plus[data-v-38b93ead] {\n        width: 15px !important;\n        height: 15px !important;\n}\n.icon_excel[data-v-38b93ead] {\n        width: 15px !important;\n        height: 15px !important;\n}\n    /* registration media styles */\n.register_main[data-v-38b93ead] {\n        padding-bottom: 5px;\n}\n.register_info[data-v-38b93ead] {\n        margin-top: 5px;\n}\n.register_info_block[data-v-38b93ead] {\n        margin: 5px 1.6666%;\n}\n.search-container input[data-v-38b93ead] {\n        padding: 5px 10px !important;\n}\ngoogle-map button[data-v-38b93ead] {\n        padding: 15px;\n}\n    /* profile media styles */\n.profile_organization_info[data-v-38b93ead] {\n        margin-bottom: 10px !important;\n}\n.profile_organization_header[data-v-38b93ead] {\n        padding: 12px 20px !important;\n}\n.profile_address[data-v-38b93ead] {\n        padding: 12px 20px !important;\n}\n.profile_map[data-v-38b93ead] {\n        height: 300px !important;\n}\n    /* childs media styles */\n.childs_import_excel[data-v-38b93ead], .childs_export_excel[data-v-38b93ead], .childs_add[data-v-38b93ead], .childs_edited[data-v-38b93ead] {\n        padding: 7px 14px !important;\n}\n.input_excel[data-v-38b93ead] {\n        font-size: 0.7rem;\n}\n    /* groups media style */\n.teachers_mobile_popup_wrapper[data-v-38b93ead] {\n        width: 80vw !important;\n}\n    /* teachers media styles */\n.teachers_button[data-v-38b93ead] {\n        padding: 7px 10px 7px 10px !important;\n}\n.teacher_group_block[data-v-38b93ead] {\n        height: 70px !important;\n        padding: 10px !important;\n        margin: 0 1% 0 0 !important;\n}\n.teachet_groups_title[data-v-38b93ead] {\n        padding: 10px 20px !important;\n}\n    /* main page media styles */\n.main_left[data-v-38b93ead] {\n        width: 50% !important;\n}\n.main_right[data-v-38b93ead] {\n        width: 50% !important;\n}\n.main_teachers_blocks[data-v-38b93ead] {\n        min-width: 49.5% !important;\n}\n.main_childs_filter[data-v-38b93ead] {\n        justify-content: flex-end !important;\n}\n.main_childs_groups[data-v-38b93ead] {\n        width: 40% !important;\n        padding: 0 !important;\n        margin-left: 10px;\n}\n.main_childs_groups select[data-v-38b93ead] {\n        padding: 5px !important;\n}\n.table tbody tr td[data-v-38b93ead] {\n        padding: 10px !important;\n}\n.search_table_child[data-v-38b93ead] {\n        width: 150px !important;\n}\n.missing_circle[data-v-38b93ead], .sick_circle[data-v-38b93ead], .wekeend_circle[data-v-38b93ead], .edited_missing_circle[data-v-38b93ead], .edited_sick_circle[data-v-38b93ead], .edited_wekeend_circle[data-v-38b93ead] {\n        margin: 0 3px !important;\n}\n.icon_excel[data-v-38b93ead] {\n        margin-right: 5px !important;\n}\n}\n@media screen and (max-width: 1024px) {\nhtml[data-v-38b93ead] {\n        font-size: 80%;\n}\n    /* login media styles */\n.login_wrapper[data-v-38b93ead] {\n        /* height: 300px; */\n        width: 350px;\n        padding: 30px;\n}\n.login_logo[data-v-38b93ead] {\n        height: 20px;\n        padding: 20px 0;\n}\n.login_input[data-v-38b93ead] {\n        padding: 5px;\n}\n.login_title[data-v-38b93ead] {\n        margin-top: 10px;\n}\n.reset_password_button[data-v-38b93ead] {\n        margin: 10px 0;\n}\n.username_icon[data-v-38b93ead], .password_icon[data-v-38b93ead] {\n        position: absolute;\n        right: 15px;\n        width: 15px;\n        height: 15px;\n}\n    /* navbar media styles */\n.side_navbar[data-v-38b93ead] {\n        display: block !important;\n}\n.navbar_icon[data-v-38b93ead] {\n        margin-right: 20px;\n        padding-right: 20px !important;\n        border-right: 1px solid #DDDDDD;\n}\n.side_logo[data-v-38b93ead] {\n        width: 100% !important;\n        height: 60px !important;\n        margin-right: 0 !important;\n}\n.logo[data-v-38b93ead] {\n        height: 60px !important;\n}\n.navbar-nav[data-v-38b93ead] {\n        flex-direction: row !important;\n}\n.navbar-nav .normal_text[data-v-38b93ead] {\n        display: none;\n}\n.navbar-nav a.icon[data-v-38b93ead] {\n        display: block;\n}\n.side_navbar_wrapper[data-v-38b93ead] {\n        display: block;\n}\n.side_navbar li[data-v-38b93ead] {\n        padding: 10px 30px !important;\n}\n.side_navbar li[data-v-38b93ead]:hover:not(:first-child){\n        cursor: pointer;\n        background-color: #2EA263;\n}\n.side_navbar li:hover:not(:first-child) a[data-v-38b93ead]{\n        color: white;\n        text-decoration: none;\n}\n    /* main page media styles */\n.main_page[data-v-38b93ead] {\n        flex-direction: column;\n}\n.main_right[data-v-38b93ead] {\n        width: 100% !important;\n}\n.main_left[data-v-38b93ead] {\n        width: 100% !important;\n        display: flex;\n        flex-direction: row !important;\n        flex-wrap: wrap;\n}\n.main_current_date[data-v-38b93ead] {\n        width: 40% !important;\n        margin-right: 1%;\n        margin-bottom: 0 !important;\n        height: 300px !important;\n}\n.main_attendance_wrapper[data-v-38b93ead] {\n        width: 59% !important;\n        height: 300px !important;\n}\n.main_childs_groups[data-v-38b93ead] {\n        width: 20% !important;\n}\n.main_groups_title[data-v-38b93ead] {\n        margin: 20px 0 !important;\n}\n.main_progressbar[data-v-38b93ead] {\n        width: 90% !important;\n}\n.main_teachers_blocks[data-v-38b93ead] {\n        min-width: 33% !important;\n}\n.main_teachers_photo[data-v-38b93ead] {\n        height: 80px !important;\n}\n.main_teachers_wrapper[data-v-38b93ead] {\n        width: 99% !important;\n}\n    /* profile styles media css */\n.profile_wrapper[data-v-38b93ead] {\n        display: flex;\n        flex-wrap: wrap;\n        margin: 0 1%;\n}\n.profile_block[data-v-38b93ead] {\n        width: 49% !important;\n}\n}\n@media screen and (max-width: 900px) {\n.register_info_block[data-v-38b93ead] {\n        width: 47%;\n        margin: 5px 1.5%;\n}\n.register_access[data-v-38b93ead] {\n        flex-direction: column;\n}\n}\n@media screen and (max-width: 768px) {\nhtml[data-v-38b93ead] {\n        font-size: 70%;\n}\n.register_wrapper[data-v-38b93ead] {\n        padding: 20px 50px;\n        top: 95%;\n}\n.side_logo[data-v-38b93ead] {\n        background-image: url(" + ___CSS_LOADER_URL_REPLACEMENT_2___ + ") !important;\n}\n.logo[data-v-38b93ead] {\n        width: 140px !important;\n        background-image: url(" + ___CSS_LOADER_URL_REPLACEMENT_2___ + ") !important;\n}\n.icon img[data-v-38b93ead]{\n        width: 30px !important;\n        height: 30px !important;\n}\n.navbar_icon[data-v-38b93ead] {\n        padding-right: 10px !important;\n        margin-right: 10px;\n}\n.img-profile[data-v-38b93ead] {\n        width: 40px !important;\n        height: 40px !important;\n}\n    /* main page media styles */\n.main_current_date[data-v-38b93ead] {\n        display: none !important;\n}\n.main_attendance_wrapper[data-v-38b93ead] {\n        width: 100% !important;\n        height: 275px !important;\n}\n.main_progressbar[data-v-38b93ead] {\n        width: 90% !important;\n}\n.main_attendance_graph[data-v-38b93ead] {\n        height: 250px !important;\n}\n.main_teachers_blocks[data-v-38b93ead] {\n        min-width: 49.5% !important;\n}\n    /* profile style */\n.profile_block[data-v-38b93ead] {\n        width: 98% !important;\n}\n.profile_error[data-v-38b93ead] {\n        padding: 20px !important;\n}\n}\n@media screen and (max-width: 600px) {\n.register_wrapper[data-v-38b93ead] {\n        padding: 20px 50px;\n        top:140%;\n}\n.register_info_block[data-v-38b93ead] {\n        width: 97%;\n}\n.register_info select[data-v-38b93ead] {\n        padding: 5px;\n}\n.register_access[data-v-38b93ead] {\n        margin-top: 50px;\n}\n.register_down[data-v-38b93ead] {\n        flex-direction: column;\n}\n.side_navbar[data-v-38b93ead] {\n        width: 40% !important;\n}\n.side_navbar li[data-v-38b93ead] {\n        padding: 5px 20px !important;\n}\n.main_childs_groups[data-v-38b93ead] {\n        width: 35% !important;\n}\n.forgot_wrapper[data-v-38b93ead] {\n        width: 80%;\n}\nnav ul li[data-v-38b93ead] {\n        margin: 0 !important;\n        padding: 0 !important;\n}\n.nav-link[data-v-38b93ead] {\n        padding: 0 !important;\n}\n.img-profile[data-v-38b93ead] {\n        margin-right: 0 !important;\n}\n}\n@media screen and (max-width: 414px) {\nhtml[data-v-38b93ead] {\n        /* font-size: 70%; */\n}\n.login_wrapper[data-v-38b93ead] {\n        height: -moz-max-content;\n        height: max-content;\n        width: 250px;\n        padding: 15px;\n}\n.login_logo[data-v-38b93ead] {\n        background-image: url(" + ___CSS_LOADER_URL_REPLACEMENT_2___ + ");\n        height: -moz-max-content;\n        height: max-content;\n        padding: 15px 0;\n}\n.login_input[data-v-38b93ead] {\n        padding: 5px 10px;\n}\n.login_title[data-v-38b93ead] {\n        margin-top: 10px;\n}\n.reset_password_button[data-v-38b93ead] {\n        margin: 10px 0 !important;\n}\n.register_access[data-v-38b93ead] {\n        margin-top: 60px;\n        text-align: center;\n}\n.search-container[data-v-38b93ead] {\n        flex-direction: column;\n}\n.search-container button[data-v-38b93ead] {\n        width: 100% !important;\n        border-top-right-radius: 0px !important;\n}\n.large_light_text[data-v-38b93ead] {\n        font-size: 1.8rem;\n}\n.username_icon[data-v-38b93ead], .password_icon[data-v-38b93ead] {\n      position: absolute;\n      right: 15px;\n      width: 10px;\n      height: 10px;\n}\n.side_navbar[data-v-38b93ead] {\n        width: 50% !important;\n}\n.side_navbar li[data-v-38b93ead] {\n        padding: 5px !important;\n}\n.main_teachers_blocks[data-v-38b93ead] {\n        min-width: 99% !important;\n}\n.main_attendance_wrapper[data-v-38b93ead] {\n        flex-direction: column;\n}\n.main_attendance_wrapper[data-v-38b93ead] {\n        height: -moz-max-content !important;\n        height: max-content !important;\n}\n.main_attendance_tables[data-v-38b93ead] {\n        width: 100% !important;\n}\n.main_attendance_graph[data-v-38b93ead] {\n        width: 100% !important;\n        height: 275px !important;\n}\n.main_progressbar[data-v-38b93ead] {\n        width: 85% !important;\n}\n.childs_search[data-v-38b93ead] {\n        width: 100px !important;\n        margin-right: 10px;\n}\n.table thead th[data-v-38b93ead] {\n        padding: 5px !important;\n}\n.table tbody tr td[data-v-38b93ead] {\n        padding: 5px !important;\n}\n.profile_settings[data-v-38b93ead] {\n        margin-top: 30px;\n}\n}\n@media screen and (max-height: 900px) {\n.register_wrapper[data-v-38b93ead]{\n        height: -moz-max-content;\n        height: max-content;\n}\n}", ""]);
// Exports
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9.use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/errors/index.vue?vue&type=style&index=1&id=38b93ead&lang=css&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9.use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/errors/index.vue?vue&type=style&index=1&id=38b93ead&lang=css& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_cjs_js_clonedRuleSet_9_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_9_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_1_id_38b93ead_lang_css___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! !!../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-9.use[1]!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9.use[2]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=style&index=1&id=38b93ead&lang=css& */ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9.use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/errors/index.vue?vue&type=style&index=1&id=38b93ead&lang=css&");

            

var options = {};

options.insert = "head";
options.singleton = false;

var update = _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default()(_node_modules_css_loader_dist_cjs_js_clonedRuleSet_9_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_9_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_1_id_38b93ead_lang_css___WEBPACK_IMPORTED_MODULE_1__["default"], options);



/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_css_loader_dist_cjs_js_clonedRuleSet_9_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_9_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_1_id_38b93ead_lang_css___WEBPACK_IMPORTED_MODULE_1__["default"].locals || {});

/***/ }),

/***/ "./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9.use[2]!./resources/css/app.css?vue&type=style&index=0&id=38b93ead&scoped=true&lang=css&":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9.use[2]!./resources/css/app.css?vue&type=style&index=0&id=38b93ead&scoped=true&lang=css& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_cjs_js_clonedRuleSet_9_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_9_use_2_app_css_vue_type_style_index_0_id_38b93ead_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! !!../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-9.use[1]!../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9.use[2]!./app.css?vue&type=style&index=0&id=38b93ead&scoped=true&lang=css& */ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9.use[2]!./resources/css/app.css?vue&type=style&index=0&id=38b93ead&scoped=true&lang=css&");

            

var options = {};

options.insert = "head";
options.singleton = false;

var update = _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default()(_node_modules_css_loader_dist_cjs_js_clonedRuleSet_9_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_9_use_2_app_css_vue_type_style_index_0_id_38b93ead_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_1__["default"], options);



/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_css_loader_dist_cjs_js_clonedRuleSet_9_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_9_use_2_app_css_vue_type_style_index_0_id_38b93ead_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_1__["default"].locals || {});

/***/ }),

/***/ "./resources/js/views/errors/index.vue":
/*!*********************************************!*\
  !*** ./resources/js/views/errors/index.vue ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _index_vue_vue_type_template_id_38b93ead_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index.vue?vue&type=template&id=38b93ead&scoped=true& */ "./resources/js/views/errors/index.vue?vue&type=template&id=38b93ead&scoped=true&");
/* harmony import */ var _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./index.vue?vue&type=script&lang=js& */ "./resources/js/views/errors/index.vue?vue&type=script&lang=js&");
/* harmony import */ var _css_app_css_vue_type_style_index_0_id_38b93ead_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../../css/app.css?vue&type=style&index=0&id=38b93ead&scoped=true&lang=css& */ "./resources/css/app.css?vue&type=style&index=0&id=38b93ead&scoped=true&lang=css&");
/* harmony import */ var _index_vue_vue_type_style_index_1_id_38b93ead_lang_css___WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./index.vue?vue&type=style&index=1&id=38b93ead&lang=css& */ "./resources/js/views/errors/index.vue?vue&type=style&index=1&id=38b93ead&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");



;



/* normalize component */

var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_4__["default"])(
  _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _index_vue_vue_type_template_id_38b93ead_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render,
  _index_vue_vue_type_template_id_38b93ead_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  "38b93ead",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/errors/index.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/views/errors/index.vue?vue&type=script&lang=js&":
/*!**********************************************************************!*\
  !*** ./resources/js/views/errors/index.vue?vue&type=script&lang=js& ***!
  \**********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/errors/index.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/css/app.css?vue&type=style&index=0&id=38b93ead&scoped=true&lang=css&":
/*!****************************************************************************************!*\
  !*** ./resources/css/app.css?vue&type=style&index=0&id=38b93ead&scoped=true&lang=css& ***!
  \****************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_dist_cjs_js_node_modules_css_loader_dist_cjs_js_clonedRuleSet_9_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_9_use_2_app_css_vue_type_style_index_0_id_38b93ead_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../node_modules/style-loader/dist/cjs.js!../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-9.use[1]!../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9.use[2]!./app.css?vue&type=style&index=0&id=38b93ead&scoped=true&lang=css& */ "./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9.use[2]!./resources/css/app.css?vue&type=style&index=0&id=38b93ead&scoped=true&lang=css&");


/***/ }),

/***/ "./resources/js/views/errors/index.vue?vue&type=style&index=1&id=38b93ead&lang=css&":
/*!******************************************************************************************!*\
  !*** ./resources/js/views/errors/index.vue?vue&type=style&index=1&id=38b93ead&lang=css& ***!
  \******************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_dist_cjs_js_node_modules_css_loader_dist_cjs_js_clonedRuleSet_9_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_9_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_1_id_38b93ead_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader/dist/cjs.js!../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-9.use[1]!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9.use[2]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=style&index=1&id=38b93ead&lang=css& */ "./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9.use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/errors/index.vue?vue&type=style&index=1&id=38b93ead&lang=css&");


/***/ }),

/***/ "./resources/js/views/errors/index.vue?vue&type=template&id=38b93ead&scoped=true&":
/*!****************************************************************************************!*\
  !*** ./resources/js/views/errors/index.vue?vue&type=template&id=38b93ead&scoped=true& ***!
  \****************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_38b93ead_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   staticRenderFns: () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_38b93ead_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_38b93ead_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=template&id=38b93ead&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/errors/index.vue?vue&type=template&id=38b93ead&scoped=true&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/errors/index.vue?vue&type=template&id=38b93ead&scoped=true&":
/*!*******************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/errors/index.vue?vue&type=template&id=38b93ead&scoped=true& ***!
  \*******************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: () => (/* binding */ render),
/* harmony export */   staticRenderFns: () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "login_bg" }, [
    _c("div", { staticClass: "login_wrapper" }, [
      _c("div", { staticClass: "large_light_text mb-3" }, [
        _vm._v(" Сообщить об ошибке")
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "w-100 h-100 " }, [
        _c("textarea", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.problem,
              expression: "problem"
            }
          ],
          staticClass: "w-100 h-100 error_input p-3",
          attrs: { id: "message" },
          domProps: { value: _vm.problem },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.problem = $event.target.value
            }
          }
        }),
        _vm._v(" "),
        _vm.isSend
          ? _c(
              "p",
              {
                staticClass: "done_block",
                staticStyle: { width: "100% !important" }
              },
              [_vm._v(" Отправлено ")]
            )
          : _vm._e()
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "mt-2 flex justify-content-center" }, [
        _c(
          "button",
          {
            staticClass: "teachers_button teacher_cancel mr-2",
            on: { click: _vm.back }
          },
          [_vm._v(" Вернуться назад")]
        ),
        _vm._v(" "),
        _c(
          "button",
          {
            staticClass: "teachers_button",
            attrs: { type: "submit" },
            on: { click: _vm.sendEmail }
          },
          [_vm._v(" Отправить ")]
        )
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);