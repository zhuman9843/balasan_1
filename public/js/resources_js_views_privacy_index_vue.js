"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_views_privacy_index_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/privacy/index.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/privacy/index.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: "Privacy",
  components: {},
  data: function data() {
    return {};
  },
  computed: {},
  methods: {},
  mounted: function mounted() {}
});

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9.use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/privacy/index.vue?vue&type=style&index=0&id=dc56e8c8&lang=css&":
/*!***********************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9.use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/privacy/index.vue?vue&type=style&index=0&id=dc56e8c8&lang=css& ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
// Imports

var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.id, "\n.privacy {\n    border-radius: 10px;\n    background: #FFF;\n    box-shadow: 0px 1px 8px -1px rgba(0, 0, 0, 0.20);\n    margin: 50px;\n    padding: 50px;\n    line-height: 24px;\n}\n", ""]);
// Exports
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9.use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/privacy/index.vue?vue&type=style&index=0&id=dc56e8c8&lang=css&":
/*!***************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9.use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/privacy/index.vue?vue&type=style&index=0&id=dc56e8c8&lang=css& ***!
  \***************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_cjs_js_clonedRuleSet_9_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_9_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_id_dc56e8c8_lang_css___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! !!../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-9.use[1]!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9.use[2]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=style&index=0&id=dc56e8c8&lang=css& */ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9.use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/privacy/index.vue?vue&type=style&index=0&id=dc56e8c8&lang=css&");

            

var options = {};

options.insert = "head";
options.singleton = false;

var update = _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default()(_node_modules_css_loader_dist_cjs_js_clonedRuleSet_9_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_9_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_id_dc56e8c8_lang_css___WEBPACK_IMPORTED_MODULE_1__["default"], options);



/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_css_loader_dist_cjs_js_clonedRuleSet_9_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_9_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_id_dc56e8c8_lang_css___WEBPACK_IMPORTED_MODULE_1__["default"].locals || {});

/***/ }),

/***/ "./resources/js/views/privacy/index.vue":
/*!**********************************************!*\
  !*** ./resources/js/views/privacy/index.vue ***!
  \**********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _index_vue_vue_type_template_id_dc56e8c8___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index.vue?vue&type=template&id=dc56e8c8& */ "./resources/js/views/privacy/index.vue?vue&type=template&id=dc56e8c8&");
/* harmony import */ var _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./index.vue?vue&type=script&lang=js& */ "./resources/js/views/privacy/index.vue?vue&type=script&lang=js&");
/* harmony import */ var _index_vue_vue_type_style_index_0_id_dc56e8c8_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./index.vue?vue&type=style&index=0&id=dc56e8c8&lang=css& */ "./resources/js/views/privacy/index.vue?vue&type=style&index=0&id=dc56e8c8&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");



;


/* normalize component */

var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _index_vue_vue_type_template_id_dc56e8c8___WEBPACK_IMPORTED_MODULE_0__.render,
  _index_vue_vue_type_template_id_dc56e8c8___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/privacy/index.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/views/privacy/index.vue?vue&type=script&lang=js&":
/*!***********************************************************************!*\
  !*** ./resources/js/views/privacy/index.vue?vue&type=script&lang=js& ***!
  \***********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/privacy/index.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/privacy/index.vue?vue&type=style&index=0&id=dc56e8c8&lang=css&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/views/privacy/index.vue?vue&type=style&index=0&id=dc56e8c8&lang=css& ***!
  \*******************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_dist_cjs_js_node_modules_css_loader_dist_cjs_js_clonedRuleSet_9_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_9_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_id_dc56e8c8_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader/dist/cjs.js!../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-9.use[1]!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9.use[2]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=style&index=0&id=dc56e8c8&lang=css& */ "./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-9.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-9.use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/privacy/index.vue?vue&type=style&index=0&id=dc56e8c8&lang=css&");


/***/ }),

/***/ "./resources/js/views/privacy/index.vue?vue&type=template&id=dc56e8c8&":
/*!*****************************************************************************!*\
  !*** ./resources/js/views/privacy/index.vue?vue&type=template&id=dc56e8c8& ***!
  \*****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_dc56e8c8___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   staticRenderFns: () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_dc56e8c8___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_dc56e8c8___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=template&id=dc56e8c8& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/privacy/index.vue?vue&type=template&id=dc56e8c8&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/privacy/index.vue?vue&type=template&id=dc56e8c8&":
/*!********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/privacy/index.vue?vue&type=template&id=dc56e8c8& ***!
  \********************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: () => (/* binding */ render),
/* harmony export */   staticRenderFns: () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", [
      _c("div", { staticClass: "privacy" }, [
        _c("h1", { staticClass: "text-align-center" }, [
          _vm._v(" Политика конфиденциальности ")
        ]),
        _vm._v(" "),
        _c("h6", { staticClass: "ml-5" }, [
          _vm._v("Дата вступления в силу: 27.09.2023 г ")
        ]),
        _vm._v(" "),
        _c("div", { staticStyle: { "text-align": "justify" } }, [
          _c("span", { staticClass: "ml-5" }),
          _vm._v(
            ' Добро пожаловать в приложение "Balasan" (далее "Приложение"), разработанное ТОО "Balasan" (далее -Компания). Мы ценим ваше доверие и стремимся обеспечить максимальную конфиденциальность и безопасность ваших данных. Ниже мы разъясним, как мы собираем, используем и защищаем  личные данные лиц, пользующихся услугами Приложения (далее – Пользователи).\n          Целью настоящей Политики является обеспечение надлежащей защиты информации о Пользователях, в том числе их персональных данных, от несанкционированного доступа и разглашения.\n          Отношения, связанные со сбором, хранением, распространением и защитой информации о Пользователях Приложения, регулируются настоящей Политикой, иными официальными документами Компании и действующим законодательством Республики Казахстан.\n      '
          )
        ]),
        _vm._v(" "),
        _c("br"),
        _vm._v(" "),
        _c("h4", { staticClass: "text-align-center" }, [
          _vm._v("Сбор и использование информации")
        ]),
        _vm._v(" "),
        _c("div", { staticStyle: { "text-align": "justify" } }, [
          _vm._v(
            "\n          1. Геолокация: Наше приложение запрашивает доступ к геолокации устройства пользователя с целью проверки находиться ли пользователь в определенной локации для разблокировки функций. Геолокационные данные используются исключительно для проверки позиции относительно садика или секции в данный момент и не передаются третьим лицам.\n          "
          ),
          _c("br"),
          _vm._v(
            "\n          2. Отметка присутствия через сканирование лица: Приложение использует технологию сканирования лица для идентификации человека. Эти данные используются исключительно для целей учета присутствия посещения и не передаются третьим лицам.\n          "
          ),
          _c("br"),
          _vm._v(
            "\n          3. Управление отсутствиями: Если человек отсутствует в группе, сотрудник может указать причину отсутствия. Эта информация также используется только в целях учета отсутствий внутри организации, уполномоченными сотрудниками организации, и не разглашается.\n          "
          ),
          _c("br"),
          _vm._v(
            "\n          4. Безопасность и конфиденциальность: Мы применяем современные меры безопасности для защиты ваших данных от несанкционированного доступа. Данные о табелируемых людях хранятся в зашифрованной форме и доступна только уполномоченным сотрудникам.\n          "
          ),
          _c("br"),
          _vm._v(
            "\n          5. Регистрация Пользователя в Приложении доступна только на стороне учетной системы организации, чьи сотрудники в дальнейшем являются пользователями Приложения.\n          "
          ),
          _c("br"),
          _vm._v(
            "\n          6. Персональные данные о табелируемых сотрудниках доступны в Приложении уполномоченным к табелириованию пользователям, только после предоставления этих данных из учетной системы организации. Результаты табелирования из Приложения в дальнейшем доступны в учетной системе организации.\n          "
          ),
          _c("br"),
          _vm._v(
            "\n          7. Персональные данные Пользователей  и табелируемых сотрудников, включают в себя: имя, фамилия, отчество и фотография.\n          "
          ),
          _c("br"),
          _vm._v(
            "\n          8. Обработка персональных данных осуществляется на основе принципов: "
          ),
          _c("br"),
          _vm._v(" "),
          _c("span", { staticClass: "ml-5" }),
          _vm._v(
            " а) законности целей и способов обработки персональных данных; "
          ),
          _c("br"),
          _vm._v(" "),
          _c("span", { staticClass: "ml-5" }),
          _vm._v(" б) добросовестности; "),
          _c("br"),
          _vm._v(" "),
          _c("span", { staticClass: "ml-5" }),
          _vm._v(
            " в) соответствия целей обработки персональных данных целям, заранее определенным в договоре организации и Компании; "
          ),
          _c("br"),
          _vm._v(" "),
          _c("span", { staticClass: "ml-5" }),
          _vm._v(
            " г) соответствия объема и характера обрабатываемых персональных данных, способов обработки персональных данных целям обработки персональных данных. "
          ),
          _c("br"),
          _vm._v(
            "\n          9. Персональные данные Пользователей и табелируемых сотрудников не передаются никаким другим третьим лицам, за исключением случаев, прямо предусмотренных настоящей Политикой.\n      "
          )
        ]),
        _vm._v(" "),
        _c("h4", { staticClass: "text-align-center mt-4" }, [
          _vm._v(" Обратная связь и обращение к нам ")
        ]),
        _vm._v(" "),
        _c("div", { staticStyle: { "text-align": "justify" } }, [
          _c("span", { staticClass: "ml-5" }),
          _vm._v(
            " Eсли у вас есть вопросы или замечания относительно нашей политики конфиденциальности или обработки данных, пожалуйста, свяжитесь с нами по адресу arlanv.tech@gmail.com\n      "
          )
        ]),
        _vm._v(" "),
        _c("h4", { staticClass: "text-align-center mt-4" }, [
          _vm._v(" Изменения в политике конфиденциальности ")
        ]),
        _vm._v(" "),
        _c("div", { staticStyle: { "text-align": "justify" } }, [
          _c("span", { staticClass: "ml-5" }),
          _vm._v(
            " Мы оставляем за собой право вносить изменения в нашу политику конфиденциальности. В случае существенных изменений, мы уведомим вас об этом через приложение или другими доступными средствами.\n      "
          )
        ]),
        _vm._v(" "),
        _c("h6", { staticClass: "text-align-center mt-4" }, [
          _vm._v(
            "Спасибо за использование нашего приложения. Мы ценим ваше доверие и делаем все возможное, чтобы обеспечить безопасность и конфиденциальность ваших данных."
          )
        ]),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "mt-4", staticStyle: { "text-align": "left" } },
          [
            _vm._v('\n          ТОО "Balasan" '),
            _c("br"),
            _vm._v(
              "\n          город Астана, улица Кайым Мухамедханова 17, офис 172. "
            ),
            _c("br"),
            _vm._v("\n          arlanv.tech@gmail.com "),
            _c("br")
          ]
        )
      ])
    ])
  }
]
render._withStripped = true



/***/ })

}]);