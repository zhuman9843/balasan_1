<?php

namespace App\Console\Commands;

use App\Models\Childs;
use App\Models\ChildStatus;
use Carbon\Carbon;
use Illuminate\Console\Command;

class SendChildsStatusAfterEndDate extends Command
{
    private $currentDate;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send-child-status-after-end-date';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->currentDate = Carbon::now()->format('Y-m-d');
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $childsWithStatus3 = ChildStatus::where('date', $this->currentDate)->where('status_send', 3)->get()->map(function($child) {
            $child->status_text = "Отсутствует без причины";
            $child->status_send = 6;
            $child->save();
            return $child;
        });

        $childsWithStatus = ChildStatus::where('date', $this->currentDate)->where('status_send', '!=', 6)->pluck('id_child')->toArray();

        $childs = Childs::whereNotIn('id', $childsWithStatus)->pluck('id_group', 'id');

        foreach ( $childs as $childId => $idGroup ) {
            $childStatus = new ChildStatus();
            $childStatus->id_child = $childId;
            $childStatus->id_teacher = null;
            $childStatus->id_group = $idGroup;
            $childStatus->status_text = "Отсутствует без причины";
            $childStatus->status_send = 6;
            $childStatus->date = $this->currentDate;
            $childStatus->save();
        }
    }
}
