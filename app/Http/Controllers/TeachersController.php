<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Teachers;
use App\Models\Groups;
use App\Models\Childs;
use App\Models\User;
use App\Models\Organizations;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\TeachersImport;

class TeachersController extends Controller
{
    private $teachers;
    private $groups;
    private $childs;

    public function __construct()
    {
        $this->teachers = Teachers::all();
        $this->groups = Groups::all();
        $this->childs = Childs::all();
    }

    public function index() 
    {
        return view('teachers.index');
    }

    public function getTeachers() {
        try {
            $user = auth()->user();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
        if (!$user) {
            return response()->json(['error' => 'No user found!']);
        }

        $orgId = Organizations::where('bin', $user->email)->pluck('id')->first();
        $teachers = $this->teachers->where('id_org', $orgId);
        $newTeachers = [];

        foreach ($teachers as $teacher) {
            $newTeacher = $teacher;
            $groupIds = json_decode($teacher->id_group, true);
            $groups = [];
            
            if ($groupIds) {
                foreach ($groupIds as $groupId) {
                    $groupName = $this->groups->where('id', $groupId)->pluck('name')->first();
                    $childsAmount = $this->childs->where('id_group', $groupId)->count();
                    $groups[$groupId] = [
                        'childs_amount' => $childsAmount,
                        'group_name' => $groupName
                    ];
                }
            } else {
                $groups = [];
            }
            $newTeacher->group_names = $groups;
            $newTeachers[] = $newTeacher;
        }
        return $newTeachers;
    }
    
    public function getOtherTeachers(Request $request){
        try {
            $user = auth()->user();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
        
        $orgId = Organizations::where('bin', $user->email)->pluck('id')->first();
        
        if ( $request->input('teachers') ) {
            $groupTeacherIds = explode(",", $request->input('teachers'));
            $teachers = $this->teachers->where('id_org', $orgId)->whereNotIn('id', $groupTeacherIds);
        } else {
            $teachers = $this->teachers->where('id_org', $orgId);
        }

        return $teachers;
    }

    public function update(Request $request, $id)
    {
        try {
            $user = auth()->user();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        $request->validate([
            'name' => 'required|string|max:255',
            'iin' => 'required|numeric|min:12',
            'telephone' => 'required|numeric|min:11',
            'email' => 'required|email|max:255',
            'birth' => 'required',
        ]);

        $orgId = Organizations::where('bin', $user->email)->pluck('id')->first();
        $person = Teachers::findOrFail($id);
        
        $checkExistingTeacherId = Teachers::where('iin', $request->iin)->where('id_org', $orgId)->pluck('id')->first();

        if ($checkExistingTeacherId && $checkExistingTeacherId != $id) {
            return response()->json(['errors' => 'ИИН уже зарегистрирован!'], 500);
        }

        $user = User::where('email', $person->iin)->first();

        $person->update([
            'name' => $request->input('name'),
            'telephone' => $request->input('telephone'),
            'email' => $request->input('email'),
            'iin' => $request->input('iin'),
            'birth_date' => $request->input('birth'),
        ]);

        $user->email = $request->iin;
        $user->save();

        return response()->json(['message' => 'Teacher info updated successfully.']);
    }

    public function delete($id) {
        try {
            $person = Teachers::findOrFail($id);

            User::where('email', $person->iin)->delete();
            Teachers::where('id', $id)->delete();

            return response()->json(['message' => 'Teacher have been deleted successfully.']);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return $e->getMessage();
        }
    }

    public function addGroupToTeacher(Request $request, $id) 
    {
        $request->validate([
            'id_group' => 'required|numeric',
        ]);

        $teacher = Teachers::findOrFail($id);
        
        $teacherGroupIds = json_decode($teacher->id_group, true);
        $teacherGroupIds[] = $request->input('id_group');

        $teacher->id_group  = json_encode($teacherGroupIds, true);
        $teacher->save();

        return response()->json(['message' => 'Teacher group list updated successfully.']);
    }

    public function deleteGroupToTeacher(Request $request, $id) 
    {
        $request->validate([
            'id_group' => 'required|numeric',
        ]);

        $teacher = Teachers::findOrFail($id);
        
        $teacherGroupIds = json_decode($teacher->id_group, true);
        $teacherGroupIds = array_diff($teacherGroupIds, [$request->input('id_group')]);
        
        $teacher->id_group  = json_encode($teacherGroupIds);
        $teacher->save();

        return response()->json(['message' => 'Teacher group list updated successfully.']);
    }
    
    public function import(Request $request)
    {
        try {
            $user = auth()->user();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        $orgId = Organizations::where('bin', $user->email)->pluck('id')->first();

        $file = $request->file('file');

        try {
            Excel::import(new TeachersImport($orgId), $file);
        } catch (\Exception $ex) {
            return "Таблица эксель заполнена не по формату";
        }

        return response()->json(['status' => 'success'], 200);
    }

    public function changePassword(Request $request) {
        try {
            $user = auth()->user();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        $user = User::where('email', $request->iin)->first();
        $user->password = bcrypt($request->password);
        $user->save();

        return response()->json(['status' => 'success'], 200);
    }
}
