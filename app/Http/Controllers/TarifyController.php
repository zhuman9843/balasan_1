<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\TarifyImport;

class TarifyController extends Controller
{
    public function import( Request $request ) {

        $file = $request->file('file');

        Excel::import(new TarifyImport(), $file);

        return response()->json(['status' => 'success'], 200);
    }
}
