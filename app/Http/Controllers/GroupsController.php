<?php

namespace App\Http\Controllers;

use App\Models\Groups;
use App\Models\Teachers;
use App\Models\Childs;
use App\Models\ChildToDay;
use App\Models\ChildStatus;
use App\Models\Organizations;
use App\Models\TypeOfGroup;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\GroupsImport;
use App\Models\GroupsAgeRanges;

class GroupsController extends Controller
{
    private $id;
    private $teachers;
    private $groups;
    private $groupsAgeRange;
    private $childs;
    private $groupId;
    private $currentDate;

    public function __construct()
    {
        $this->currentDate = Carbon::now()->format('Y-m-d');
        $this->teachers = Teachers::all();
        $this->groups = Groups::all();
        $this->groupsAgeRange = GroupsAgeRanges::all();
        $this->childs = Childs::all();
    }
    
    public function index() 
    {
        return view('groups.index');
    }

    public function getGroupsToDashboard(Request $request) {
        $bin = $request->input('bin');

        $orgId = Organizations::where('bin', $bin)->pluck('id')->toArray();
        $groups = Groups::where('id_org', $orgId)->get(['id', 'name'])->toArray();

        return $groups;
    }

    public function getGroups(): Array
    {   
        try {
            $user = auth()->user();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
        
        $teachersGroupIds = $this->teachers->where('iin', $user->email)->pluck('id_group')->first();
        $groupIds = json_decode($teachersGroupIds, true);

        $groups = [];
        foreach ( $groupIds as $groupId) {
            $group = $this->groups->where('id', $groupId)->first()->toArray();
            
            if ( $group['location'] ) {
                $location = $group['location'];
                list($latitude, $longitude) = explode(', ', $location);
                $group['location'] = [
                    'latitude' => round($latitude, 6),
                    'longitude' => round($longitude, 6)
                ];
            }
            
            $childsStatus = ChildStatus::where('id_group', $groupId)->where('date', $this->currentDate)->where('status_send',"!=", 3)->get()->count();
            $childsByGroup = $this->childs->where('id_group', $groupId)->values()->count();
            $group['type_of_group'] = $this->getTypeOfGroup($group['type_group']);
            $group['childs_amount'] = $childsByGroup;
            $group['age'] = $this->groupsAgeRange->where('id', $group['age'])->pluck('age')->first();
            $group['childs_with_status'] = $childsStatus;
            $groups[] = $group;
        }

        return $groups;
    }

    public function getOtherGroups(Request $request) 
    {
        try {
            $user = auth()->user();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
        
        $teacherGroups = Teachers::findOrFail($request->input('id_teacher'))->id_group;
        $orgId = Organizations::where('bin', $user->email)->pluck('id')->first();
        $groups = $this->groups->where('id_org', $orgId)->pluck('id')->toArray();

        if ( gettype(json_decode($teacherGroups)) == "array" ) {
            $teacherOtherGroupsId = array_diff($groups, json_decode($teacherGroups));
        } else if ( gettype(json_decode($teacherGroups)) == "object" ) {
            $teacherOtherGroupsId = array_diff($groups, collect(json_decode($teacherGroups))->values()->all());
        } else {
            $teacherOtherGroupsId = $groups;
        }
        $teacherOtherGroups = $this->groups->whereIn('id', $teacherOtherGroupsId);
        $otherGroups = [];

        foreach ($teacherOtherGroups as $group) {
            $this->groupId = $group->id;
            
            $teacher = $this->teachers->filter(function ($teacher) {               
                $teachersGroupIds = json_decode($teacher->id_group, true);
                if ( $teachersGroupIds && in_array($this->groupId, $teachersGroupIds)) {
                    return $teacher;
                } 
            });
            
            if ($teacher) {
                $teacherNames = $teacher->pluck('name','id')->toArray();
                $teacherPhone = $teacher->pluck('telephone', 'id')->toArray();
            } else {
                $teacherNames = 'Нет воспитателя';
                $teacherPhone = 'Нет номера';
            }
            
            $childsAmount = $this->childs->where('id_group', $this->groupId)->count();
            $newGroup = $group;

            $newGroup->type_group = $this->getTypeOfGroup($group->type_group);
            $newGroup->teacher_names = $teacherNames;
            $newGroup->teacher_phone = $teacherPhone;
            $newGroup->childs_amount = $childsAmount;
            $otherGroups[] = $newGroup;
        }
        return $otherGroups;
    }

    public function update( Request $request, $id ) {
        $request->validate([
            'name' => 'required|string|max:255',
            'type_group' => 'required|string|max:12',
            'count_place' => 'required|integer',
            'age' => 'required|integer|max:100'
        ]);

        $group = Groups::findOrFail($id);

        $group->update([
            'name' => $request->input('name'),
            'type_group' => $request->input('type_group'),
            'count_place' => $request->input('count_place'),
            'age' => $request->input('age')
        ]);

        return response()->json(['message' => 'Group data updated successfully.']);
    }

    public function delete(Request $request, $id) {
        try {
            $user = auth()->user();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
        
        $orgId = Organizations::where('bin', $user->email)->pluck('id')->first();

        if ( $request->input('teachers') ) {
            $groupTeacherIds = $request->input('teachers');
            $teachers = $this->teachers->where('id_org', $orgId)->whereIn('id', $groupTeacherIds);
            
            foreach ( $teachers as $teacher ) {
                $teacherGroupIds = json_decode($teacher->id_group, true);
                $teacherGroupIds = array_diff($teacherGroupIds, [$id]);
                
                $teacher->id_group  = json_encode($teacherGroupIds);
                $teacher->save();
            }
        }

        ChildToDay::where('id_group', $id)->delete();
        ChildStatus::where('id_group', $id)->delete();
        Childs::where('id_group', $id)->delete();
        // Childs::where('id_group', $id)->get()->map(function($child) {
        //     $child->id_group = null;
        //     $child->save();
        //     return $child;
        // });

        // ChildToDay::where('id_group', $id)->get()->map( function($childToDay) {
        //     $childToDay->id_group = null;
        //     $childToDay->save();
        //     return $childToDay;
        // });

        Groups::where('id', $id)->delete();
    }

    public function createGroup(Request $request) {
        try {
            $user = auth()->user();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
        
        $orgId = Organizations::where('bin', $user->email)->pluck('id')->first();
        $time = Carbon::now();
        
        $group = new Groups();

        $group->name = $request->name;
        $group->id_org = $orgId;
        $group->count_place = $request->count_place;
        $group->type_group = $request->type_group;
        $group->age = $request->age;
        $group->location = '';
        $group->created_at = $time;
        $group->updated_at = $time;
        $group->save();

        return response()->json(['status' => 'success'], 200);
    }

    public function deleteTeacher(Request $request, $id) {

        $request->validate([
            'teacher_id' => 'required|numeric',
        ]);

        $teacher = Teachers::findOrFail($request->input('teacher_id'));
        
        $teacherGroups = json_decode($teacher->id_group, true);
        $teacherGroups = array_diff($teacherGroups, [$id]);

        $teacher->id_group = json_encode($teacherGroups);
        $teacher->save();

        return response()->json(['message' => 'Group teacher updated successfully.']);
    }

    public function addTeacher(Request $request, $id) {

        $request->validate([
            'teacher_id' => 'required|numeric',
        ]);

        $teacher = Teachers::findOrFail($request->input('teacher_id'));
        
        $teacherGroups = json_decode($teacher->id_group, true);
        $teacherGroups[] = $id;

        $teacher->id_group = json_encode($teacherGroups);
        $teacher->save();

        return response()->json(['message' => 'Group teacher updated successfully.']);
    }

    public function getTypeOfGroup($category) {
        $typeOfGroup = TypeOfGroup::where('category', $category)->pluck('type_of_group')->first();
        return $typeOfGroup;
    }

    public function getOrgGroups() {
        try {
            $user = auth()->user();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
        
        $orgId = Organizations::where('bin', $user->email)->pluck('id')->first();
        $groups = $this->groups->where('id_org', $orgId);

        $result = [];

        foreach ($groups as $group) {
            $this->groupId = $group->id;
            
            $teacher = $this->teachers->filter(function ($teacher) {
                $teachersGroupIds = json_decode($teacher->id_group, true);
                if ($teachersGroupIds == null) $teachersGroupIds = [];

                if ( in_array($this->groupId, $teachersGroupIds)) {
                    return $teacher;
                } 
            });
            
            if ($teacher) {
                $teacherNames = $teacher->pluck('name','id')->toArray();
                $teacherPhone = $teacher->pluck('telephone', 'id')->toArray();
            } else {
                $teacherNames = 'Нет воспитателя';
                $teacherPhone = 'Нет номера';
            }
            
            $childsAmount = $this->childs->where('id_group', $this->groupId)->count();
            $childsStatus = ChildStatus::where('id_group', $group['id'])->where('date', $this->currentDate)->where('status_send',"!=", 3)->get()->count();

            $newGroup = $group;

            $newGroup->type_group = $this->getTypeOfGroup($group->type_group);
            $newGroup->teacher_names = $teacherNames;
            $newGroup->teacher_phone = $teacherPhone;
            $newGroup->childs_amount = $childsAmount;
            $newGroup->age = $this->groupsAgeRange->where('id', $group->age)->pluck('age')->first();
            $newGroup->childs_with_status = $childsStatus;
            $result[] = $newGroup;
        }
        return $result;
    }

    public function import(Request $request)
    {
        try {
            $user = auth()->user();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        $orgId = Organizations::where('bin', $user->email)->pluck('id')->first();

        $file = $request->file('file');

        try {
            Excel::import(new GroupsImport($orgId), $file);
        } catch (\Exception $ex) {
            return "Таблица эксель заполнена не по формату";
        }

        return response()->json(['status' => 'success'], 200);
    }

    public function deleteChild ( Request $request, $id) {
        $request->validate([
            'id_child' => 'required|numeric',
        ]);

        $child = Childs::findOrFail($request->input('id_child'));
        
        $child->id_group = null;
        $child->save();

        $childToDay = ChildToDay::where('id_child', $request->input('id_child'))->get()->map(function($child) {
            $child->id_group = null;
            $child->save();
        });

        return response()->json(['message' => 'Child removed from group successfully.']);
    }

    public function addChild (Request $request, $id ) {
        $request->validate([
            'id_child' => 'required|numeric',
        ]);
        $this->id = $id;
        $child = Childs::findOrFail($request->input('id_child'));
        
        $child->id_group = $id;
        $child->save();

        $childToDay = ChildToDay::where('id_child', $request->input('id_child'))->get()->map(function($child) {
            $child->id_group = $this->id;
            $child->save();
        });

        return response()->json(['message' => 'Child added to group successfully.']);
    }

    public function getOtherGroupsList(Request $request) {
        try {
            $user = auth()->user();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        $orgId = Organizations::where('bin', $user->email)->pluck('id')->first();
        $groupsWithChilds = $this->childs->where('id_org', $orgId)->unique('id_group')->pluck('id_group')->toArray();
        $otherGroupsList = $this->groups->where('id_org', $orgId)->whereIn('id', $groupsWithChilds)->where('id', "!=", $request->input('id_group'))->pluck('name', 'id')->toArray();

        return $otherGroupsList;
    }
}
