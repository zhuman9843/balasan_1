<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Department;
use App\Models\Districts;
use App\Models\Government;
use App\Models\RegionGroup;
use App\Models\Regions;

class PlaceController extends Controller
{

    public function getGovernment() {
        $government = Government::all()->toArray();
        return $government;
    }

    public function getDepartment(Request $request) {
        $government = $request->input('government');
        if ($government == 0 ){
            $departments = Department::all()->toArray();
        } else {
            $districts = Districts::where('id_region', $government)->pluck('id')->toArray();
            $departments = Department::whereIn('id', $districts)->get()->toArray();
        }
        return $departments;
    }

    public function getRegions() {
        $regions = Regions::all()->toArray();
        return $regions;
    }

    public function getDistricts(Request $request) {
        $region = $request->input('region');
        if ($region == 0 ){
            $districts = Districts::all()->toArray();
        } else {
            $districts = Districts::where('id_region', $region)->get()->toArray();
        }
        return $districts;
    }

    public function getRegionGroup() {
        $region_group = Districts::all()->toArray();
        return $region_group;
    }
}
