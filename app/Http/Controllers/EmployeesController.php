<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Organizations;
use App\Models\Employees;
use App\Models\User;
use App\Models\TypeOfStatus;
use App\Imports\EmployeesImport;
use App\Models\EmployeesStatus;
use App\Models\EmployeesToDay;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;

class EmployeesController extends Controller
{
    private $orgId;
    private $employees;
    private $employeesStatus;
    private $currentDate;
    private $selectedMonth;
    private $allAttendance;
    private $allMissing;
    private $allSick;
    private $allWeekend;
    private $allAttend;
    private $childKey;
    private $currentYear = 2024;
    private $holidays = [ '01-01', '02-01', '03-01', '08-03', '21-03', '22-03', '23-03', '01-05', '07-05', '08-05', '09-05', '29-06', '06-07', '30-08', '01-12', '18-12' ];


    public function __construct()
    {
        $this->currentDate = Carbon::now()->format('Y-m-d');
        $this->employees = Employees::all();   
        $this->employeesStatus = EmployeesStatus::all();   
    }

    public function getEmployees() {
        try {
            $user = auth()->user();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        if ($user->role == 0) {
            $orgId = Organizations::where('bin', $user->email)->pluck('id')->first();
            $employeesStatus = EmployeesStatus::where('id_org', $orgId)->where('date', $this->currentDate)->where('status_send', "!=", 3)->get()->count();

            $employees = $this->employees->where('id_org', $orgId)->map(function($employee) {
                $employee->status_send = array_keys($this->getStatus($employee->id))[0];
                $employee->status_text = array_values($this->getStatus($employee->id))[0];
                $employee->time_of_status = $this->getTimeOfStatus($employee->id);
                $employee->save;
                return $employee;
            })->values()->toArray();

        } else {
            $idEmployee = Employees::where('iin', $user->email)->pluck('id')->first();
            $orgId = Employees::where('iin', $user->email)->pluck('id_org')->first();
            $employeesStatus = EmployeesStatus::where('id_employee', $idEmployee)->where('date', $this->currentDate)->where('status_send', "!=", 3)->get()->count();

            $employees = $this->employees->where('id', $idEmployee)->map(function($employee) {
                $employee->status_send = array_keys($this->getStatus($employee->id))[0];
                $employee->status_text = array_values($this->getStatus($employee->id))[0];
                $employee->time_of_status = $this->getTimeOfStatus($employee->id);
                $employee->save;
                return $employee;
            })->values()->toArray();
        }

        return [
            'employees_with_status' => $employeesStatus,
            'list' => $employees
        ];
    }
        
    public function import(Request $request)
    {
        try {
            $user = auth()->user();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        $orgId = Organizations::where('bin', $user->email)->pluck('id')->first();

        $file = $request->file('file');

        try {
            Excel::import(new EmployeesImport($orgId), $file);
        } catch (\Exception $ex) {
            return "Таблица эксель заполнена не по формату";
        }

        return response()->json(['status' => 'success'], 200);
    }

    public function changePassword(Request $request) {
        try {
            $user = auth()->user();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        $user = User::where('email', $request->iin)->first();
        $user->password = bcrypt($request->password);
        $user->save();

        return response()->json(['status' => 'success'], 200);
    }

    public function update(Request $request, $id)
    {
        try {
            $user = auth()->user();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        $request->validate([
            'name' => 'required|string|max:255',
            'iin' => 'required|numeric|min:12',
            'job' => 'required|string',
            'telephone' => 'required|numeric|min:11',
            'email' => 'required|email|max:255',
            'birth' => 'required',
        ]);

        $orgId = Organizations::where('bin', $user->email)->pluck('id')->first();
        $person = Employees::findOrFail($id);

        $checkExistingEmployeeId = Employees::where('iin', $request->iin)->where('id_org', $orgId)->pluck('id')->first();

        if ($checkExistingEmployeeId && $checkExistingEmployeeId != $id) {
            return response()->json(['errors' => 'ИИН уже зарегистрирован!'], 500);
        }
        
        $user = User::where('email', $person->iin)->first();

        $person->update([
            'name' => $request->input('name'),
            'telephone' => $request->input('telephone'),
            'job' => $request->input('job'),
            'email' => $request->input('email'),
            'iin' => $request->input('iin'),
            'birth_date' => $request->input('birth'),
        ]);

        $employeesToDay = EmployeesToDay::where('id_employee', $id)->first();
        $employeesToDay->job = $request->input('job');
        $employeesToDay->save();

        $user->email = $request->iin;
        $user->save();
        
        return response()->json(['message' => 'Employee info updated successfully.']);
    }

    public function delete($id) {
        try {
            $person = Employees::findOrFail($id);

            User::where('email', $person->iin)->delete();
            EmployeesToDay::where('id_employee', $id)->delete();
            EmployeesStatus::where('id_employee', $id)->delete();
            Employees::where('id', $id)->delete();

            return response()->json(['message' => 'Employee have been deleted successfully.']);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return $e->getMessage();
        }
    }

    public function sendPhotoExist(Request $request) {
        $request->validate([
            'id_employee' => 'required|numeric',
            'photo_exists' => 'required|numeric'
        ]);

        $child = $this->employees->where('id', $request->input('id_employee'))->first();
        $child->photo_exists = $request->input('photo_exists');
        $child->save();

        return response()->json(['status' => 'success'], 200);
    }

    public function addEmployeeStatus(Request $request) {
        try {
            $user = auth()->user();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        $request->validate([
            'id_employee' => 'required|numeric',
            'status_send' => 'required|numeric'
        ]);
        $orgId = $this->employees->where('id', $request->id_employee)->pluck('id_org')->first();
        $checkExistingStatus = EmployeesStatus::where('id_employee', $request->id_employee)->where('date', $this->currentDate)->first();
        $statusText = TypeOfStatus::all()->where('status_send', $request->status_send)->pluck('status_text')->first();

        if ($checkExistingStatus) {
            $checkExistingStatus->status_text = $statusText;
            $checkExistingStatus->status_send = $request->status_send;
            $checkExistingStatus->save();
        } else {
            $employeeStatis = new EmployeesStatus();
            $employeeStatis->id_employee = $request->id_employee;
            $employeeStatis->id_org = $orgId;
            $employeeStatis->status_send = $request->status_send;
            $employeeStatis->status_text = $statusText;
            $employeeStatis->date = $this->currentDate;
            $employeeStatis->save();
        }
        $this->addToEmployeeToDay($request->id_employee, $orgId, $request->status_send);

        return response()->json(['status' => 'success'], 200);
    }

    public function addToEmployeeToDay($id_employee, $id_org, $type_of_attendance){
        $currentDay = (int)Carbon::now()->format('d');
        $currentMonth = Carbon::now()->format('Y-m');
        $key = 'day_'.$currentDay;

        $checkExistingChildToDay = EmployeesToDay::where('id_child', $id_employee)->where('month', $currentMonth.'-01')->first();

        $childToDay = ($checkExistingChildToDay) ? $checkExistingChildToDay : new EmployeesToDay();

        $childToDay->id_child = $id_employee;
        $childToDay->id_org = $id_org;
        $childToDay->month = $currentMonth.'-01';
        $childToDay[$key] = $type_of_attendance;
        $childToDay->updated_at = $this->currentDate;
        $childToDay->created_at = $this->currentDate;
        $childToDay->save();

        return response()->json(['status' => 'success'], 200);
    }

    public function getEmployeeStatus (Request $request) {
        try {
            $employeeStatus = $this->employeesStatus->where('id_employee', $request->input('id_employee'))->where('date', $this->currentDate)->firstOrFail();
            $employeeInfo = Employees::where('id', $employeeStatus->id_employee)->first();
            $time = ($employeeStatus->updated_at) ? $employeeStatus->updated_at : $employeeStatus->created_at;

            $status = [
                'id' => $employeeStatus->id_employee,
                'statusText' => $employeeStatus->status_text,
                'statusSend' => $employeeStatus->status_send,
                'name' => $employeeInfo->name,
                'time' => $time->format('m.d.Y H:i')
            ];

            return  $status;
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return $e->getMessage();
        }
    }

    public function changeEmployeesStatus(Request $request) {
        try {
            $user = auth()->user();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        $childId = $request->input('employee');
        $status = $request->input('status');
        $day = $request->input('day');
        $month = $request->input('month');
        $changedDate = Carbon::create($this->currentYear, $month, $day)->format('Y-m-d');
        $date = $this->currentYear.'-'.$month.'-01';

        $orgId = $this->employees->where('id', $childId)->pluck('id_org')->first();
        $checkExistingStatus = EmployeesStatus::where('id_employee', $childId)->where('date', $changedDate)->first();
        $statusText = TypeOfStatus::all()->where('status_send', $status)->pluck('status_text')->first();

        if ($checkExistingStatus) {
            $checkExistingStatus->status_text = $statusText;
            $checkExistingStatus->status_send = $status;
            $checkExistingStatus->save();
        } else {
            $childStatus = new EmployeesStatus();
            $childStatus->id_employee = $childId;
            $childStatus->id_org = $orgId;
            $childStatus->status_send = $status;
            $childStatus->status_text = $statusText;
            $childStatus->date = $changedDate;
            $childStatus->save();
        }

        try {
            $child = EmployeesToDay::where('id_employee', $childId)->where('month', $date)->firstOrFail();
            $child['day_'.$day] = (int)str_replace("1", "", $status) + 10;
            $child->save();

        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return $e->getMessage();
        }

        return response()->json(['status' => 'success'], 200);
    }

    public function getEmployeesToDay(Request $request)
    {    
        try {
            $user = auth()->user();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        $this->orgId = Organizations::where('bin', $user->email)->pluck('id')->first();

        $year = 2024;
        $this->selectedMonth = Carbon::create($year, $request->month, 1);
        $selectedGroup = $request->group_id;
        $daysInMonth = $this->selectedMonth->daysInMonth;

        $headers = [
            'ФИО',
            'КОЛ-ВО ДНЕЙ ПРОПУСКА',
            'КОЛ-ВО ДНЕЙ БОЛЬНИЧНЫХ',
            'КОЛ-ВО ДНЕЙ ОТПУСКА',
            'Сфера',
        ];
        $keys = [
            'name',
            'job',
            'missing',
            'wekeend',
            'sick'
        ];
        $this->allAttendance =[
            'name' => 'Всего отсутствует сотрудников',
            'iin' => '',
            'job' => 'Все',
        ];
        for ($day = 1; $day <= $daysInMonth; $day++) {
            $headers[] = $day;
            $keys[] = $day;
            $this->allAttendance[$day]['attendance'] = 0;
            $this->allAttendance[$day]['all'] = 1;
            $this->allAttendance[$day]['type_of_day'] = 1;
        }

        $this->allMissing = 0;
        $this->allSick    = 0;
        $this->allWeekend = 0;
        $this->allAttend  = 0;
        
        if ($selectedGroup == 0) {
            $attendances = EmployeesToDay::where('id_org', $this->orgId)->where('month', $this->selectedMonth)->get();
        } else {
            $attendances = EmployeesToDay::where('id_org', $this->orgId)->where('id_group', $selectedGroup)->where('month', $this->selectedMonth)->get();
        }

        $this->childKey = 1;
        $dataForTable = $attendances->map(function ($child, $key) {
                                        $childName = $this->employees->where('id', $child->id_employee)->pluck('name')->first();
                                        $job = $child->job;
                                        $countMissing = 0;
                                        $countSick    = 0;
                                        $countWeekend = 0;
                                        $countAttend  = 0;
                                        
                                        $key = $this->childKey;
                                        $this->childKey++;
                                        $child->id;

                                        foreach ($child->toArray() as $isDay => $factAttendance) {
                                            if (Str::contains($isDay, 'day')) {
                                                $dayOfMonth = sprintf("%02d", filter_var($isDay, FILTER_SANITIZE_NUMBER_INT));
                                                $currentDate = date('Y-m', strtotime($this->selectedMonth)).'-'.$dayOfMonth;
        
                                                if ( checkdate($this->selectedMonth->format('m'), $dayOfMonth, 2024) === true) {
                                                    $isHoliday = $this->isHoliday($currentDate);
                                                    $isWeekend = Carbon::parse($currentDate)->isWeekend();
                                                    
                                                    if ($isHoliday || $isWeekend) {
                                                        $typeOfDay = 2;
                                                        $attendance = 6;
                                                    } else {
                                                        $typeOfDay = 1;
                                                    }
                                                    if (($factAttendance == 6 || $factAttendance == 16) && $typeOfDay !== 2){
                                                        $this->allMissing++;
                                                        $countMissing++;
                                                        $this->allAttendance[(int)Carbon::parse($currentDate)->format('d')]['attendance'] += 1;
                                                        $attendance = $factAttendance;
                                                    } else {
                                                        if ($factAttendance == 2 || $factAttendance == 12) {
                                                            $countAttend++;
                                                            $attendance = $factAttendance;
                                                        } else {
                                                            if ($factAttendance == 4) {
                                                                $this->allSick++;
                                                                $countSick++;
                                                                $attendance = 4;
                                                            } else if ($factAttendance == 5) {
                                                                $this->allWeekend++;
                                                                $countWeekend++;
                                                                $attendance = 5;
                                                            } else if ($factAttendance == 14) {
                                                                $this->allSick++;
                                                                $countSick++;
                                                                $attendance = 14;
                                                            } else if ($factAttendance == 15) {
                                                                $this->allWeekend++;
                                                                $countWeekend++;
                                                                $attendance = 15;
                                                            }
                                                        }
                                                    }
                                                    $childRow [(int)Carbon::parse($currentDate)->format('d')] = [
                                                        'type_of_day' => $typeOfDay,
                                                        'attendance' => $attendance
                                                    ];
                                                }
                                            }
                                        }
                                        
                                        $childRow['reason'] = [
                                            'type_of_day' => 1,
                                            'attendance' => 0,
                                        ];
                                        $childRow['id']    = $child->id_employee;
                                        $childRow['name']    = $childName;
                                        $childRow['job']     = $job;
                                        $childRow['sick']    = $countSick;
                                        $childRow['wekeend'] = $countWeekend;
                                        $childRow['missing'] = $countMissing;
                                        
                                        return $childRow;
                                    })->toArray();
        $this->allAttendance['sick']    = $this->allSick;
        $this->allAttendance['wekeend'] = $this->allWeekend;
        $this->allAttendance['missing'] = $this->allMissing;
        $this->allAttendance['reason'] = [
            'type_of_day' => 1,
            'attendance' => 0,
        ];
        $dataForTable[] = $this->allAttendance;
        
        $result = [
            'headers' => $headers,
            'keys' => $keys,
            'data' => array_values($dataForTable)
        ];

        return $result;
    }

    public function isHoliday($date) : bool
    {
        foreach ($this->holidays as $holiday) {
            if (Carbon::parse($date)->format('d-m') == $holiday) {
                return true;
                break;
            }
        }

        return false;
    }

    public function getTimeOfStatus($id) {
        $time = $this->employeesStatus->where('id_employee', $id)->where('date', $this->currentDate)->first();
        if( $time ) {
            $timeOfStatus = ($time->updated_at) ? $time->updated_at : $time->created_at;
            if (!$timeOfStatus) return "";
            return $timeOfStatus->format('d.m.Y H:i');
        } else {
            return "";
        }
    }

    public function getStatus($id) {
        $photoExisting = $this->employees->where('id', $id)->pluck('photo_exists')->first();

        $status = $this->employeesStatus->where('id_employee', $id)->where('date', $this->currentDate)->pluck('status_text', 'status_send')->toArray();
        if ( $status ) {
            return $status;
        } else {
            if ( $photoExisting ) {
                return [1 => "Ожидает отметки"];
            } else {
                return [0 => "Первоначальная иденфикация"];
            }
        }
        return $status;
    }

    public function createEmployeesToDayNextMonth() {
        $allChildToDay = EmployeesToDay::get(['id_employee', 'job', 'id_org'])->map(function($childToDay) {

            $newMonthChildToDay = new EmployeesToDay([
                'id_employee' => $childToDay->id_employee,
                'job' => $childToDay->job,
                'id_org' => $childToDay->id_org,
                'month' => Carbon::now()->format('Y-m')."-01"
            ]);

            $newMonthChildToDay->save();
        });

        return 'Done';
    }
}
