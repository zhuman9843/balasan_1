<?php

namespace App\Http\Controllers;

use App\Models\Groups;
use App\Models\Regions;
use App\Models\ChildToDay;
use App\Models\Government;
use App\Models\Department;
use App\Models\Organizations;
use App\Models\TableAnalytics;
use App\Models\TypeRegions;
use App\Models\TypeLocation;
use App\Models\TypeOrg;
use App\Models\Tarify;
use App\Models\Childs;
use App\Models\FilteredOrganizations;
use App\Models\TableAnalyticUsers;
use App\Models\TypeOfGroup;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Carbon\CarbonPeriod;

class ChildsToDayController extends Controller
{

    private $workingDays = [];
    private $holidays = [ '01-01', '02-01', '03-01', '08-03', '21-03', '22-03', '23-03', '01-05', '07-05', '08-05', '09-05', '29-06', '06-07', '30-08', '01-12', '18-12'];
    public $months = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];

    private $id;
    private $date;
    private $month;
    private $groups;
    private $governmentIds;
    private $regions;
    private $tarify;
    private $organizations;
    private $government;
    private $department;
    private $typeOfRegion;
    private $groupsCount;
    private $typeOfLocation;
    private $typeOrg;
    private $typeOfGroup;
    private $groupsChildAmount;
    private $regionNames;
    private $selectedDate;
    private $groupTypes;
    private $groupNames;
    private $selectedMonth;
    private $allMissing;
    private $allSick;
    private $allWeekend;
    private $allAttend;
    private $childKey;
    private $childs;
    private $allAttendance;
    private $currentYear;
    private $notMissed;
    private $startDate;
    private $endDate;
    private $allArticles = [];

    public function __construct()
    {
        $this->notMissed = [ 2, 12, 4, 14, 5, 15];
        $this->id = 1;
        $this->currentYear = Carbon::now()->format('Y');
        $this->childs = Childs::all();
        $this->tarify = Tarify::all();
        $this->typeOrg = TypeOrg::all();
        $this->government = Government::all();
        $this->department = Department::all();
        $this->organizations = Organizations::all();
        $this->regionNames = Regions::pluck('region', 'id');
        $this->typeOfGroup = TypeOfGroup::pluck('id', 'category');
        $this->groups = Groups::pluck('type_group', 'id');
        $this->groupsCount = Groups::pluck('count_place', 'id');
        $this->groupsChildAmount = Groups::pluck('count_place', 'id');
        $this->regions = Regions::pluck('group_type', 'id');
        $this->governmentIds = Government::all()->pluck('id' ,'government')->toArray();
        $this->typeOfLocation = TypeLocation::pluck('type_location_code', 'id');
        $this->typeOfRegion = TypeRegions::pluck('code_of_region', 'id');
        $this->groupTypes = Groups::pluck('type_group', 'id');
        $this->groupNames = Groups::pluck('name', 'id');
        $this->getWorkingDays();
    }
    public function getChildsToDay(Request $request) 
    {
        try {
            $user = auth()->user();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        // $departmentId = TableAnalyticUsers::where("email", $user->email)->pluck("department_id")->first();
        // $governmentId = TableAnalyticUsers::where("email", $user->email)->pluck("government_id")->first();

        $startDate  = $request->input('start_date');
        $endDate  = $request->input('end_date');
        $selectedGovernmentId = $request->input('government');

        // $department = Department::where('id', $departmentId)->pluck('department')->first();
        // $government = Government::where('id', $governmentId)->pluck('selectedGovernmentId')->first();
        $notBlockedOrganizations = FilteredOrganizations::where('is_blocked', 0)->pluck('bin')->toArray();
        $eduOrgs = $this->organizations->where('type_project', 1)->pluck('id')->toArray();

        $data = TableAnalytics::whereIn('id_org', $eduOrgs)->whereIn('bin', $notBlockedOrganizations)->where('month', "<=", $endDate)->where('month', ">=", $startDate)->get();

        $childToDay = $data->groupBy('government')
                                        ->transform(function ($item) {
    
                                            $dataByDepartment = $item->groupBy('department')->transform(function ($item, $key) {
    
                                                $dataByTypeOfDepartment = $item->groupBy('type_of_department')->transform(function ($item, $key) {
    
                                                    $dataByContragent = $item->groupBy('contragent')->transform(function ($item, $key) {
                                                        $bin = ($this->organizations->where('name', $key)->first()) ? $this->organizations->where('name', $key)->first()->getAttribute('bin') : "";

                                                        $filterByMonth['data'] = $item->groupBy(function($val) {
                                                            return Carbon::parse($val->month)->format('m');
                                                        })->map(function($value) {
                                                            $value = $value->first();
                                                            return [
                                                                'group' => $value['group'],
                                                                'child_amount_group' => $value['child_amount_group'],
                                                                'child_amount_plan' => $value['child_amount_plan'],
                                                                'child_amount_fact' => $value['child_amount_fact'],
                                                                'sick' => $value['sick'],
                                                                'missing' => $value['missing'],
                                                                'weekend' => $value['weekend'],
                                                                'plan_child_to_day' => $value['plan_child_to_day'],
                                                                'fact_child_to_day' => $value['fact_child_to_day'],
                                                                'budget_plan' => $value['budget_plan'],
                                                                'budget_fact' => $value['budget_fact'],
                                                            ];                                                                 
                                                        })->toArray();

                                                        $filterByMonth['bin'] = $bin;
                                                        $filterByMonth['level'] = 4;
                                                        $filterByMonth['id'] = $this->id;
                                                        $this->id ++;
                                                        return $filterByMonth;
                                                    });
                                                    return $this->getSumOfChildElements($dataByContragent, 3);
                                                });
                                                
                                                return $this->getSumOfChildElements($dataByTypeOfDepartment, 2);
                                            });

                                            return $this->getSumOfChildElements($dataByDepartment, 1);
                                        })->toArray();
                                        
        foreach ($childToDay as $key => &$child) {
            $regionId = $this->governmentIds[$key];
            $child['region_id'] = $regionId;
            $child['name'] = $key;
        }

        return $childToDay;
    }

    public function getSumOfChildElements($childElements, $level)
    {
        $sums = [];

        foreach ($childElements as $key1 => $subarray1) {
            foreach ($subarray1['data'] as $key2 => $subarray2) {
                foreach ($subarray2 as $subkey => $value) {
                    if (!isset($sums[$key2][$subkey])) {
                        $sums[$key2][$subkey] = $value;
                    } else {
                        $sums[$key2][$subkey] += $value;
                    }
                }
            }
        }

        $childElements['data'] = $sums;

        $childItem = [];

        foreach ($childElements as $key => &$child) {
            $child['name'] = $key;
            if ( $key !== 'data' ) {
                unset($child[$key]);
                unset($childElements[$key]);
                $childItem[$key] = $child;
            }
        }
        
        $childElements['child'] = $childItem;
        $childElements['level'] = $level;
        $childElements['id'] = $this->id;
        $this->id ++;
        
        return $childElements;
    }

    public function insertChildToDay(Request $request) 
    {
        $this->startDate  = $request->input('start_date');
        $this->endDate  = $request->input('end_date');

        $eduOrgs = $this->organizations->where('type_project', 1)->pluck('id')->toArray();
        $attendances = ChildToDay::whereIn('id_org', $eduOrgs)->where('month', "<=", $this->endDate)->where('month', ">=", $this->startDate)->get();

        $attendances->groupBy('id_org')
                        ->transform(function ($dataByOrg) {
                            $orgDataByMonth = $dataByOrg->groupBy('month')->transform(function ($attendancesByOrg, $date) {
                                $month = date('m', strtotime($date));

                                $childByGroup = [];
                                $countAttend  = 0;
                                $countMissing = 0;
                                $countSick    = 0;
                                $countWeekend = 0;
                                
                                foreach ($attendancesByOrg->toArray() as $child) {
                                    foreach ($child as $isDay => $factAttendance) {
                                        if (Str::contains($isDay, 'day')) {
                                            $dayOfMonth = sprintf("%02d", filter_var($isDay, FILTER_SANITIZE_NUMBER_INT));
                                            $currentDate = date('Y-m', strtotime($date)).'-'.$dayOfMonth;
                                            
                                            if (!isset($childByGroup[$child['id_group']])) {
                                                $childByGroup[$child['id_group']] = 0;
                                            }
                                            
                                            if ( checkdate($month, $dayOfMonth, $this->currentYear) === true) {
                                                if ($factAttendance == 6 || $factAttendance == 16){
                                                    $isHoliday = $this->isHoliday($currentDate);
                                                    $isWeekend = Carbon::parse($currentDate)->isWeekend();
                                                    
                                                    if (!$isHoliday && !$isWeekend) {
                                                        $countMissing++;
                                                    }
                                                } else {
                                                    $childByGroup[$child['id_group']] += 1;
                                                    
                                                    if ($factAttendance == 4) {
                                                        $countSick++;
                                                    } else if ($factAttendance == 5) {
                                                        $countWeekend++;
                                                    } else if ($factAttendance == 2) {
                                                        $countAttend++;
                                                    } else if ($factAttendance == 14) {
                                                        $countSick++;
                                                    } else if ($factAttendance == 15) {
                                                        $countWeekend++;
                                                    } else if ($factAttendance == 12) {
                                                        $countAttend++;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                
                                $currentOrg = $this->organizations->where('id', $attendancesByOrg->first()['id_org'])->first();
                                
                                if ( $currentOrg ) {
                                    $groupOfRegions = $this->regions[$currentOrg['region']];
                                    $typeOfLocation = $currentOrg['type_location'];
                                    $typeOfRegion = $currentOrg['type_region'];
                                    
                                    $childGroupAmount = 0;
                                    $budgetPlan = 0;
                                    $budgetFact = 0;
                                    $groupAmount = 0;
                                    
                                    foreach ($attendancesByOrg->unique('id_group')->pluck('id_group') as $groupId) {
                                        if (isset($this->groupsCount[$groupId])) {
                                            $childGroupAmount += $this->groupsCount[$groupId];
                                        }
                                    }

                                    foreach( $childByGroup as $group => $childAmount) {
                                        if ($group) {
                                            $groupAmount ++;

                                            $category = $this->groups[$group];
                                            $groupType = $this->typeOfGroup[$category];
                                            
                                            if ($this->tarify->where('region', $currentOrg['region'])->where('group_id', $groupOfRegions)->where('type_of_group', $groupType)->where('type_of_location', $typeOfLocation)->where('type_of_region', $typeOfRegion)->first() == null) {
                                                continue;
                                            }
                                            
                                            $tarify = $this->tarify->where('region', $currentOrg['region'])
                                                    ->where('group_id', $groupOfRegions)
                                                    ->where('type_of_group', $groupType)
                                                    ->where('type_of_location', $typeOfLocation)
                                                    ->where('type_of_region', $typeOfRegion)
                                                    ->first()
                                                    ->getAttribute('tarify');
                                            
                                            
                                            if (isset($this->groupsChildAmount[$group])) {
                                                $amountPlan = $this->getChildAmountByGroup($currentOrg['id'], $group);
                                                $budgetPlan += ($amountPlan * $tarify);
                                            }
                                            
                                            $budgetFact += ($childAmount * $tarify / $this->workingDays[$month]);
                                        }
                                    }
                                    
                                    $checkExistingAttendanceOrgByMonth = TableAnalytics::where('id_org', $currentOrg['id'])->where('month', $date)->first();

                                    if ( $checkExistingAttendanceOrgByMonth ) {
                                        $checkExistingAttendanceOrgByMonth->region = $currentOrg['region'];
                                        $checkExistingAttendanceOrgByMonth->government = $this->government->where('region_id', $currentOrg['region'])->pluck('government')->first();
                                        $checkExistingAttendanceOrgByMonth->department = $this->department->where('id', $currentOrg['district'])->pluck('department')->first();
                                        $checkExistingAttendanceOrgByMonth->bin = $currentOrg['bin'];
                                        $checkExistingAttendanceOrgByMonth->contragent = $currentOrg['name'];
                                        $checkExistingAttendanceOrgByMonth->type_of_department = $this->typeOrg->where('id', $currentOrg->type_org)->first()->getAttribute('type_org');
                                        $checkExistingAttendanceOrgByMonth->month = $date;
                                        $checkExistingAttendanceOrgByMonth->group = $groupAmount;
                                        $checkExistingAttendanceOrgByMonth->child_amount_plan = $currentOrg['project_capacity'];
                                        $checkExistingAttendanceOrgByMonth->child_amount_fact = count($attendancesByOrg);
                                        $checkExistingAttendanceOrgByMonth->child_amount_group = $childGroupAmount;
                                        $checkExistingAttendanceOrgByMonth->plan_child_to_day = count($attendancesByOrg) * $this->workingDays[$month];
                                        $checkExistingAttendanceOrgByMonth->fact_child_to_day = $countAttend + $countSick + $countWeekend;
                                        $checkExistingAttendanceOrgByMonth->missing = $countMissing;
                                        $checkExistingAttendanceOrgByMonth->sick = $countSick;
                                        $checkExistingAttendanceOrgByMonth->weekend = $countWeekend;
                                        $checkExistingAttendanceOrgByMonth->budget_plan = $budgetPlan;
                                        $checkExistingAttendanceOrgByMonth->budget_fact = $budgetFact;
                                        $checkExistingAttendanceOrgByMonth->created_at = Carbon::now();
                                        $checkExistingAttendanceOrgByMonth->updated_at = Carbon::now();
                                        $checkExistingAttendanceOrgByMonth->save();
                                    } else {
                                        $insertAttendance = new TableAnalytics();
                                        $insertAttendance->id_org = $currentOrg['id'];
                                        $insertAttendance->region = $currentOrg['region'];
                                        $insertAttendance->government = $this->government->where('region_id', $currentOrg['region'])->pluck('government')->first();
                                        $insertAttendance->department = $this->department->where('id', $currentOrg['district'])->pluck('department')->first();
                                        $insertAttendance->bin = $currentOrg['bin'];
                                        $insertAttendance->contragent = $currentOrg['name'];
                                        $insertAttendance->type_of_department = $this->typeOrg->where('id', $currentOrg->type_org)->first()->getAttribute('type_org');
                                        $insertAttendance->month = $date;
                                        $insertAttendance->group = $groupAmount;
                                        $insertAttendance->child_amount_plan = $currentOrg['project_capacity'];
                                        $insertAttendance->child_amount_fact = count($attendancesByOrg);
                                        $insertAttendance->child_amount_group = $childGroupAmount;
                                        $insertAttendance->plan_child_to_day = $currentOrg['project_capacity'] * $this->workingDays[$month];
                                        $insertAttendance->fact_child_to_day = $countAttend;
                                        $insertAttendance->missing = $countMissing;
                                        $insertAttendance->sick = $countSick;
                                        $insertAttendance->weekend = $countWeekend;
                                        $insertAttendance->budget_plan = $budgetPlan;
                                        $insertAttendance->budget_fact = $budgetFact;
                                        $insertAttendance->created_at = Carbon::now();
                                        $insertAttendance->updated_at = Carbon::now();
                                        $insertAttendance->save();
                                    }
                                }
                            });
                            return $orgDataByMonth;
                        });

        return response()->json($attendances);
    }

    public function tarifyData(Request $request) 
    {
        $this->selectedDate = $this->currentYear.'-'.$request->input('month').'-01';

        if ($request->input('bin')) {
            $result = [];

            $currentOrg = $this->organizations->where('bin', $request->input('bin'))->first();
            $result['oblast_name'] = $this->regionNames[$currentOrg['region']];
            $result['type_city'] = TypeLocation::where('id', $currentOrg['type_location'])->pluck('type_location')->first();
            $result['type_ecolog'] = TypeRegions::where('id', $currentOrg['type_region'])->pluck('type_region')->first();
            $result['working_days'] = $this->workingDays[date('m', strtotime($this->selectedDate))];

            $result['child_amount'] = 0;
            $result['amount_plan'] = 0;
            $result['amount_fact'] = 0;
            $result['budget_plan'] = 0;
            $result['budget_fact'] = 0;
            
            $groupOfRegions = $this->regions[$currentOrg['region']];

            $attendances = ChildToDay::where('id_org', $currentOrg['id'])->where('month', $this->selectedDate)->get();
            
            $groupIds = $attendances->unique('id_group')->pluck('id_group');

            foreach ($groupIds as $groupId) {
                if ($groupId) {
                    $group = [];

                    $tarify = $this->tarify->where('region', $currentOrg['region'])
                                                ->where('group_id', $groupOfRegions)
                                                ->where('type_of_group', $this->typeOfGroup[$this->groupTypes[$groupId]])
                                                ->where('type_of_location', $currentOrg['type_location'])
                                                ->where('type_of_region', $currentOrg['type_region'])
                                                ->first()
                                                ->getAttribute('tarify');

                    $filteredData = $attendances->where('id_group', $groupId)->map(function ($item) {
                        $count = 0;

                        foreach ($item->toArray() as $isDay => $factAttendance) {

                            if (Str::contains($isDay, 'day')) {
                                $dayOfMonth = sprintf("%02d", filter_var($isDay, FILTER_SANITIZE_NUMBER_INT));

                                $currentDate = date('Y-m', strtotime($this->selectedDate)).'-'.$dayOfMonth;

                                if ( checkdate(date('m', strtotime($this->selectedDate)), $dayOfMonth, $this->currentYear) === true) {

                                    $isHoliday = $this->isHoliday($currentDate);
                                    $isWeekend = Carbon::parse($currentDate)->isWeekend();

                                    $isMissed = (in_array( $factAttendance, $this->notMissed));

                                    if (!$isHoliday && !$isWeekend && $isMissed) {
                                        $count ++;
                                    }
                                }
                            }
                        }
                        return $count;
                    })->toArray();

                    $amountPlan = $this->getChildAmountByGroup($currentOrg['id'], $groupId);
                    $result['amount_plan'] += $amountPlan * $this->workingDays[date('m', strtotime($this->selectedDate))];
                    $result['amount_fact'] += array_sum($filteredData);
                    $result['budget_plan'] += $amountPlan * $tarify;
                    $result['budget_fact'] += array_sum($filteredData) * $tarify / $this->workingDays[date('m', strtotime($this->selectedDate))];
                    $result['child_amount'] += $amountPlan;
                    
                    $group['group']        = $this->groupNames[$groupId];
                    $group['child_amount'] = $amountPlan;
                    $group['tarify']       = $tarify;
                    $group['working_days'] = $this->workingDays[date('m', strtotime($this->selectedDate))];
                    $group['amount_plan']  = $amountPlan * $this->workingDays[date('m', strtotime($this->selectedDate))];
                    $group['amount_fact']  = array_sum($filteredData);
                    $group['budget_plan']  = $amountPlan * $tarify;
                    $group['budget_fact']  = array_sum($filteredData) * $tarify / $this->workingDays[date('m', strtotime($this->selectedDate))];            
                    $result['groups'][] = $group;
                }
            }
        }
        return $result;
    }

    public function getChildAmountByGroup($org, $groupId) {
        return Childs::where('id_org', $org)->where('id_group', $groupId)->get()->count();
    }

    public function tableData(Request $request) {
        try {
            $user = auth()->user();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
        
        $bin = $request->input('bin');
        $orgId = $this->organizations->where('bin', $bin)->pluck('id')->first();

        $this->selectedMonth = Carbon::create($this->currentYear, $request->month, 1);
        $daysInMonth = $this->selectedMonth->daysInMonth;

        $headers = [
            'ФИО',
            'КОЛ-ВО ДНЕЙ ПРОПУСКА',
            'КОЛ-ВО ДНЕЙ ОТПУСКА',
            'КОЛ-ВО ДНЕЙ БОЛЬНИЧНЫХ',
            'ГРУППА',
        ];
        $keys = [
            'name',
            'group',
            'missing',
            'wekeend',
            'sick'
        ];
        $this->allAttendance =[
            'name' => 'Всего отсутствует детей',
            'iin' => '',
            'group' => 'Все',
        ];
        for ($day = 1; $day <= $daysInMonth; $day++) {
            $headers[] = $day;
            $keys[] = $day;
            $this->allAttendance[$day]['attendance'] = 0;
            $this->allAttendance[$day]['all'] = 1;
            $this->allAttendance[$day]['type_of_day'] = 1;
        }

        $this->allMissing = 0;
        $this->allSick    = 0;
        $this->allWeekend = 0;
        $this->allAttend  = 0;

        $groupId = $request->input('group');

        if($groupId == 0) {
            $attendances = ChildToDay::where('id_org', $orgId)->where('month', $this->selectedMonth)->get();
        } else {
            $attendances = ChildToDay::where('id_org', $orgId)->where('id_group', $groupId)->where('month', $this->selectedMonth)->get();
        }

        $this->childKey = 1;
        $dataForTable = $attendances->map(function ($child, $key) {
                                        $childName = $this->childs->where('id', $child->id_child)->pluck('name')->first();
                                        $groupName = ($child->id_group) ? $this->groupNames[$child->id_group] : "Нет группы";
                                        $countMissing = 0;
                                        $countSick    = 0;
                                        $countWeekend = 0;
                                        $countAttend  = 0;
                                        
                                        $key = $this->childKey;
                                        $this->childKey++;
                                        $child->id;

                                        foreach ($child->toArray() as $isDay => $factAttendance) {
                                            if (Str::contains($isDay, 'day')) {
                                                $dayOfMonth = sprintf("%02d", filter_var($isDay, FILTER_SANITIZE_NUMBER_INT));
                                                $currentDate = date('Y-m', strtotime($this->selectedMonth)).'-'.$dayOfMonth;
        
                                                if ( checkdate($this->selectedMonth->format('m'), $dayOfMonth, $this->currentYear) === true) {
                                                    $isHoliday = $this->isHoliday($currentDate);
                                                    $isWeekend = Carbon::parse($currentDate)->isWeekend();
                                                    
                                                    if ($isHoliday || $isWeekend) {
                                                        $typeOfDay = 2;
                                                        $attendance = 6;
                                                    } else {
                                                        $typeOfDay = 1;
                                                    }
                                                    if (($factAttendance == 6 || $factAttendance == 16) && $typeOfDay !== 2){
                                                        $this->allMissing++;
                                                        $countMissing++;
                                                        $this->allAttendance[(int)Carbon::parse($currentDate)->format('d')]['attendance'] += 1;
                                                        $attendance = $factAttendance;
                                                    } else {
                                                        if ($factAttendance == 2 || $factAttendance == 12) {
                                                            $countAttend++;
                                                            $attendance = $factAttendance;
                                                        } else {
                                                            if ($factAttendance == 4) {
                                                                $this->allSick++;
                                                                $countSick++;
                                                                $attendance = 4;
                                                            } else if ($factAttendance == 5) {
                                                                $this->allWeekend++;
                                                                $countWeekend++;
                                                                $attendance = 5;
                                                            } else if ($factAttendance == 14) {
                                                                $this->allSick++;
                                                                $countSick++;
                                                                $attendance = 14;
                                                            } else if ($factAttendance == 15) {
                                                                $this->allWeekend++;
                                                                $countWeekend++;
                                                                $attendance = 15;
                                                            }
                                                        }
                                                    }
                                                    $childRow [(int)Carbon::parse($currentDate)->format('d')] = [
                                                        'type_of_day' => $typeOfDay,
                                                        'attendance' => $attendance
                                                    ];
                                                }
                                            }
                                        }
                                        
                                        $childRow['reason'] = [
                                            'type_of_day' => 1,
                                            'attendance' => 0,
                                        ];
                                        $childRow['name']    = $childName;
                                        $childRow['group']   = $groupName;
                                        $childRow['sick']    = $countSick;
                                        $childRow['wekeend'] = $countWeekend;
                                        $childRow['missing'] = $countMissing;
                                        
                                        return $childRow;
                                    })->toArray();
        $this->allAttendance['sick']    = $this->allSick;
        $this->allAttendance['wekeend'] = $this->allWeekend;
        $this->allAttendance['missing'] = $this->allMissing;
        $this->allAttendance['reason'] = [
            'type_of_day' => 1,
            'attendance' => 0,
        ];
        $dataForTable[] = $this->allAttendance;
        return [
            'headers' => $headers,
            'keys' => $keys,
            'data' => array_values($dataForTable)
        ];            
    }

    public function getDataExport(Request $request) {
        $childToDay = $this->getChildsToDay($request);
        $this->allArticles($request, $childToDay);

        return $this->allArticles;
    }

    public function allArticles($request, $childToDay) {
        // $month = date('m', strtotime($request->input('start_date')));
        foreach($childToDay as $article){
            $month = array_keys($article['data'])[0];
            $space = $this->addSpacesByLevels($article['level']);

            $filteredArticle = [
                'НАИМЕНОВАНИЕ' => $space.$article['name'],
                'УРОВЕНЬ' => $article['level'],
                'КОЛ-ВО ГРУПП' => $article['data'][$month]['group'],
                'КОЛ-ВО ДЕТЕЙ ПО ПЛАНУ' => $article['data'][$month]['child_amount_plan'],
                'КОЛ-ВО ДЕТЕЙ ПО ФАКТУ' => $article['data'][$month]['child_amount_fact']
            ];

            foreach ($article['data'] as $key => $data) {
                if ($request->page == 'amount') {
                    $filteredArticle[strtoupper($this->months[(int)$key - 1]).'(ПЛАН)'] = $data['plan_child_to_day'];
                    $filteredArticle[strtoupper($this->months[(int)$key - 1]).'(ФАКТ)'] = $data['fact_child_to_day'];
                } else {
                    $filteredArticle[strtoupper($this->months[(int)$key - 1]).'(ПЛАН)'] = $data['budget_plan'];
                    $filteredArticle[strtoupper($this->months[(int)$key - 1]).'(ФАКТ)'] = $data['budget_fact'];
                }
            }

            if (isset($article['child'])) {
                array_push($this->allArticles, $filteredArticle);
                $this->allArticles($request, $article['child']);
            } else {
                array_push($this->allArticles, $filteredArticle);
            }
        }
    }

    public function addSpacesByLevels($level) {
        $result = '';
        $result .= str_repeat('  ', $level * 2);
        return $result;
    }

    public function getWorkingDays(){
        $start_date = Carbon::parse($this->currentYear.'-01-01');
        $end_date = Carbon::parse($this->currentYear.'-12-31');
        $period = CarbonPeriod::create($start_date, $end_date);

        foreach ($period as $date) {
            if ($date->isWeekend() || in_array($date->format('d-m'), $this->holidays)) {
                continue;
            }
        
            $month = $date->format('m');
            if (!isset($this->workingDays[$month])) {
                $this->workingDays[$month] = 1;
            } else {
                $this->workingDays[$month] += 1;
            }
        }
    }

    public function isHoliday($date) : bool
    {
        foreach ($this->holidays as $holiday) {
            if (Carbon::parse($date)->format('d-m') == $holiday) {
                return true;
                break;
            }
        }
        return false;
    }
}
