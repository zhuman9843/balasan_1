<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Artisan;
use Illuminate\Http\Request;
use App\Models\Employees;
use App\Models\TypeProject;
use App\Models\TypeOrg;
use App\Models\TypeOfGroup;
use App\Models\TypeOfDay;
use App\Models\TypeOfAttendances;
use App\Models\TypeLocation;
use App\Models\TypeRegions;
use App\Models\ReportProblems;
use App\Models\Gender;
use App\Models\GroupsAgeRanges;
use App\Models\Organizations;
use App\Models\TableAnalyticUsers;
use App\Models\Teachers;

class PropertiesController extends Controller
{
    private $teachers;

    public function __construct()
    {
        $this->teachers = Teachers::all();
    }

    public function getTypeProject() {
        $type_projects = TypeProject::all()->toArray();
        return $type_projects;
    }

    public function getTypeRegions() {
        $type_regions = TypeRegions::all()->toArray();
        return $type_regions;
    }
    
    public function getTypeOrg() {
        $type_org = TypeOrg::all()->toArray();
        return $type_org;
    }    

    public function getTypeGroup() {
        $type_group = TypeOfGroup::all()->toArray();
        return $type_group;
    }
        
    public function getTypeOfDay() {
        $type_of_day = TypeOfDay::all()->toArray();
        return $type_of_day;
    }
        
    public function getTypeOfAttendances() {
        $type_of_attendances = TypeOfAttendances::all()->toArray();
        return $type_of_attendances;
    }
        
    public function getTypeLocation() {
        $type_location = TypeLocation::all()->toArray();
        return $type_location;
    }

    public function getGroupsAgeRange() {
        $ages = GroupsAgeRanges::all()->toArray();
        return $ages;
    }

    public function getGender() {
        $gender = Gender::all()->toArray();
        return $gender;
    }

    public function cacheClear() {
        Artisan::call('cache:clear');
        return 'Cache cleared successfully';
    }

    public function sendReportedProblem(Request $request) {
        $request->validate([
            'problem' => 'required|string'
        ]);

        $user = auth()->user();
        
        if ($user) {
            if ($user->role == 0) {
                $organization = Organizations::where('bin', $user->email)->first();

                $orgId = $organization->id;
                $email = $organization->email;
                $telephone = $organization->telephone;
            } else if ($user->role == 1){
                $teacher = Teachers::where('iin', $user->email)->first();

                $orgId = $teacher->id_org;
                $email = $teacher->email;
                $telephone = $teacher->telephone;
            } else if ($user->role == 2){
                $tableAnalyticUsers = TableAnalyticUsers::where('email', $user->email)->first();

                $orgId = null; 
                $email = $tableAnalyticUsers->email;
                $telephone = $tableAnalyticUsers->telephone;
            } else if ($user->role == 3){
                $employee = Employees::where('iin', $user->email)->first();
                
                $orgId = $employee->id_org;
                $email = $employee->email;
                $telephone = $employee->telephone;
            }
            
            $problem = new ReportProblems;
            $problem->id_manager = null;
            $problem->id_org = $orgId;
            $problem->email = $email;
            $problem->telephone = $telephone;
            $problem->role = $user->role; 
            $problem->problem = $request->input('problem');
            $problem->type = 0;
        } else {
            $problem = new ReportProblems;

            $problem->id_manager = null;
            $problem->id_org = null;
            $problem->email = $request->input('email');
            $problem->telephone = $request->input('telephone');
            $problem->role = null; 
            $problem->problem = $request->input('problem');
            $problem->type = 0;
        }

        $problem->save();

        return response()->json(['status' => 'success'], 200);
    }
}
