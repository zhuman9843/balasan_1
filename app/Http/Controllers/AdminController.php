<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\FilteredOrganizations;
use App\Models\Managers;
use App\Models\Organizations;
use App\Models\ReportProblems;
use App\Models\User;
use Illuminate\Database\Capsule\Manager;
use Symfony\Component\Console\Question\Question;

class AdminController extends Controller
{
    private $managers;

    public function __construct()
    {
        $this->managers = Managers::pluck('name', 'id');
    }

    public function getFilteredOrganizations( Request $request ) {
        $isBlocked = $request->input('is_blocked');

        $filteredOrganizations = FilteredOrganizations::where('is_blocked', $isBlocked)->get();

        $filteredOrganizations->map(function($organization) {
            $organization->manager = $this->managers[$organization->id_manager];
            $organization->save;
            return $organization;
        });

        return $filteredOrganizations->toArray();
    }

    public function getFilteredOrganizationsNames() {
        $userEmails = User::pluck('email')->toArray();
        $filteredOrganizations = FilteredOrganizations::whereNotIn('bin', $userEmails)->where('is_blocked', 0)->pluck( 'org_name', 'bin')->toArray();
        
        return $filteredOrganizations;
    }

    public function addOrganization(Request $request) {

        $request->validate([
            'manager' => 'required|numeric',
            'bin' => 'required|numeric',
            'org_name' => 'required|string',
            'telephone' => 'required|numeric',
            'notes' => 'required|string',
        ]);

        $currentDate = Carbon::now()->format('Y-m-d');
        
        $checkExistingBin = FilteredOrganizations::where('bin', $request->bin)->first();
        
        if ($checkExistingBin) {
            return response()->json(['errors' => 'По этому БИНу уже зарегистрирована организация'], 500);
        }

        try {
            $organization = new FilteredOrganizations([
                'id_manager' => $request->input('manager'),
                'bin' => $request->input('bin'),
                'org_name' => $request->input('org_name'),
                'telephone' => $request->input('telephone'),
                'notes' => $request->input('notes'),
                'added_date' => $currentDate,
                'is_blocked' => 0,
            ]);
            $organization->save();
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(['errors' => $e->getMessage()], 500);
        }

        return response()->json(['message' => 'Organization added successfully.']);
    }

    public function updateOrganization(Request $request, $id) {
        
        $currentDate = Carbon::now()->format('Y-m-d');

        $organization = FilteredOrganizations::findOrFail($id);
        $organization->id_manager = $request->organization['id_manager'];
        $organization->bin = $request->organization['bin'];
        $organization->org_name = $request->organization['org_name'];
        $organization->telephone = $request->organization['telephone'];
        $organization->notes = $request->organization['notes'];
        $organization->is_blocked = $request->organization['is_blocked'];
        $organization->added_date = $currentDate;
        $organization->save();
        
        return response()->json(['message' => 'Organization updated successfully.']);
    }

    public function deleteOrganization(Request $request, $id) {
        $organization = FilteredOrganizations::findOrFail($id);
        $organization->delete();

        return response()->json(['status' => 'success'], 200);
    }

    public function getQuestions(Request $request) {
        $typeOfQuestion = $request->input('type');

        $questions = ReportProblems::where('type', $typeOfQuestion)->get();
        
        $new = ReportProblems::where('type', 0)->count();
        $waiting = ReportProblems::where('type', 1)->count();
        $done = ReportProblems::where('type', 2)->count();

        $questions->map(function($question) {
            $question->organization = ($question->id_org) ? Organizations::where('id', $question->id_org)->pluck('name')->first() : "";

            if ($question->role === 0) {
                $question->source = "Портал";
            } else if ($question->role == 1) {
                $question->source = "Мобильное приложение учителя";
            } else if ($question->role == 2) {
                $question->organization = "Дэшборд";
                $question->source = "Дэшборд";
            } else if ($question->role == 3) {
                $question->source = "Мобильное приложение сотрудника";
            } else {
                $question->source = "С окна регистрации";
            }

            $question->manager = ($question->id_manager) ? $this->managers[$question->id_manager] : null;
            $question->date = Carbon::parse($question->created_at)->format('d.m.Y');
            $question->save;
            return $question;
        });

        $amount = [
            'new' => $new,
            'waiting' => $waiting,
            'done' => $done
        ];

        return [
            'questions' => $questions->toArray(),
            'amount' => $amount
        ];
    }

    public function updateQuestion(Request $request, $id) {

        $organization = ReportProblems::findOrFail($id);
        $organization->type = $request->question['type'];
        $organization->id_manager = $request->question['id_manager'];
        $organization->save();
        
        return response()->json(['message' => 'Organization updated successfully.']);
    }

    public function getManagers () {
        return Managers::all()->toArray();
    }

    public function addManager(Request $request) {
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string',
            'password' => 'required|string'
        ]);

        $manager = new Managers();
        $user = new User();

        $manager->name = $request->name;
        $manager->email = $request->email;
        $manager->save();
        
        $user->email = $request->email;
        $user->role = 10;
        $user->password = bcrypt($request->password);
        $user->save();
        
        return response()->json(['message' => 'Manager added successfully.']);
    }
}
