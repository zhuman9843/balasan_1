<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Childs;
use App\Models\ChildToDay;
use App\Models\Groups;
use App\Models\Teachers;
use App\Models\Gender;
use App\Models\ChildAttendance;
use App\Models\ChildStatus;
use App\Models\TypeOfStatus;
use App\Models\Organizations;
use App\Imports\ChildsImport;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;

class ChildsController extends Controller
{
    private $currentYear = 2024;
    private $id;
    private $user;
    private $request;
    private $childs;
    private $organizations;
    private $orgId;
    private $groups;
    private $groupId;
    private $teachers;
    private $childStatus;
    private $currentDate;
    private $currentTime;
    private $selectedGroup;
    private $selectedMonth;
    private $allMissing;
    private $allSick;
    private $allWeekend;
    private $allAttend;
    private $allAttendance;
    private $typeOfAttendance;
    private $childKey;
    private $childsWithStatus;
    // 2024
    private $holidays = [ '01-01', '02-01', '08-03', '21-03', '22-03', '25-03', '01-05', '07-05', '09-05', '08-07', '30-08', '25-10', '16-12'];
    // 2023
    // private $holidays = [ '01-01', '02-01', '03-01', '08-03', '21-03', '22-03', '23-03', '01-05', '07-05', '08-05', '09-05', '29-06', '06-07', '30-08', '01-12', '18-12' ];

    public function __construct()
    {
        $this->currentDate = Carbon::now()->format('Y-m-d');
        $this->currentTime = Carbon::now()->format('m.d.Y H:i');
        $this->teachers = Teachers::all();
        $this->childs = Childs::all();
        $this->organizations = Organizations::all();
        $this->groups = Groups::all();
        $this->teachers = Teachers::all();
        $this->childStatus = ChildStatus::all();
    }
    public function index() 
    {
        return view('children.index');
    }

    public function getChilds(Request $request) : Array
    {
        try {
            $user = auth()->user();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
        
        $groupId = $request->input('id_group');
        $orgId = $this->groups->where('id', $groupId)->pluck('id_org')->first();
        $teacherId = $this->teachers->where('iin', $user->email)->where('id_org', $orgId)->pluck('id')->first();

        if ( $groupId ) {
            $childsStatus = ChildStatus::where('id_group', $groupId)->where('date', $this->currentDate)->where('status_send', "!=", 3)->get()->count();
            
            $childs = $this->childs->where('id_group', $groupId)->map(function($child) {
                $child->status_send = array_keys($this->getStatus($child->id))[0];
                $child->status_text = array_values($this->getStatus($child->id))[0];
                $child->time_of_status = $this->getTimeOfStatus($child->id);
                $child->save;
                return $child;
            })->values()->toArray();
        } else {
            $teachersGroupIds = $this->teachers->where('id', $teacherId)->pluck('id_group')->first();
            $groupIds = json_decode($teachersGroupIds, true);
            $childsStatus = ChildStatus::whereIn('id_group', $groupIds)->where('date', $this->currentDate)->where('status_send', "!=", 3)->get()->count();

            $childs = $this->childs->whereIn('id_group', $groupIds)->map(function($child) {
                $child->status_send = array_keys($this->getStatus($child->id))[0];
                $child->status_text = array_values($this->getStatus($child->id))[0];
                $child->time_of_status = $this->getTimeOfStatus($child->id);
                $child->save;
                return $child;
            })->values()->toArray();
        }
        
        $childs = [
            'childs_with_status' => $childsStatus,
            'list' => $childs
        ];

        return $childs;
    }

    public function getOrgChilds() {
        try {
            $user = auth()->user();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        $orgId = Organizations::where('bin', $user->email)->pluck('id')->first();

        $childs = $this->childs->where('id_org', $orgId)->map(function($child) {
            $groupId = $this->groups->where('id', $child->id_group)->pluck('id')->first();
            
            // if (!$groupName) {
            //     $groupName = 'Нет группы';
            // }

            $child->group_name = $groupId;
            // dd($child);
            // $child->gender = $this->getGender($child->gender);
            $child->birth = $this->getBirth($child->iin);
            $child->age = $this->getAge($child->birth);
            $child->save;
            return collect($child)->forget(['id_org', 'id_group', 'photo_exists', 'created_at', 'updated_at']);;
        })->values();

        return $childs;
    }

    public function getTimeOfStatus($id) {
        $time = $this->childStatus->where('id_child', $id)->where('date', $this->currentDate)->first();
        if( $time ) {
            $timeOfStatus = ($time->updated_at) ? $time->updated_at : $time->created_at;
            if (!$timeOfStatus) return "";
            return $timeOfStatus->format('d.m.Y H:i');
        } else {
            return "";
        }
    }

    public function getStatus($id) {
        $photoExisting = $this->childs->where('id', $id)->pluck('photo_exists')->first();

        $status = $this->childStatus->where('id_child', $id)->where('date', $this->currentDate)->pluck('status_text', 'status_send')->toArray();
        if ( $status ) {
            return $status;
        } else {
            if ( $photoExisting ) {
                return [1 => "Ожидает отметки"];
            } else {
                return [0 => "Первоначальная иденфикация"];
            }
        }
        return $status;
    }

    public function getGroupChilds(Request $request) : Array
    {
        $groupId = $request->input('groupId');
        
        $childs = $this->childs->where('id_group', $groupId)->map(function($child) {
            $groupName = $this->groups->where('id', $child->id_group)->pluck('name')->first();
            
            if (!$groupName) {
                $groupName = 'Нет группы';
            }

            $child->group_name = $groupName;
            $child->gender = $this->getGender($child->gender);
            $child->birth = $this->getBirth($child->iin);
            $child->age = $this->getAge($child->birth);
            $child->save;
            return $child;
        })->values()->toArray();
        
        return $childs;
    }

    public function getChildsNotIncluded(Request $request) 
    {
        try {
            $user = auth()->user();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        $orgId = Organizations::where('bin', $user->email)->pluck('id')->first();
        $groupId = (int)$request->input('id_group');

        if ( $groupId == 0 ) {
            $childs = $this->childs->where('id_org', $orgId)->where('id_group', null);
        } else {
            $childs = $this->childs->where('id_org', $orgId)->where('id_group', $groupId);
        }

        $childs = $childs->map(function($child) {
            $groupName = $this->groups->where('id', $child->id_group)->pluck('name')->first();
            
            if (!$groupName) {
                $groupName = 'Нет группы';
            }

            $child->group_name = $groupName;
            $child->gender = $this->getGender($child->gender);
            $child->birth = $this->getBirth($child->iin);
            $child->age = $this->getAge($child->birth);
            $child->save;
            return $child;
        })->values()->toArray();

        return $childs;
    }

    public function addChildAttendance(Request $request) {
        $request->validate([
            'id_child' => 'required|numeric',
            'id_group' => 'required|numeric',
            'attendance' => 'required|numeric'
        ]);

        $checkExistingAttendance = ChildAttendance::where('id_child', $request->id_child)->where('date', $this->currentDate)->first();

        if ( $checkExistingAttendance ) {
            $checkExistingAttendance->attendance = $request->attendance;
            $checkExistingAttendance->save();
        } else {
            $childAttendance = new ChildAttendance();
            $childAttendance->id_child = $request->id_child;
            $childAttendance->id_group = $request->id_group;
            $childAttendance->attendance = $request->attendance;
            $childAttendance->date = $this->currentDate;
            $childAttendance->save();
        }

        return response()->json(['status' => 'success'], 200);
    }
    

    public function getChildAttendance(Request $request) {
        try {
            $attendancesByGroup = ChildAttendance::where('id_group', $request->input('id_group'))->get()->count();
            
            return $attendancesByGroup;
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return $e->getMessage();
        }
    }

    public function getChildStatus (Request $request) {
        try {
            $childStatus = $this->childStatus->where('id_child', $request->input('id_child'))->where('date', $this->currentDate)->firstOrFail();
            $childInfo = Childs::where('id', $childStatus->id_child)->first();
            $gender = Gender::where('id', $childInfo->gender)->pluck('gender')->first();
            $time = ($childStatus->updated_at) ? $childStatus->updated_at : $childStatus->created_at;

            $status = [
                'id' => $childStatus->id_child,
                'gender' => $gender,
                'statusText' => $childStatus->status_text,
                'statusSend' => $childStatus->status_send,
                'name' => $childInfo->name,
                'time' => $time->format('m.d.Y H:i')
            ];

            return  $status;
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return $e->getMessage();
        }
    }

    public function addChildStatus(Request $request) {
        try {
            $user = auth()->user();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }


        $request->validate([
            'id_child' => 'required|numeric',
            'status_send' => 'required|numeric'
        ]);

        $child = $this->childs->where('id', $request->input('id_child'))->first();

        $groupId = $child->id_group;
        $orgId = $child->id_org;
        $teacherId = $this->teachers->where('iin', $user->email)->where('id_org', $orgId)->pluck('id')->first();
        $checkExistingStatus = ChildStatus::where('id_child', $request->id_child)->where('date', $this->currentDate)->first();
        $statusText = TypeOfStatus::all()->where('status_send', $request->status_send)->pluck('status_text')->first();

        if ($checkExistingStatus) {
            $checkExistingStatus->id_teacher = $teacherId;
            $checkExistingStatus->status_text = $statusText;
            $checkExistingStatus->status_send = $request->status_send;
            $checkExistingStatus->save();
        } else {
            $childStatus = new ChildStatus();
            $childStatus->id_child = $request->id_child;
            $childStatus->id_group = $groupId;
            $childStatus->id_teacher = $teacherId;
            $childStatus->status_send = $request->status_send;
            $childStatus->status_text = $statusText;
            $childStatus->date = $this->currentDate;
            $childStatus->save();
        }
        $this->addToChildToDay($request->id_child, $groupId, $request->status_send);

        return response()->json(['status' => 'success'], 200);
    }

    public function addToChildToDay($id_child, $id_group, $type_of_attendance){
        $idOrg = $this->groups->where('id', $id_group)->pluck('id_org')->first();

        $currentDay = (int)Carbon::now()->format('d');
        $currentMonth = Carbon::now()->format('Y-m');
        $key = 'day_'.$currentDay;

        $checkExistingChildToDay = ChildToDay::where('id_child', $id_child)->where('month', $currentMonth.'-01')->first();

        $childToDay = ($checkExistingChildToDay) ? $checkExistingChildToDay : new ChildToDay();

        $childToDay->id_child = $id_child;
        $childToDay->id_group = $id_group;
        $childToDay->id_org = $idOrg;
        $childToDay->month = $currentMonth.'-01';
        $childToDay[$key] = $type_of_attendance;
        $childToDay->updated_at = $this->currentDate;
        $childToDay->created_at = $this->currentDate;
        $childToDay->save();

        return response()->json(['status' => 'success'], 200);
    }

    public function getChildsWithStatus(Request $request) {
        try {
            $user = auth()->user();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        $orgId = Organizations::where('bin', $user->email)->pluck('id')->first();
        $groupIds = Groups::where('id_org', $orgId)->pluck('id')->toArray();
        $projectCapacity = $this->childs->where('id_org', $orgId)->count();
        // $projectCapacity = Organizations::where('id', $orgId)->pluck('project_capacity')->first();
        $selectedGroup = $request->input('id_group');
        
        if ($selectedGroup == 0) {
            $childsByOrg = $this->childStatus->whereIn('id_group', $groupIds)->where('date', $this->currentDate);
            $childsAttended = $childsByOrg->where('status_send', 2)->count();
            $childsError = $childsByOrg->where('status_send', 3)->count();
            $childsSick = $childsByOrg->where('status_send', 4)->count();
            $childsWeekend = $childsByOrg->where('status_send', 5)->count();
            $childsNoReason = $childsByOrg->where('status_send', 6)->count();
            $childsMissed = $projectCapacity - $childsAttended - $childsSick - $childsNoReason - $childsWeekend; //- $childsError
            $childsAbsent = $childsSick + $childsNoReason + $childsWeekend;
        } else {
            $childsByOrg = $this->childStatus->where('id_group', $selectedGroup)->where('date', $this->currentDate);
            $childsAttended = $childsByOrg->where('status_send', 2)->count();
            $childsError = $childsByOrg->where('status_send', 3)->count();
            $childsSick = $childsByOrg->where('status_send', 4)->count();
            $childsWeekend = $childsByOrg->where('status_send', 5)->count();
            $childsNoReason = $childsByOrg->where('status_send', 6)->count();
            $groupCountPlace = $this->childs->where('id_group', $selectedGroup)->count();
            $childsMissed = $groupCountPlace - $childsAttended - $childsSick - $childsNoReason - $childsWeekend; //- $childsError
            $childsAbsent = $childsSick + $childsNoReason + $childsWeekend;
        }

        $childs = [
            'attended' => $childsAttended,
            'sick' => $childsSick,
            'weekend' =>$childsWeekend,
            'no_reason' => $childsNoReason,
            'absent' => $childsAbsent, 
            'missed' => $childsMissed,
            'error' => $childsError
        ];

        return $childs;
    }

    public function getChildsStatused(Request $request) {
        try {
            $this->user = auth()->user();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        $this->typeOfAttendance = $request->input('type_of_attendance');
        $selectedGroup = $request->input('id_group');

        if ( (int)$selectedGroup == 0) {
            $orgId = Organizations::where('bin', $this->user->email)->pluck('id')->first();
            $groupIds = Groups::where('id_org', $orgId)->pluck('id')->toArray();
            
            if ( $this->typeOfAttendance == 8 ) {
                $this->childsWithStatus = $this->childStatus->whereIn('id_group', $groupIds)->where('date', $this->currentDate)->where('status_send', '!=', 3)->pluck('id_child')->toArray();

                $childsByOrg = $this->childs->whereIn('id_group', $groupIds)->whereNotIn('id', $this->childsWithStatus)->map(function($child) {
                    $child->id_child = $child->id;
                    $child->time = "-";
                    return $child;
                });
            } else if ($this->typeOfAttendance == 7 ){    
                $childsByOrg = $this->childStatus->whereIn('id_group', $groupIds)->where('date', $this->currentDate)->where('status_send', '!=', 3);
            } else {
                $childsByOrg = $this->childStatus->whereIn('id_group', $groupIds)->where('date', $this->currentDate)->where('status_send', $this->typeOfAttendance);
            }
        } else {
            if ( $this->typeOfAttendance == 8) {
                $this->childsWithStatus = $this->childStatus->where('date', $this->currentDate)->where('status_send', '!=', 3)->pluck('id_child')->toArray();
                $childsByOrg = $this->childs->where('id_group', $selectedGroup)->whereNotIn('id', $this->childsWithStatus)->map(function($child) {
                        $child->id_child = $child->id;
                        $child->time = "-";
                        return $child;
                });
            } else if ( $this->typeOfAttendance == 7 ) {
                $childsByOrg = $this->childStatus->where('id_group', $selectedGroup)->where('date', $this->currentDate)->where('status_send', '!=', 3);
            } else {
                $childsByOrg = $this->childStatus->where('id_group', $selectedGroup)->where('date', $this->currentDate)->where('status_send', $this->typeOfAttendance);
            }
        }

        $childsByOrg->map(function($child) {
            if ($this->typeOfAttendance == 8) {
                $this->groupId = $child->id_group;

                $teacher = $this->teachers->filter(function ($teacher) {
                    $teachersGroupIds = json_decode($teacher->id_group, true);
                    
                    if ($teachersGroupIds == null) $teachersGroupIds = [];

                    if ( in_array($this->groupId, $teachersGroupIds)) {
                        return $teacher;
                    } 
                });
                
                if ($teacher) {
                    $teacherNames = $teacher->pluck('name')->toArray();
                } else {
                    $teacherNames = 'Нет воспитателя';
                }
            } else {
                $teacherNames = ($child->id_teacher) ? $this->teachers->where('id', $child->id_teacher)->pluck('name')->toArray() : $this->organizations->where('bin', $this->user->email)->pluck('admin_name')->toArray();
            }
            
            $groupName = $this->groups->where('id', $child->id_group)->pluck('name')->first();

            $child->id_child = $this->childs->where('id', $child->id_child)->pluck('name')->first();
            $child->id_group = ($groupName) ? $groupName : "Нет привязанных групп";
            $child->teachers = ($teacherNames) ? implode(', ', $teacherNames) : 'Нет привязанных воспитателей';
            if ( $child->time == "-") {
                $child->time = '';
            } else {
                $child->time = ($child->updated_at) ? $child->updated_at->format("H:i") : $child->created_at->format("H:i");
            }
            $child->save;
            return $child;
        });

        return $childsByOrg->values()->toArray();
    }

    public function import(Request $request)
    {

        try {
            $user = auth()->user();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        $orgId = Organizations::where('bin', $user->email)->pluck('id')->first();

        $file = $request->file('file');

        try {
             Excel::import(new ChildsImport($orgId), $file);
        } catch (\Exception $ex) {
            return "Таблица эксель заполнена не по формату";
        }
        
        return response()->json(['status' => 'success'], 200);
    }

    public function addChild (Request $request) {
        try {
            $user = auth()->user();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        $orgId = Organizations::where('bin', $user->email)->pluck('id')->first();

        $request->validate([
            'name' => 'required|string',
            'iin' => 'required|numeric',
            'gender' => 'required|numeric',
            'group' => 'required|numeric',
        ]);

        $child = new Childs;

        $child->name = $request->input('name');
        $child->iin = $request->input('iin');
        $child->id_org = $orgId;
        $child->gender = (int)$request->gender;
        $child->id_group = $request->input('group');
        $child->photo_exists = 0;
        $child->save();

        $childToDay = new ChildToDay;
        $childToDay->id_child = $child->id;
        $childToDay->id_group = $request->input('group');
        $childToDay->id_org = $orgId;
        $childToDay->month = Carbon::now()->format('Y-m')."-01";
        $childToDay->save();
        
        return response()->json(['status' => 'success'], 200);
    }

    public function deleteChild($id) {
        try {
            $user = auth()->user();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        $child = Childs::findOrFail($id);
        $childToDay = ChildToDay::where('id_child', $id)->get()->map(function($child) {
            $child->delete();
        });
        $childStatus = ChildStatus::where('id_child', $id)->get()->map(function($child) {
            $child->delete();
        });

        $child->delete();
        
        return response()->json(['status' => 'success'], 200);
    }

    public function updateChild(Request $request, $id) {
        try {
            $user = auth()->user();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        $request->validate([
            'gender' => 'required|numeric',
            'group_name' => 'required|numeric',
        ]);

        $this->request = $request;

        $childStatuses = ChildStatus::where('id_child', $id)->get()->map(function($childStatus) {
            if($childStatus !== null) {
                $childStatus->id_group = $this->request->group_name;
                $childStatus->save();
            } 
        });
        
        $childToDay = ChildToDay::where('id_child', $id)->get()->map(function($child) {
            $child->id_group = $this->request->group_name;
            $child->save();
            return $child;
        });
            
        $child = Childs::findOrFail($id);
        $child->gender = $request->gender;
        $child->id_group = $request->group_name;
        $child->save();
        
        
        return response()->json(['status' => 'success'], 200);
    }

    public function sendPhotoExist(Request $request) {
        $request->validate([
            'id_child' => 'required|numeric',
            'photo_exists' => 'required|numeric'
        ]);

        $child = $this->childs->where('id', $request->input('id_child'))->first();
        $child->photo_exists = $request->input('photo_exists');
        $child->save();

        return response()->json(['status' => 'success'], 200);
    }

    public function getGender ($gender) {
        return Gender::where('id', $gender)->pluck('gender')->first();
    }

    public function getAge($birth) {
        return Carbon::parse($birth)->diffInYears(Carbon::now());
    }

    public function getBirth($iin) {
        $year  = '20'.substr((string)$iin, 0, 2);
        $month = substr((string)$iin, 2, 2);
        $day   = substr((string)$iin, 4, 2);
        $timestamp = mktime(0, 0, 0, $month, $day, $year);
        $formattedDate =  date('d.m.Y', $timestamp);

        return $formattedDate;
    }

    public function getChildToDay(Request $request)
    {    
        try {
            $user = auth()->user();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        $this->orgId = Organizations::where('bin', $user->email)->pluck('id')->first();

        $year = 2024;
        $this->selectedMonth = Carbon::create($year, $request->month, 1);
        $selectedGroup = $request->group_id;
        $daysInMonth = $this->selectedMonth->daysInMonth;

        $headers = [
            'ФИО',
            'КОЛ-ВО ДНЕЙ ПРОПУСКА',
            'КОЛ-ВО ДНЕЙ БОЛЬНИЧНЫХ',
            'КОЛ-ВО ДНЕЙ ОТПУСКА',
            'ГРУППА',
        ];
        $keys = [
            'name',
            'group',
            'missing',
            'wekeend',
            'sick'
        ];
        $this->allAttendance =[
            'name' => 'Всего отсутствует детей',
            'iin' => '',
            'group' => 'Все',
        ];
        for ($day = 1; $day <= $daysInMonth; $day++) {
            $headers[] = $day;
            $keys[] = $day;
            $this->allAttendance[$day]['attendance'] = 0;
            $this->allAttendance[$day]['all'] = 1;
            $this->allAttendance[$day]['type_of_day'] = 1;
        }

        $this->allMissing = 0;
        $this->allSick    = 0;
        $this->allWeekend = 0;
        $this->allAttend  = 0;
        
        if ($selectedGroup == 0) {
            $attendances = ChildToDay::where('id_org', $this->orgId)->where('month', $this->selectedMonth)->get();
        } else {
            $attendances = ChildToDay::where('id_org', $this->orgId)->where('id_group', $selectedGroup)->where('month', $this->selectedMonth)->get();
        }

        $this->childKey = 1;
        $dataForTable = $attendances->map(function ($child, $key) {
                                        $childName = $this->childs->where('id', $child->id_child)->pluck('name')->first();
                                        $groupName = ($child->id_group) ? $this->groups->find($child->id_group)->name : "Нет группы";
                                        $countMissing = 0;
                                        $countSick    = 0;
                                        $countWeekend = 0;
                                        $countAttend  = 0;
                                        
                                        $key = $this->childKey;
                                        $this->childKey++;
                                        $child->id;

                                        foreach ($child->toArray() as $isDay => $factAttendance) {
                                            if (Str::contains($isDay, 'day')) {
                                                $dayOfMonth = sprintf("%02d", filter_var($isDay, FILTER_SANITIZE_NUMBER_INT));
                                                $currentDate = date('Y-m', strtotime($this->selectedMonth)).'-'.$dayOfMonth;
                                                
                                                if ( checkdate($this->selectedMonth->format('m'), $dayOfMonth, 2024) === true) {
                                                    $isHoliday = $this->isHoliday($currentDate);
                                                    $isWeekend = Carbon::parse($currentDate)->isWeekend();
                                                    
                                                    $attendance = 6;
                                                    if ($isHoliday || $isWeekend) {
                                                        $typeOfDay = 2;
                                                        $attendance = 6;
                                                    } else {
                                                        $typeOfDay = 1;
                                                    }
                                                    if (($factAttendance == 6 || $factAttendance == 16) && $typeOfDay !== 2){
                                                        $this->allMissing++;
                                                        $countMissing++;
                                                        $this->allAttendance[(int)Carbon::parse($currentDate)->format('d')]['attendance'] += 1;
                                                        $attendance = $factAttendance;
                                                    } else {
                                                        if ($factAttendance == 2 || $factAttendance == 12) {
                                                            $countAttend++;
                                                            $attendance = $factAttendance;
                                                        } else {
                                                            if ($factAttendance == 4) {
                                                                $this->allSick++;
                                                                $countSick++;
                                                                $attendance = 4;
                                                            } else if ($factAttendance == 5) {
                                                                $this->allWeekend++;
                                                                $countWeekend++;
                                                                $attendance = 5;
                                                            } else if ($factAttendance == 14) {
                                                                $this->allSick++;
                                                                $countSick++;
                                                                $attendance = 14;
                                                            } else if ($factAttendance == 15) {
                                                                $this->allWeekend++;
                                                                $countWeekend++;
                                                                $attendance = 15;
                                                            }
                                                        }
                                                    }
                                                    $childRow [(int)Carbon::parse($currentDate)->format('d')] = [
                                                        'type_of_day' => $typeOfDay,
                                                        'attendance' => $attendance
                                                    ];
                                                }
                                            }
                                        }
                                        
                                        $childRow['reason'] = [
                                            'type_of_day' => 1,
                                            'attendance' => 0,
                                        ];
                                        $childRow['id']    = $child->id_child;
                                        $childRow['name']    = $childName;
                                        $childRow['group']   = $groupName;
                                        $childRow['sick']    = $countSick;
                                        $childRow['wekeend'] = $countWeekend;
                                        $childRow['missing'] = $countMissing;
                                        
                                        return $childRow;
                                    })->toArray();
        $this->allAttendance['sick']    = $this->allSick;
        $this->allAttendance['wekeend'] = $this->allWeekend;
        $this->allAttendance['missing'] = $this->allMissing;
        $this->allAttendance['reason'] = [
            'type_of_day' => 1,
            'attendance' => 0,
        ];
        $dataForTable[] = $this->allAttendance;
        
        $result = [
            'headers' => $headers,
            'keys' => $keys,
            'data' => array_values($dataForTable)
        ];

        return $result;
    }

    public function isHoliday($date) : bool
    {
        foreach ($this->holidays as $holiday) {
            if (Carbon::parse($date)->format('d-m') == $holiday) {
                return true;
                break;
            }
        }

        return false;
    }
    
    public function changeChildStatus(Request $request) {
        try {
            $user = auth()->user();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        $childId = $request->input('children');
        $status = $request->input('status');
        $day = $request->input('day');
        $month = $request->input('month');
        $changedDate = Carbon::create($this->currentYear, $month, $day)->format('Y-m-d');
        $date = $this->currentYear.'-'.$month.'-01';

        $groupId = $this->childs->where('id', $childId)->pluck('id_group')->first();
        $checkExistingStatus = ChildStatus::where('id_child', $childId)->where('date', $changedDate)->first();
        $statusText = TypeOfStatus::all()->where('status_send', $status)->pluck('status_text')->first();

        if ($checkExistingStatus) {
            $checkExistingStatus->id_teacher = null;
            $checkExistingStatus->status_text = $statusText;
            $checkExistingStatus->status_send = $status;
            $checkExistingStatus->save();
        } else {
            $childStatus = new ChildStatus();
            $childStatus->id_child = $childId;
            $childStatus->id_group = $groupId;
            $childStatus->id_teacher = null;
            $childStatus->status_send = $status;
            $childStatus->status_text = $statusText;
            $childStatus->date = $changedDate;
            $childStatus->save();
        }

        try {
            $child = ChildToDay::where('id_child', $childId)->where('month', $date)->firstOrFail();
            $child['day_'.$day] = (int)str_replace("1", "", $status) + 10;
            $child->save();

        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return $e->getMessage();
        }

        return response()->json(['status' => 'success'], 200);
    }

    public function createChildToDayNextMonth() {
        $allChildToDay = ChildToDay::get(['id_child', 'id_group', 'id_org'])->map(function($childToDay) {

            $newMonthChildToDay = new ChildToDay([
                'id_child' => $childToDay->id_child,
                'id_group' => $childToDay->id_group,
                'id_org' => $childToDay->id_org,
                'month' => Carbon::now()->format('Y-m')."-01"
            ]);

            $newMonthChildToDay->save();
        });

        return 'Done';
    }

    public function sendChildsStatusAfterEndDate() {
        $childsWithStatus3 = ChildStatus::where('date', $this->currentDate)->where('status_send', 3)->get()->map(function($child) {
            $child->status_text = "Отсутствует без причины";
            $child->status_send = 6;
            $child->save();
            return $child;
        });

        $childsWithStatus = ChildStatus::where('date', $this->currentDate)->where('status_send', '!=', 6)->pluck('id_child')->toArray();

        $childs = Childs::whereNotIn('id', $childsWithStatus)->pluck('id_group', 'id');

        foreach ( $childs as $childId => $idGroup ) {
            $childStatus = new ChildStatus();
            $childStatus->id_child = $childId;
            $childStatus->id_teacher = null;
            $childStatus->id_group = $idGroup;
            $childStatus->status_text = "Отсутствует без причины";
            $childStatus->status_send = 6;
            $childStatus->date = $this->currentDate;
            $childStatus->save();
        }
    }

    public function checkSuccessStatus(Request $request) {
        $idChild = $request->input('id_person');
        
        $child = Childs::findOrFail($idChild);
        $name = $child->name;

        $statusedChild = ChildStatus::where('id_child', $idChild)->where('date', $this->currentDate)->get()->toArray();

        if ( $statusedChild != [] ) {
            $isTrue = false;
            foreach ($statusedChild as $child) {
                if ( $child['status_send'] != 3) {
                    return [
                        'name' => $name,
                        'time' => $this->getTimeOfStatus($idChild)
                    ];
                }  else {
                    $isTrue = 0;
                }
            }
            return $isTrue;
        } else {
            return 0;
        }
    }

    public function checkPhotoExists(Request $request) {
        $idChild = $request->input('id_person');
        
        $photoExisting = Childs::findOrFail($idChild);

        return $photoExisting->photo_exists;
    }
}
