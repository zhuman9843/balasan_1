<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use App\Models\Childs;
use App\Models\ChildToDay;
use App\Models\Groups;
use App\Models\GroupsAgeRanges;
use App\Models\Organizations;
use App\Models\TypeOfGroup;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\WithStartRow;

class ChildsImport implements ToModel, WithStartRow
{
    const START_ROW = 2;
    protected $orgId;
    protected $groups;
    protected $groupsAge;
    protected $typeGroups;
    protected $groupsImported = [];

    public function __construct($orgId)
    {
        $this->orgId = $orgId;
        $this->groups = Groups::where('id_org', $orgId)->pluck('id', 'name')->toArray();
        $this->typeGroups = TypeOfGroup::all();
        $this->groupsAge = GroupsAgeRanges::all();
    }

    public function model(array $row)
    {
        if ($row[0] != "") {
            # Name
            $name = mb_strtolower(($row[1].' '.$row[2]), 'UTF-8');
            $name = mb_convert_case($name, MB_CASE_TITLE, 'UTF-8');

            # Gender
            $row[3] = str_replace(' ', '', $row[3]);

            if ( $row[3] == 'Женский' ) {
                $gender = 2;
            } else if ( $row[3] == 'Мужской' ) {
                $gender = 1;
            } else {
                return "";
            }

            # IIN
            if (gettype($row[4]) == 'integer') {
                $row[3] = (string)$row[4];
            }

            if (strlen($row[4]) !== 12) {
                return  "";
            }

            # Groups 
            $row[6] = str_replace(' ', '', $row[6]);
            $groupId = '';

            if (in_array($row[6], array_keys($this->groups))) {
                $groupId = $this->groups[$row[6]];
            } else if (in_array($row[6], array_keys($this->groupsImported))) {
                $groupId = $this->groupsImported[$row[6]];
            } else {
                # Groups type
                if ($row[9]) {
                    try {
                        $typeGroup = $this->typeGroups->where('type_of_group', $row[9])->pluck('category')->first();
                    } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
                        return "В одной из групп неправильно заполнен режим";
                    }
                } else {
                    return "В одной из групп не заполнен режим";
                }

                # Groups Age Range
                if ($row[7]) {
                    try {
                        $age = $this->groupsAge->where('age', $row[7])->pluck('id')->first();
                    } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
                        return "В одной из групп неправильно заполнена возрастная группа";
                    }
                } else {
                    return "В одной из групп не заполнена возрастная группа";
                }

                $group = new Groups([
                    'name' => $row[6],
                    'id_org' => $this->orgId,
                    'count_place' => $row[8],
                    'type_group' => $typeGroup,
                    'age' => $age,
                    'location' => null,
                ]);

                $group->save();
                $groupId = $group->id;
                $this->groupsImported[$row[6]] = $groupId;
            }


            if ($groupId) {
                $child = new Childs([
                    'name' => $name,
                    'gender' => $gender,
                    'iin' => (int)$row[4],
                    'id_group' => $groupId,
                    'id_org' => $this->orgId,
                    'photo_exists' => 0
                ]);

                $child->save();
                
                $childToDay = new ChildToDay([
                    'id_child' => $child->id,
                    'id_group' => $groupId,
                    'id_org' => $this->orgId,
                    'month' => Carbon::now()->format('Y-m')."-01"
                ]);
                $childToDay->save();
                
                return [$child, $childToDay];
            }
        }
    }
    
    public function startRow(): int
    {
        return self::START_ROW;
    }
}
