<?php
namespace App\Imports;

use App\Models\Tarify;
use App\Models\Regions;
use App\Models\TypeLocation;
use App\Models\TypeOfGroup;
use App\Models\TypeRegions;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Illuminate\Contracts\Queue\ShouldQueue;

class TarifyImport implements ToModel, WithStartRow, WithBatchInserts, WithChunkReading, ShouldQueue
{
    const CHUNK = 1000;
    const START_ROW = 2;

    const COLUMNS = [
        'region' => 0,
        'group' => 1,
        'type_of_group' => 2,
        'type_region' => 5,
        'type_location' => 6,
        'tarify' => 7,
    ];

    public function model(array $row): ? Tarify
    {
        $shortRegion = explode(' ', $row[self::COLUMNS['region']])[0];

        if ( Regions::where('region', $shortRegion)->first() !== null ) {
            $region = Regions::where('region', $shortRegion)->first()->getAttribute('id');
            $group = Regions::where('region', $shortRegion)->first()->getAttribute('group_type');
            $type_of_group = TypeOfGroup::where('type_of_group', $row[self::COLUMNS['type_of_group']])->first()->getAttribute('id');
            $type_of_region = TypeRegions::where('type_region', $row[self::COLUMNS['type_region']])->first()->getAttribute('id');
            $type_of_location = TypeLocation::where('type_location', $row[self::COLUMNS['type_location']])->first()->getAttribute('id');

            return new Tarify([
                'region' => $region,
                'group_id' => $group,
                'type_of_group' => $type_of_group,
                'type_of_region' => $type_of_region,
                'type_of_location' => $type_of_location,
                'tarify' => $row[self::COLUMNS['tarify']] ?? 0,
            ]);
        } else {
            return null;
        }
    }

    public function startRow(): int
    {
        return self::START_ROW;
    }

    public function batchSize(): int
    {
        return self::CHUNK;
    }

    public function chunkSize(): int
    {
        return self::CHUNK;
    }
}
