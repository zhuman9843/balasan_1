<?php

namespace App\Imports;

use App\Models\Groups;
use App\Models\User;
use App\Models\Teachers;
use Maatwebsite\Excel\Concerns\ToModel;
use Carbon\Carbon;
use Faker\Core\Number;
use Maatwebsite\Excel\Concerns\WithStartRow;

class TeachersImport implements ToModel, WithStartRow
{
    const START_ROW = 2;

    protected $orgId;
    protected $users;
    protected $groups;
    protected $teachersIin;
    protected $excelIin;

    public function __construct($orgId)
    {
        $this->orgId = $orgId;
        $this->groups = Groups::where('id_org', $orgId)->pluck('id', 'name')->toArray();
        $this->teachersIin = Teachers::where('id_org', $orgId)->pluck('iin')->toArray();
        $this->excelIin = [];
    }

    public function model(array $row)
    {
        if ($row[0] != "") {
            
            if (gettype($row[3]) == 'integer') {
                $row[3] = (string)$row[3];
            }

            if (strlen($row[3]) !== 12) {
                return '';
            }

            if (gettype($row[4]) == 'integer') {
                $row[4] = (string)$row[4];
            }

            if (strlen($row[4]) !== 11) {
                return '';
            }

            if (!in_array($row[3], $this->teachersIin) && !in_array($row[3], $this->excelIin)) {
                $name = mb_strtolower(($row[1].' '.$row[2]), 'UTF-8');
                $name = mb_convert_case($name, MB_CASE_TITLE, 'UTF-8');

                if ( gettype($row[6]) == 'integer') {
                    $excelDate = ($row[6] - 25569) * 86400; 
                    $birthDate = date("Y-m-d", $excelDate);
                } else {
                    $birthDate = Carbon::createFromFormat('d.m.Y', $row[6])->format('Y-m-d');
                }
                
                $groupName = str_replace(' ', '', $row[7]);

                if (in_array($groupName, array_keys($this->groups))) {
                    $group = $this->groups[$groupName];
                    $groupId[] = $group;
                    $groupId = json_encode($groupId, true);
                } else {
                    $groupId = "[]";
                }

                $teacher = new Teachers([
                    'name' => $name,
                    'iin' => (int)$row[3],
                    'telephone' => (int)$row[4],
                    'email' => $row[5],
                    'birth_date' => $birthDate,
                    'id_org' => $this->orgId,
                    'id_group' => $groupId
                ]);

                array_push($this->excelIin, $row[3]);

                $user = new User();
                $user->email = $row[3];
                $user->role = 1;
                $user->password = bcrypt("123qweQ!");

                return [$teacher, $user];
            } else {
                return '';
            }
        }
    }

    public function startRow(): int
    {
        return self::START_ROW;
    }
}
