<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TableAnalyticUsers extends Model
{
    use HasFactory;

    protected $table = "table_analytic_users";

    protected $fillable = [ 'name', 'email', 'government_id', 'department_id' ];
}