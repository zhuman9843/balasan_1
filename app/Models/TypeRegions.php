<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypeRegions extends Model
{
    use HasFactory;

    protected $table = "type_region";

    protected $fillable = ["type_region"];
}
