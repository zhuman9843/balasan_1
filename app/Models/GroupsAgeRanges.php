<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GroupsAgeRanges extends Model
{
    use HasFactory;

    protected $table = "groups_age_range";

    protected $fillable = ["age"];
}
