<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Childs extends Model
{
    use HasFactory;

    protected $table = "childs";

    protected $fillable = ["name", "gender", "iin", "id_org", "id_group", "photo_exists"];
}
