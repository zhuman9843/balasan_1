<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employees extends Model
{
    use HasFactory;

    protected $table = "employees";

    protected $fillable = ["name", "id_org", "telephone", "iin", "birth_date", "email", "job", "created_at", "updated_at"];
}
