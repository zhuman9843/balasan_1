<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TableAnalytics extends Model
{
    use HasFactory;

    protected $table = "table_analytics";

    protected $fillable = [ 'child_amount_group', 'region', 'bin', 'government', 'department', 'type_of_department', 'month', 'contragent', 'child_amount_plan', 'child_amount_fact', 'plan_child_to_day', 'fact_child_to_day', 'missing', 'sick', 'weekend', 'budget_plan', 'budget_fact'];
}
