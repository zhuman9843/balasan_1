<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChildStatus extends Model
{
    use HasFactory;

    protected $table = "child_status";

    protected $fillable = ["id_child", "id_group", "status_text", "status_send", "date"];
}
