<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FilteredOrganizations extends Model
{
    use HasFactory;

    protected $table = "filtered_organizations";

    protected $fillable = ["id_manager", "bin", "org_name", "telephone", "added_date", "notes", "is_blocked"];
}
