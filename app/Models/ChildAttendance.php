<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChildAttendance extends Model
{
    use HasFactory;

    protected $table = "child_attendance";

    protected $fillable = ["id_child", "id_group", "attendance", "date"];
}
