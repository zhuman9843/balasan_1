<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GroupsController;
use App\Http\Controllers\TeachersController;
use App\Http\Controllers\ChildsController;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\LoginController;
use App\Http\Controllers\Api\RegisterController;
use App\Http\Controllers\Api\ForgotController;
use App\Http\Controllers\PlaceController;
use App\Http\Controllers\PropertiesController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\MobileUserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', [LoginController::class, 'index']);
Route::get('/registration', [RegisterController::class, 'index']);

Route::get('/get-all-groups', [GroupsController::class, 'getAllGroups']);
Route::put('/teachers/update/{id}', [TeachersController::class, 'update'])->name('teachers.update');
Route::get('/teachers', [TeachersController::class, 'index']);
Route::get('/get-teachers', [TeachersController::class, 'getTeachers']);

Route::get('/get-all-childs', [ChildsController::class, 'getAllChilds']);
Route::get('/get-group-childs', [ChildsController::class, 'getGroupChilds']);

Route::get('/get-department', [PlaceController::class, 'getDepartment']);
Route::get('/get-government', [PlaceController::class, 'getGovernment']);
Route::get('/get-districts', [PlaceController::class, 'getDistricts']);
Route::get('/get-regions', [PlaceController::class, 'getRegions']);

Route::get('/get-type-projects', [PropertiesController::class, 'getTypeProject']);
Route::get('/get-type-org', [PropertiesController::class, 'getTypeOrg']);
Route::get('/get-type-group', [PropertiesController::class, 'getTypeGroup']);
Route::get('/get-type-of_day', [PropertiesController::class, 'getTypeOfDay']);
Route::get('/get-type-of-attendances', [PropertiesController::class, 'getTypeOfAttendances']);
Route::get('/get-type-location', [PropertiesController::class, 'getTypeLocation']);
Route::get('/get-type-regions', [PropertiesController::class, 'getTypeRegions']);
Route::get('/get-gender', [PropertiesController::class, 'getGender']);
Route::get('/get-groups-age', [PropertiesController::class, 'getGroupsAgeRange']);
Route::get('/admin/get-filtered-organizations-name', [AdminController::class, 'getFilteredOrganizationsNames']);
Route::post('/send-problem', [PropertiesController::class, 'sendReportedProblem']);
Route::get('/cache-clear', [PropertiesController::class, 'cacheClear']);

Route::get('/get-type-project', [MobileUserController::class, 'emailByProject']);


Route::get('/{any?}', function (){
    return view('login.index');
})->where('any', '^(?!api\/)[\/\w\.-]*');