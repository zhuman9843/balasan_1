<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api;
use App\Http\Controllers\MobileUserController;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\GroupsController;
use App\Http\Controllers\PropertiesController;
use App\Http\Controllers\ChildsController;
use App\Http\Controllers\TeachersController;
use App\Http\Controllers\OrganizationsController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ChildsToDayController;
use App\Http\Controllers\TarifyController;
use App\Http\Controllers\EmployeesController;
use App\Http\Controllers\AdminController;


Route::post('login', [Api\AuthController::class, 'login']);
Route::post('register', [Api\RegisterController::class, 'register']);
Route::post('analytic-users-register', [Api\RegisterController::class, 'analyticUsersRegister']);
Route::post('forgot', [Api\ForgotController::class, 'forgot']);
Route::post('reset', [Api\ForgotController::class, 'reset']);
Route::post('reset-password', [Api\ForgotController::class, 'resetFromEmail']);
Route::get('email/resend/{user}', [Api\VerifyController::class, 'resend'])->name('verification.resend');

Route::group(['middleware' => ['auth:api']], function () {
    Route::post('/upload-pdf', [HomeController::class, 'uploadPdf']);
    Route::get('/extract-pdf-data', [HomeController::class, 'extractPdfData']);
    // Route::post('/send-error', [HomeController::class, 'sendErrorToMail']);
    Route::post('/send-error', [HomeController::class, 'sendErrorToDB']);
    
    Route::get('user', [Api\AuthController::class, 'user']);
    Route::get('/get-groups', [GroupsController::class, 'getGroups']);
    Route::post('/add-groups', [GroupsController::class, 'import']);
    Route::post('/create-group', [GroupsController::class, 'createGroup']);
    Route::get('/get-other-groups', [GroupsController::class, 'getOtherGroups']);
    Route::get('/get-other-groups-list', [GroupsController::class, 'getOtherGroupsList']);
    Route::get('/get-org-groups', [GroupsController::class, 'getOrgGroups']);
    Route::get('/get-groups-to-dashboard', [GroupsController::class, 'getGroupsToDashboard']);
    Route::put('/groups/update/{id}', [GroupsController::class, 'update'])->name('groups.update');
    Route::put('/groups/delete/{id}', [GroupsController::class, 'delete'])->name('groups.delete');
    Route::put('/groups/delete-teacher/{id}', [GroupsController::class, 'deleteTeacher'])->name('groups.delete_teacher');
    Route::put('/groups/delete-child/{id}', [GroupsController::class, 'deleteChild'])->name('groups.delete_child');
    Route::put('/groups/add-child/{id}', [GroupsController::class, 'addChild'])->name('groups.add_child');
    Route::put('/groups/add-teacher/{id}', [GroupsController::class, 'addTeacher'])->name('groups.add_teacher');

    Route::get('/get-teachers', [TeachersController::class, 'getTeachers']);
    Route::post('/add-teachers', [TeachersController::class, 'import']);
    Route::get('/get-other-teachers', [TeachersController::class, 'getOtherTeachers']);
    Route::put('/teachers/update/{id}', [TeachersController::class, 'update']);
    Route::put('/teachers/delete/{id}', [TeachersController::class, 'delete']);
    Route::put('/teachers/add-group/{id}', [TeachersController::class, 'addGroupToTeacher']);
    Route::put('/teachers/delete-group/{id}', [TeachersController::class, 'deleteGroupToTeacher']);
    Route::post('/teacher-change-password', [TeachersController::class, 'changePassword']);
    
    Route::get('/get-employees', [EmployeesController::class, 'getEmployees']);
    Route::put('/employees/delete/{id}', [EmployeesController::class, 'delete']);
    Route::put('/employees/update/{id}', [EmployeesController::class, 'update']);
    Route::post('/employees/import', [EmployeesController::class, 'import']);
    Route::post('/employees/change-password', [EmployeesController::class, 'changePassword']);
    Route::post('/employees/send-photo', [EmployeesController::class, 'sendPhotoExist']);
    Route::post('/employees/send-status', [EmployeesController::class, 'addEmployeeStatus']);
    Route::get('/employees/get-status', [EmployeesController::class, 'getEmployeeStatus']);
    Route::get('/get-employees-to-day', [EmployeesController::class, 'getEmployeesToDay']);
    Route::post('/employees/change-status', [EmployeesController::class, 'changeEmployeesStatus']);
    
    Route::get('/get-childs', [ChildsController::class, 'getChilds']);
    Route::get('/get-org-childs', [ChildsController::class, 'getOrgChilds']);
    Route::get('/get-childs-not-included', [ChildsController::class, 'getChildsNotIncluded']);
    Route::get('/get-child-to-day', [ChildsController::class, 'getChildToDay']);
    Route::get('/get-childs-with-status', [ChildsController::class, 'getChildsWithStatus']);
    Route::get('/get-childs-statused', [ChildsController::class, 'getChildsStatused']);
    Route::put('/organization/update/{id}', [OrganizationsController::class, 'update']);
    Route::post('/add-childs', [ChildsController::class, 'import']);
    Route::post('/childs/add-child', [ChildsController::class, 'addChild']);
    Route::put('/childs/delete/{id}', [ChildsController::class, 'deleteChild']);
    Route::put('/childs/update/{id}', [ChildsController::class, 'updateChild']);
    Route::post('/change-child-status', [ChildsController::class, 'changeChildStatus']);

    Route::post('/send-child-status', [ChildsController::class, 'addChildStatus']);
    Route::get('/get-child-status', [ChildsController::class, 'getChildStatus']);
    Route::post('/send-child-attendance', [ChildsController::class, 'addChildAttendance']);
    Route::get('/get-child-attendance', [ChildsController::class, 'getChildAttendance']);
    Route::post('/send-photo-exist', [ChildsController::class, 'sendPhotoExist']);
    Route::get('/create-child-to-day-next-month', [ChildsController::class, 'createChildToDayNextMonth']);
    Route::get('/create-employees-to-day-next-month', [EmployeesController::class, 'createEmployeesToDayNextMonth']);
    
    Route::post('/send-problem', [PropertiesController::class, 'sendReportedProblem']);
    Route::post('/teacher-register', [MobileUserController::class, 'registerTeacher']);
    Route::post('/employee-register', [MobileUserController::class, 'registerEmployee']);
    Route::get('/teacher-logout', [MobileUserController::class, 'logout']);
    Route::get('/get-errors', [MobileUserController::class, 'getErrors']);
   
    Route::get('/get-table-data', [ChildsToDayController::class, 'tableData']);
    Route::get('/get-tarify-data', [ChildsToDayController::class, 'tarifyData']);
    Route::get('/get-data-child-to-day', [ChildsToDayController::class, 'getChildsToDay']);
    Route::get('/get-data-export', [ChildsToDayController::class, 'getDataExport']);
    Route::get('/insert-to-table-analytics', [ChildsToDayController::class, 'insertChildToDay']);
    Route::post('/tarify-import', [TarifyController::class, 'import']);

    Route::get('/admin/get-filtered-organizations', [AdminController::class, 'getFilteredOrganizations']);
    Route::post('/admin/add-organization', [AdminController::class, 'addOrganization']);
    Route::post('/admin/organizations/update/{id}', [AdminController::class, 'updateOrganization']);
    Route::post('/admin/organizations/delete/{id}', [AdminController::class, 'deleteOrganization']);
    Route::get('/admin/get-questions', [AdminController::class, 'getQuestions']);
    Route::post('/admin/questions/update/{id}', [AdminController::class, 'updateQuestion']);
    Route::get('/admin/get-managers', [AdminController::class, 'getManagers']);
    Route::post('/admin/add-manager', [AdminController::class, 'addManager']);
    
    Route::get('/kernel', [ChildsController::class, 'sendChildsStatusAfterEndDate']);
    Route::get('/childs/check-status', [ChildsController::class, 'checkSuccessStatus']);
    Route::get('/childs/check-photo-exists', [ChildsController::class, 'checkPhotoExists']);
});

Route::post('mobile-login', [MobileUserController::class, 'login']);