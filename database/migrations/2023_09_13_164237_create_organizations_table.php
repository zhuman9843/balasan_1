<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrganizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organizations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('type_project');
            $table->string('name');
            $table->unsignedBigInteger('bin');
            $table->integer('region')->nullable();
            $table->integer('district')->nullable();
            $table->integer('type_org')->nullable();
            $table->integer('type_location')->nullable();
            $table->integer('type_region')->nullable();
            $table->integer('project_capacity')->nullable();
            $table->bigInteger('telephone');
            $table->string('admin_name');
            $table->string('location')->nullable();
            $table->string('address')->nullable();
            $table->string('email');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organizations');
    }
}
