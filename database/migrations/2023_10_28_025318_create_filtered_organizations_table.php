<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilteredOrganizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('filtered_organizations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_manager');
            $table->bigInteger('bin')->unique();
            $table->string('org_name');
            $table->string('telephone');
            $table->date('added_date');
            $table->string('notes')->nullable();
            $table->integer('is_blocked')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('filtered_organizations');
    }
}
