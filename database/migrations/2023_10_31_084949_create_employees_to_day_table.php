<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesToDayTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees_to_day', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_employee');
            $table->string('job');
            $table->unsignedBigInteger('id_org');
            $table->date('month');
            $table->integer('day_1')->default(6);
            $table->integer('day_2')->default(6);
            $table->integer('day_3')->default(6);
            $table->integer('day_4')->default(6);
            $table->integer('day_5')->default(6);
            $table->integer('day_6')->default(6);
            $table->integer('day_7')->default(6);
            $table->integer('day_8')->default(6);
            $table->integer('day_9')->default(6);
            $table->integer('day_10')->default(6);
            $table->integer('day_11')->default(6);
            $table->integer('day_12')->default(6);
            $table->integer('day_13')->default(6);
            $table->integer('day_14')->default(6);
            $table->integer('day_15')->default(6);
            $table->integer('day_16')->default(6);
            $table->integer('day_17')->default(6);
            $table->integer('day_18')->default(6);
            $table->integer('day_19')->default(6);
            $table->integer('day_20')->default(6);
            $table->integer('day_21')->default(6);
            $table->integer('day_22')->default(6);
            $table->integer('day_23')->default(6);
            $table->integer('day_24')->default(6);
            $table->integer('day_25')->default(6);
            $table->integer('day_26')->default(6);
            $table->integer('day_27')->default(6);
            $table->integer('day_28')->default(6);
            $table->integer('day_29')->default(6);
            $table->integer('day_30')->default(6);
            $table->integer('day_31')->default(6);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees_to_day');
    }
}
