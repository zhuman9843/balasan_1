<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypeOfStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('type_of_status')->insert([
            'status_send' => '0',
            'status_text' => 'Первоначальная иденфикация',
        ]);
        DB::table('type_of_status')->insert([
            'status_send' => '1',
            'status_text' => 'Ожидает отметки',
        ]);
        DB::table('type_of_status')->insert([
            'status_send' => '2',
            'status_text' => 'Успешно',
        ]);
        DB::table('type_of_status')->insert([
            'status_send' => '3',
            'status_text' => 'Ошибка обработки',
        ]);
        DB::table('type_of_status')->insert([
            'status_send' => '4',
            'status_text' => 'Болеет',
        ]);
        DB::table('type_of_status')->insert([
            'status_send' => '5',
            'status_text' => 'В отпуске',
        ]);
        DB::table('type_of_status')->insert([
            'status_send' => '6',
            'status_text' => 'Отсутствует без причины',
        ]);
    }
}
