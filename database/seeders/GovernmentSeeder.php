<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GovernmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('government')->insert([
            'government' => 'ГУ  "Управление образования Абайской области"',
            'region_id' => '1',
        ]);
        DB::table('government')->insert([
            'government' => 'ГУ  "Управление образования Акмолинской области"',
            'region_id' => '2',
        ]);
        DB::table('government')->insert([
            'government' => 'ГУ  "Управление образования Актюбинской области"',
            'region_id' => '3',
        ]);
        DB::table('government')->insert([
            'government' => 'ГУ  "Управление образования Алматинской области"',
            'region_id' => '4',
        ]);
        DB::table('government')->insert([
            'government' => 'ГУ  "Управление образования Атырауской области"',
            'region_id' => '5',
        ]);
        DB::table('government')->insert([
            'government' => 'ГУ  "Управление образования Восточно-Казахстанской области"',
            'region_id' => '6',
        ]);
        DB::table('government')->insert([
            'government' => 'ГУ  "Управление образования Жамбылской области"',
            'region_id' => '7',
        ]);
        DB::table('government')->insert([
            'government' => 'ГУ  "Управление образования Жетысуской области"',
            'region_id' => '8',
        ]);
        DB::table('government')->insert([
            'government' => 'ГУ  "Управление образования Западно-Казахстанской области"',
            'region_id' => '9',
        ]);
        DB::table('government')->insert([
            'government' => 'ГУ  "Управление образования Карагандинской области"',
            'region_id' => '10',
        ]);
        DB::table('government')->insert([
            'government' => 'ГУ  "Управление образования Костанайской области"',
            'region_id' => '11',
        ]);
        DB::table('government')->insert([
            'government' => 'ГУ  "Управление образования Мангистауской области"',
            'region_id' => '12',
        ]);
        DB::table('government')->insert([
            'government' => 'ГУ  "Управление образования Павлодарской области"',
            'region_id' => '13',
        ]);
        DB::table('government')->insert([
            'government' => 'ГУ  "Управление образования Северо-Казахстанской области"',
            'region_id' => '14',
        ]);
        DB::table('government')->insert([
            'government' => 'ГУ  "Управление образования Улытауской области"',
            'region_id' => '15',
        ]);
        DB::table('government')->insert([
            'government' => 'ГУ  "Управление образования Кызылординской области"',
            'region_id' => '16',
        ]);
        DB::table('government')->insert([
            'government' => 'ГУ  "Управление образования Туркестанской области"',
            'region_id' => '17',
        ]);
    }
}
