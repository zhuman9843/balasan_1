<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypeOfDaySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('type_of_day')->insert([
            'type_of_day' => 'Рабочий',
        ]);
        DB::table('type_of_day')->insert([
            'type_of_day' => 'Выходной',
        ]);
    }
}
