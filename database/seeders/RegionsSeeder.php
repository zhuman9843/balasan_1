<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RegionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('regions')->insert([
            'region' => 'Абайская',
            'group_type' => '1',
        ]);
        DB::table('regions')->insert([
            'region' => 'Акмолинская',
            'group_type' => '2',
        ]);
        DB::table('regions')->insert([
            'region' => 'Актюбинская',
            'group_type' => '2',
        ]);
        DB::table('regions')->insert([
            'region' => 'Алматинская',
            'group_type' => '1',
        ]);
        DB::table('regions')->insert([
            'region' => 'Атырауская',
            'group_type' => '1',
        ]);
        DB::table('regions')->insert([
            'region' => 'Восточно-Казахстанская',
            'group_type' => '2',
        ]);
        DB::table('regions')->insert([
            'region' => 'Жамбылская',
            'group_type' => '2',
        ]);
        DB::table('regions')->insert([
            'region' => 'Жетысуская',
            'group_type' => '1',
        ]);
        DB::table('regions')->insert([
            'region' => 'Западно-Казахстанская',
            'group_type' => '1',
        ]);
        DB::table('regions')->insert([
            'region' => 'Карагандинская',
            'group_type' => '2',
        ]);
        DB::table('regions')->insert([
            'region' => 'Костанайская',
            'group_type' => '2',
        ]);
        DB::table('regions')->insert([
            'region' => 'Мангистауская',
            'group_type' => '1',
        ]);
        DB::table('regions')->insert([
            'region' => 'Павлодарская',
            'group_type' => '2',
        ]);
        DB::table('regions')->insert([
            'region' => 'Северо-Казахстанская',
            'group_type' => '3',
        ]);
        DB::table('regions')->insert([
            'region' => 'Улытауская',
            'group_type' => '1',
        ]);
        DB::table('regions')->insert([
            'region' => 'Кызылординская',
            'group_type' => '4',
        ]);
        DB::table('regions')->insert([
            'region' => 'Туркестанская',
            'group_type' => '1',
        ]);
    }
}
