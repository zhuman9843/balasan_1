<?php

namespace Database\Seeders;

use App\Models\Department;
use App\Models\Districts;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $districts = Districts::all();

        foreach ( $districts as $district ) {
            $department = new Department();

            if ( strpos($district->district, 'г.') !== false ) {
                $dist = str_replace('г.', '', $district->district);
                $department->department = 'ГУ "Отдел образования по городу '.$dist.'"';
                $department->district_id = $district->id;
                $department->save();
            } else {
                if (
                    strpos($district->district, 'Биржан-сал') !== false || 
                    strpos($district->district, 'Тереңкөл') !== false || 
                    strpos($district->district, 'Аққулы') !== false 
                ) {
                    $dist = str_replace('район', 'району', $district->district);
                    $department->department = 'ГУ "Отдел образования по '.$dist.'"';
                    $department->district_id = $district->id;
                    $department->save();
                }
                if (strpos($district->district, 'ий') !== false) {
                    $dist = str_replace('ий', 'ому', $district->district);
                    $department->department = 'ГУ "Отдел образования по '.$dist.'y"';
                    $department->district_id = $district->id;
                    $department->save();
                }
            }
        }
    }
}
