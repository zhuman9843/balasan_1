<?php

namespace Database\Seeders;

use App\Models\Organizations;
use Illuminate\Database\Seeder;

class AddressSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $org = Organizations::where('name', 'КГКП "Ясли-сад «Асыл бөпе»')->first();
        $org->address = 'Улица Школьная, 18а​ с. Прапорщиково, Глубоковский район, Восточно-Казахстанская область';
        $org->save();
        $org = Organizations::where('name', 'КГКП «Детский сад «Ручеёк»')->first();
        $org->address = 'Улица Садовая, 38 с. Белоусовка, Глубоковский район, Восточно-Казахстанская область';
        $org->save();
    }
}
