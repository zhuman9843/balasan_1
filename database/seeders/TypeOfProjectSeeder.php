<?php

namespace Database\Seeders;

use App\Models\TypeProject;
use Illuminate\Database\Seeder;

class TypeOfProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    private $typeProjects = [
        'Образование', 'Спорт'
    ];

    public function run()
    {
        foreach ( $this->typeProjects as $typeProject ) {
            $typeP = new TypeProject();
            $typeP->project = $typeProject;
            $typeP->save();
        }
    }
}
