<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GroupsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('groups')->insert([
            'name' => 'Балдырған',
            'id_org' => '1',
            'count_place' => '20',
            'type_group' => 'gor0',
            'age' => '5',
            'location' => '51.140278, 71.481039',
        ]);
        DB::table('groups')->insert([
            'name' => 'Ботақан',
            'id_org' => '1',
            'count_place' => '20',
            'type_group' => 'gor0',
            'age' => '5',
            'location' => '51.140278, 71.481039',
        ]);
        DB::table('groups')->insert([
            'name' => 'Дарабоз',
            'id_org' => '1',
            'count_place' => '20',
            'type_group' => 'gor0',
            'age' => '5',
            'location' => '51.140278, 71.481039',
        ]);
        DB::table('groups')->insert([
            'name' => 'Қарлығаш',
            'id_org' => '1',
            'count_place' => '20',
            'type_group' => 'gor0',
            'age' => '5',
            'location' => '51.140278, 71.481039',
        ]);
        DB::table('groups')->insert([
            'name' => 'Көбелек',
            'id_org' => '2',
            'count_place' => '20',
            'type_group' => 'gor0',
            'age' => '5',
            'location' => '51.140278, 71.481039',
        ]);
        DB::table('groups')->insert([
            'name' => 'Аққу',
            'id_org' => '2',
            'count_place' => '20',
            'type_group' => 'gor0',
            'age' => '5',
            'location' => '51.140278, 71.481039',
        ]);
        DB::table('groups')->insert([
            'name' => 'Құлпынай',
            'id_org' => '2',
            'count_place' => '20',
            'type_group' => 'gor0',
            'age' => '5',
            'location' => '51.140278, 71.481039',
        ]);
        DB::table('groups')->insert([
            'name' => 'Айналайын',
            'id_org' => '2',
            'count_place' => '20',
            'type_group' => 'gor0',
            'age' => '5',
            'location' => '51.140278, 71.481039',
        ]);
    }
}
