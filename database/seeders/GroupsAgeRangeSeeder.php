<?php

namespace Database\Seeders;

use App\Models\GroupsAgeRanges;
use Illuminate\Database\Seeder;

class GroupsAgeRangeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $ages = [
        'от 1 года до 2 лет', 
        'от 2 до 3 лет', 
        'от 3 до 4 лет', 
        'от 4 до 5 лет', 
        'от 5 до 7 лет'
    ];

    public function run()
    {
        foreach ($this->ages as $age) {
            $ranges = new GroupsAgeRanges([
                'age' => $age
            ]);

            $ranges->save();
        }
    }
}
