<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GroupTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('region_group')->insert([
            'group_type' => 'Группа 1',
        ]);
        DB::table('region_group')->insert([
            'group_type' => 'Группа 2',
        ]);
        DB::table('region_group')->insert([
            'group_type' => 'Группа 3',
        ]);
        DB::table('region_group')->insert([
            'group_type' => 'Группа 4',
        ]);
    }
}
