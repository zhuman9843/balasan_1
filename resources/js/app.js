/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");

import App from './App.vue';
import store from "./vuex";
import axios from "./axios";
import { router } from './router';
import Toasted from "vue-toasted";
import Chart from "chart.js";
import Toast from "vue-toastification"
import "vue-toastification/dist/index.css"

window.Vue = require("vue").default;
Vue.use(Toasted);
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.use(Toast, {
  transition: "Vue-Toastification__bounce",
  maxToasts: 3,
  newestOnTop: true,
  position: "top-right",
  timeout: 3000,
  closeOnClick: true,
  pauseOnFocusLoss: true,
  pauseOnHover: false,
  draggable: true,
  draggablePercent: 0.7,
  showCloseButtonOnHover: false,
  hideProgressBar: false,
  closeButton: "button",
  icon: true,
  rtl: false,
})

Vue.component("app", require("./App.vue").default);

Vue.component("login", require("./views/login/index.vue").default);
Vue.component("registration", require("./views/registration/index.vue").default);

Vue.component("groups", require("./views/groups/index.vue").default);
Vue.component("group", require("./views/groups/Group.vue").default);
Vue.component("groupInfo", require("./views/groups/GroupInfo.vue").default);

Vue.component("teachers", require("./views/teachers/index.vue").default);
Vue.component("teachers", require("./views/teachers/index.vue").default);
Vue.component("teacher", require("./views/teachers/Teacher.vue").default);
Vue.component("teacherInfo", require("./views/teachers/TeacherInfo.vue").default);
Vue.component("teacherMobile", require("./views/teachers/TeacherMobile.vue").default);

Vue.component("employee", require("./views/employees/Employee.vue").default);
Vue.component("employeeInfo", require("./views/employees/EmployeeInfo.vue").default);

Vue.component("children", require("./views/children/index.vue").default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

new Vue({
    router,
    store,
    axios,
    render: h => h(App)
  }).$mount('#app');

// const app = new Vue({
//     store,
//     axios,
//     router,
//     el: "#app"
// });
