import chartOptionsChildToDay from "./chart_options_child_to_day.json";
import chartOptionsAttendanceChildToDay from "./chart_options_attendance_child_to_day.json";
import chartOptionsBudgetChildToDay from "./chart_options_budget_child_to_day.json";
import chartOptionsBudgetDifferenceChildToDay from "./chart_options_budget_difference_child_to_day.json";
import chartsOptionsReasonOfMissing from "./chart_options_reason_of_missing.json";
import VueApexCharts from 'vue-apexcharts';
import { BootstrapVue, BModal } from 'bootstrap-vue';
import Nav from "./../../components/Nav";
import * as XLSX from 'xlsx-js-style';
import { mapState } from "vuex";
import { mapGetters } from "vuex";

export default {
  name: 'ChildToDay',
  components: {
    Nav,
    XLSX,
    BootstrapVue,
    BModal,
    apexchart: VueApexCharts
  },
  data: () => ({
    chartOptionsChildToDay: chartOptionsChildToDay,
    chartOptionsAttendanceChildToDay: chartOptionsAttendanceChildToDay,
    chartOptionsBudgetChildToDay: chartOptionsBudgetChildToDay,
    chartOptionsBudgetDifferenceChildToDay: chartOptionsBudgetDifferenceChildToDay,
    chartsOptionsReasonOfMissing: chartsOptionsReasonOfMissing,
    fullscreenTimer: null,
    currentDayAttendance: '',
    isDragging: false,
    buttonText: 'Перетащите вверх',
    screenWidth: window.innerWidth,
    mainData: '',
    regions: [],
    groups: '',
    counter: 0,
    fetchedHeaders: '',
    fetchedRows: '',
    fetchedKeys: '',
    searchKindergarten: '',
    lastDownloadedDate: '',
    searchChild: '',
    kindergartenDate: '2024-01',
    selectedKindergarten: [],
    selectedGroup: 0,
    selectedTitle: 'Все области',
    clickedKindergarten: '',
    budgetData: '',
    clickedGroup: '',
    selectedRegion: 0,
    allKindergartenInClicked: [],
    amountOfTypeAttendance: [],
    typeOfAttendance: [],
    isKindergartenAttendance: false,
    isShowTable: false,
    isShowTarify: false,
    isFilter: false,
    isChartsChildToDay: false,
    isChartsAttendanceChildToDay: false,
    isChartsBudgetChildToDay: false,
    isChartsBudgetDifferenceChildToDay: false,
    selectedRowIndex: null,
    isExpanded: true,
    currentPage: 'amount',
    currentComponent: 'graph',
    itemId: null,
    childToDayFiltered: '',
    chartDataFiltered: '',
    chartAttendanceDataFiltered: '',
    listOfData: '',
    options: {},
    isDisable: true,
    startDate: '2024-01',
    endDate: '2024-12',
    listOfMonths: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
    listOfMonthKeys: {
      '0' : 0,
      '1' : 0,
      '2' : 0,
      '3' : 0,
      '4' : 0,
      '5' : 0,
      '6' : 0,
      '7' : 0,
      '8' : 0,
      '9' : 0,
      '10' : 0,
      '11' : 0,
    },
    filteredMonthKeys: {}
  }),
  watch: {
    'startDate': function() {
      this.fetchOptions()
    },
    'endDate': function() {
      this.fetchOptions()
    },
    'selectedRegion': function() {
      // if (this.selectedRegion == 0) {
      //   location.reload();
      // }
      this.fetchOptions()
    },
    'searchKindergarten': function() {
      this.fetchOptions()
    },
    'kindergartenMonth': function() {
      this.showTable(this.clickedKindergarten);
    },
    'searchChild': function() {
      this.showTable(this.clickedKindergarten);
    },
    'selectedGroup': function() {
      this.showTable(this.clickedKindergarten);
    }
  },
  computed:{
    ...mapState({
      isLoggedIn: "isLoggedIn"
    }),
    ...mapGetters(["user"]),
    isLargeScreen() {
      if (this.screenWidth > 1024) {
        return true;
      } else {
        return false;
      }
    },
    isMiddleScreen() {
      if (this.screenWidth < 1024) {
        return true;
      } else {
        return false;
      }
    },
    days() {
      let days = [];
      Object.values(this.fetchedHeaders).forEach ( item => {
        if ( typeof(item) == 'number') {
          days.push(item);
        }
      });
      return days;
    },
    arrayTreeObj() {
      let vm = this
      var newObj = [];
      vm.putInsideParentElement(vm.childToDay, newObj, 0, vm.itemId, true);
      return newObj;
    },
    chartsAttendanceChildToDay() {
      this.chartOptionsAttendanceChildToDay.chartOptions = {
        chart : {
          stacked : true
        },
        dataLabels: {
          enabled: true,
          style: {
            colors: ['#525f76']
          },
          formatter: (val) => {
            if (val) {
              val = val;
              return val.toLocaleString(undefined, {maximumFractionDigits: 1});
            }
          },
        },
        xaxis: {
          categories: this.months['names']
        },
        yaxis: {
          title: {
            text: "количество",
            style: {
              fontSize:  "12px",
              fontWeight:  600,
              fontFamily:  "Nunito",
              color:  "#263238"
            }
          },
          labels: { formatter: function (y) {
              if (y) return y.toLocaleString(undefined, {maximumFractionDigits: 1});
            }
          }
        }
      }

      let series = [];
      let sickMonthKeys = {};
      let weekendMonthKeys = {};
      let noReasonMonthKeys = {};

      Object.keys(this.filteredMonthKeys).forEach( key => {
        this.$set(sickMonthKeys, key, '0');
        this.$set(weekendMonthKeys, key, '0');
        this.$set(noReasonMonthKeys, key, '0');
      })
      let sickData = {
        'name' : 'Болеют',
        'data' : sickMonthKeys
      };
      let weekendData = {
        'name' : 'Отпуск',
        'data' : weekendMonthKeys
      };
      let noReasonData = {
        'name' : 'Пропуск',
        'data' : noReasonMonthKeys
      };

      for (let prop in this.chartAttendanceData) {
        let data = this.chartAttendanceData[prop].data;
        Object.entries(data).forEach( entry => {
          const [key, value] = entry;
          let month = key - 1;

          sickData.data[month] = value.sick;
          weekendData.data[month] = value.weekend;
          noReasonData.data[month] = value.missing;
        });
      }

      sickData.data = Object.values(sickData.data);
      weekendData.data = Object.values(weekendData.data);
      noReasonData.data = Object.values(noReasonData.data);
      series.push(sickData);
      series.push(weekendData);
      series.push(noReasonData);
      this.chartOptionsAttendanceChildToDay.series = series
      this.chartOptionsAttendanceChildToDay.chartOptions.colors = ['#EDA774', '#4F81BD', '#B1B1B7'];
      return this.chartOptionsAttendanceChildToDay;
    },
    chartsChildToDay() {
      this.chartOptionsChildToDay.chartOptions = {
        dataLabels: {
          enabled: true,
          style: {
            colors: ['#525f76']
          },
          formatter: (val) => {
            if (val) {
              val = val;
              return val.toLocaleString(undefined, {maximumFractionDigits: 1});
            }
          },
        },
        xaxis: {
          categories: this.months['names']
        },
        yaxis: {
          title: {
            text: "количество",
            style: {
              fontSize:  "12px",
              fontWeight:  600,
              fontFamily:  "Nunito",
              color:  "#263238"
            }
          },
          labels: { formatter: function (y) {
              if (y) return y.toLocaleString(undefined, {maximumFractionDigits: 1});
            }
          }
        }
      }

      let series = [];
      let planMonthKeys = {};
      let factMonthKeys = {};

      Object.keys(this.filteredMonthKeys).forEach( key => {
        this.$set(planMonthKeys, key, '0');
        this.$set(factMonthKeys, key, '0');
      })

      let monthPlanData = {
        'name' : 'План',
        'data' : planMonthKeys
      };
      let monthFactData = {
        'name' : 'Факт',
        'data' : factMonthKeys
      };

      for (let prop in this.chartData) {
        let data = this.chartData[prop].data;

        Object.entries(data).forEach( entry => {
          const [key, value] = entry;
          let month = key - 1;

          monthPlanData.data[month] = value.plan_child_to_day;
          monthFactData.data[month] = value.fact_child_to_day;
        });
      }
      
      monthPlanData.data = Object.values(monthPlanData.data);
      monthFactData.data = Object.values(monthFactData.data);

      series.push(monthPlanData);
      series.push(monthFactData);
      
      this.chartOptionsChildToDay.series = series;
      this.chartOptionsChildToDay.chartOptions.colors = ['#27AE60', '#D7C41E'];
      return this.chartOptionsChildToDay;
    },
    chartsBudgetChildToDay() {
      this.chartOptionsBudgetChildToDay.chartOptions = {
        chart : {
          stacked : false
        },
        dataLabels: {
          enabled: true,
          style: {
            colors: ['#525f76']
          },
          formatter: (val) => {
            if (val) {
              val = val;
              return val.toLocaleString(undefined, {maximumFractionDigits: 1});
            }
          },
        },
        xaxis: {
          categories: this.months['names']
        },
        yaxis: {
          title: {
            text: "тыс. тенге",
            style: {
              fontSize:  "12px",
              fontWeight:  600,
              fontFamily:  "Nunito",
              color:  "#263238"
            }
          },
          labels: { formatter: function (y) {
              if (y) return y.toLocaleString(undefined, {maximumFractionDigits: 1});
            }
          }
        }
      }
      let series = [];
      let planMonthKeys = {};
      let factMonthKeys = {};

      Object.keys(this.filteredMonthKeys).forEach( key => {
        this.$set(planMonthKeys, key, '0');
        this.$set(factMonthKeys, key, '0');
      })
      
      let monthPlanData = {
        'name' : 'План',
        'data' : planMonthKeys
      };
      let monthFactData = {
        'name' : 'Факт',
        'data' : factMonthKeys
      };

      for (let prop in this.chartData) {
        let data = this.chartData[prop].data;
        Object.entries(data).forEach( entry => {
          const [key, value] = entry;
          let month = key - 1;
          monthPlanData.data[month] = value.budget_plan / 1000;
          monthFactData.data[month] = value.budget_fact / 1000;
        });
      }
      monthPlanData.data = Object.values(monthPlanData.data);
      monthFactData.data = Object.values(monthFactData.data);
      series.push(monthPlanData);
      series.push(monthFactData);

      this.chartOptionsBudgetChildToDay.series = series;
      this.chartOptionsBudgetChildToDay.chartOptions.colors = ['#27AE60', '#D7C41E'];
      
      return this.chartOptionsBudgetChildToDay;
    },
    chartsBudgetDifferenceChildToDay() {
      this.chartOptionsBudgetDifferenceChildToDay.chartOptions = {
        dataLabels: {
          enabled: true,
          style: {
            colors: ['#525f76']
          },
          formatter: (val) => {
            if (val) {
              val = val;
              return val.toLocaleString(undefined, {maximumFractionDigits: 1});
            }
          },
        },
        xaxis: {
          categories: this.months['names']
        },
        yaxis: {
          title: {
            text: "тыс. тенге",
            style: {
              fontSize:  "12px",
              fontWeight:  600,
              fontFamily:  "Nunito",
              color:  "#263238"
            }
          },
          labels: { formatter: function (y) {
              if (y) return y.toLocaleString(undefined, {maximumFractionDigits: 1});
            }
          }
        }
      }
      let series = [];
      let diffMonthKeys = {};

      Object.keys(this.filteredMonthKeys).forEach( key => {
        this.$set(diffMonthKeys, key, '0');
      })
      
      let differenceData = {
        'name' : 'План - Факт',
        'data' : diffMonthKeys
      };
      for (let prop in this.chartData) {
        let data = this.chartData[prop].data;
        Object.entries(data).forEach( entry => {
          const [key, value] = entry;
          let month = key - 1;

          differenceData.data[month] = (value.budget_plan - value.budget_fact) / 1000;
        });
      }
      differenceData.data = Object.values(differenceData.data);
      series.push(differenceData);
      this.chartOptionsBudgetDifferenceChildToDay.series = series;
      this.chartOptionsBudgetDifferenceChildToDay.chartOptions.colors = ['#eda774'];
      
      return this.chartOptionsBudgetDifferenceChildToDay;
    },
    currentDate(){
      const currentDate = new Date();
      const year = currentDate.getFullYear();
      const month = (currentDate.getMonth() + 1).toString().padStart(2, '0');

      const formattedDate = `${year}-${month}`;
      return formattedDate;
    },
    kindergartenTableMonth() {
      const options = { month: 'long', year: 'numeric' };
      return (new Date(this.kindergartenDate)).toLocaleDateString('ru-RU', options);
    },
    currentMonth() {
      const options = { month: 'long', year: 'numeric' };
      return (new Date()).toLocaleDateString('ru-RU', options);
    },
    months() {
      const monthNames = [];
      const monthNumbers = [];
      const monthAttributes = [];
      
      let startDate = (new Date(this.startDate).getMonth() + 1);
      let endDate = (new Date(this.endDate).getMonth() + 1);

      for (let i = 0; i < new Date(this.endDate).getMonth() + 1 ; i++) {
        if ( startDate <= (i + 1) && i <= endDate) {
          monthNumbers.push((i + 1).toString().padStart(2, '0'));
          monthNumbers.push((i + 1).toString().padStart(2, '0'));
          monthNames.push(this.listOfMonths[i]);
          monthAttributes.push('План')
          monthAttributes.push('Факт')
        }
      }

      return {
        'numbers': monthNumbers,
        'names': monthNames,
        'attributes': monthAttributes
      }
    },
    lastMonth() {
      return this.months['numbers'].slice(-1)[0];
      // return this.months['numbers'][0] 
    },
    childToDay() {
      return this.childToDayFiltered;
    },
    chartData() {
      return this.chartDataFiltered;
    },
    chartAttendanceData() {
      return this.chartAttendanceDataFiltered;
    },
    kindergartenMonth() {
      let month = (new Date(this.kindergartenDate + '-01').getMonth() + 1).toString().padStart(2, '0');
      return month;
    },
    classOfAttendance() {
      if (this.typeOfAttendance == 'missing') {
        return 'missing_day';
      } else if (this.typeOfAttendance == 'sick') {
        return 'sick_day';
      } else if (this.typeOfAttendance == 'weekend') {
        return 'wekeend_day';
      }
    },
    amountDays(){
      let month = new Date(this.kindergartenDate).getMonth() + 1;
      let countDays = new Date(2024, month, 0).getDate();
      return countDays;
    },
    blindData(){
      Object.values(this.chartData);
    },
    currentDayAttend() {
      return this.currentDayAttendance;
    },
    chartsReasonOfMissing() {
      this.chartsOptionsReasonOfMissing.series = [this.currentDayAttend.missed, this.currentDayAttend.sick, this.currentDayAttend.weekend];
      this.chartsOptionsReasonOfMissing.chartOptions = {
        plotOptions: {
          pie: {
            donut: {
              size: '80%',
              labels: {
                show: true,
                total: {
                  show: true,
                  showAlways: true,
                  label: 'Всего:',
                  fontSize: 32,
                  color: '#666666',
                  fontWeight: '700',
                }
              }
            }
          }
        },
        chart: {
          type: 'donut',
        },
        dataLabels: {
          enabled: false,
        },
        colors: ['#B1B1B7', '#EDA774', '#4F81BD'],
        labels: ["Пропуск", "Больничный", "Отпуск"],
        legend: {
          show: false
        },
        responsive: [{
          breakpoint: 480,
          options: {
            chart: {
              width: '100%'
            },
            legend: {
              show: false
            }
          },
        }]
      }
      return this.chartsOptionsReasonOfMissing;
    },
    exactlyCurrentDate() {
      const date = new Date();
      const day = String(date.getDate()).padStart(2, '0');
      const month = String(date.getMonth() + 1).padStart(2, '0');
      const year = String(date.getFullYear());
      return `${day}.${month}.${year}`;
    }
  },
  methods: {
    updatePage() {

    },
    updateScreenWidth() {
      this.screenWidth = window.innerWidth;
    },
    selectGroup(event) {
      this.selectedGroup = event.target.value;

      if ( this.selectedGroup == 0) {
        this.selectedGroupInfo.name = "Все группы"
        this.selectedGroupInfo.type_group = "Все возможные режимы"
      }
      this.selectedGroupInfo = Array.from(this.groups).filter((group) => {
          return group.id == event.target.value;
      })[0];
    },
    async getGroups(bin) {
      const res = await axios.get('/api/get-groups-to-dashboard?bin=' + bin);
      this.groups = res.data;
    },
    async updateAttendance() {
      location.reload();
    },
    exportTableToExcel(){
      var wb = XLSX.utils.book_new();
      var ws = XLSX.utils.table_to_sheet(document.querySelector('.table_child'));
      document.querySelector('.hide_none').classList.add('show_table');

      ws['!cols'] = [{ width: 5 },{ width: 40 }, { width: 15 }, { width: 15 }, { width: 15 }, { width: 15 }];
      ws['!margins'] = { top: 1.25, bottom: 1.25, header: 1.5, footer: 0.75 };
      
      const border = { 
        top: { style: 'thin', color: 'black' },
        bottom: { style: 'thin', color: 'black' },
        left: { style: 'thin', color: 'black' },
        right: { style: 'thin', color: 'black' }
      };

      const borderBottom = {
        bottom: { style: 'thin', color: 'black' }
      };
      
      const alignment = {
        vertical: 'center',
        horizontal: 'center',
        wrapText: true
      };
      Object.keys(ws).forEach((cell) => {
        if (cell !== '!ref' && cell !== '!fullref' && cell !== '!merges' && cell !== '!margins') {
          const style = ws[cell].s || {};
          if (ws[cell].v == '..' ) {
            style.border = borderBottom;
          } else {
            style.border = border;
          }
          style.alignment = alignment;
          style.padding = { top: 5, bottom: 5, left: 5, right: 5 };
          ws[cell].s = style;

          if ( ws[cell].v == '...' || ws[cell].v == '..') {
            ws[cell].v = ''
          }
        }
      });

      ws['B2'].s = { border: {}, alignment : { vertical: 'center', horizontal: 'center', wrapText: true }, font: { bold: true, underline: true }};
      ws['B3'].s = { border: {}, alignment : { vertical: 'top', horizontal: 'center', wrapText: true }, font: { sz: 9, italic: true }};
      ws['B6'].s = { border: {}, alignment : { vertical: 'center', horizontal: 'center', wrapText: true }, font: { bold: true }};

      XLSX.utils.book_append_sheet(wb, ws, 'Табель');
      let excel = XLSX.writeFile(wb, 'Табель посещаемости.xlsx');
      document.querySelector('.show_table').remove('show_table');
      this.$toast.success("Табель посещаемости успешно загрузился!");
      return excel;

    },
    exportTarifyToExcel(){
      var wb = XLSX.utils.book_new();
      var ws = XLSX.utils.table_to_sheet(document.querySelector('.table_tarify'));

      ws['!cols'] = [{ width: 20 },{ width: 15 }, { width: 10 }, { width: 15 }, { width: 15 }, { width: 15 }, { width: 15 }];
      ws['!margins'] = { top: 1.25, bottom: 1.25, header: 1.5, footer: 0.75 };
      
      const border = { 
        top: { style: 'thin', color: 'black' },
        bottom: { style: 'thin', color: 'black' },
        left: { style: 'thin', color: 'black' },
        right: { style: 'thin', color: 'black' }
      };

      const borderBottom = {
        bottom: { style: 'thin', color: 'black' }
      };
      
      const alignment = {
        vertical: 'center',
        horizontal: 'center',
        wrapText: true
      };

      Object.keys(ws).forEach((cell) => {
        if (cell !== '!ref' && cell !== '!fullref' && cell !== '!merges' && cell !== '!margins') {
          const style = ws[cell].s || {};
          if (ws[cell].v == '..' ) {
            style.border = borderBottom;
          } else {
            style.border = border;
          }
          
          if ( ws[cell].v == '...' ) {
            ws[cell].v = ''
          }

          style.alignment = alignment;
          style.padding = { top: 5, bottom: 5, left: 5, right: 5 };
          ws[cell].s = style;
        }
      });

      ws['B2'].s = { border: {}, alignment : { vertical: 'center', horizontal: 'left', wrapText: true }, font: { bold: true }};
      ws['B3'].s = { border: {}, alignment : { vertical: 'top', horizontal: 'left', wrapText: true }, font: { bold: true}};
      ws['B4'].s = { border: {}, alignment : { vertical: 'center', horizontal: 'left', wrapText: true }, font: { bold: true }};
      ws['B5'].s = { border: {}, alignment : { vertical: 'center', horizontal: 'left', wrapText: true }, font: { bold: true }};
      
      ws['B7'].s = { border: {}, alignment : { vertical: 'center', horizontal: 'center', wrapText: true }, font: { bold: true }};
      ws['B9'].s = { border: {}, alignment : { vertical: 'center', horizontal: 'center', wrapText: true }, font: { bold: true }};

      XLSX.utils.book_append_sheet(wb, ws, 'Расчет бюджета');
      let excel = XLSX.writeFile(wb, 'Таблица расчета бюджета.xlsx');
      this.$toast.success("Таблица расчета бюджета успешно загрузился!")
      return excel;
    },
    showAttendance(type) {
      this.typeOfAttendance = type;
      this.allKindergartenInClicked = [];
      this.amountOfTypeAttendance = [];
      let startDate = (new Date(this.startDate).getMonth() + 1);
      let endDate = (new Date(this.endDate).getMonth() + 1);

      if (this.clickedGroup) {
        this.getKindergarten(this.clickedGroup);
      } else {
        Object.values(this.chartData).forEach(item => {
          this.getKindergarten(item);
        })
      }

      this.allKindergartenInClicked.forEach(kingarten => {
        let filteredByMonth = Object.keys(kingarten.data).filter(function(key) {
                                      if (startDate <= key && key <= endDate) return true;
                                    })
                                    .reduce((obj, key) => {
                                      obj[key] = kingarten.data[key];
                                      return obj;
                                    }, {});
        kingarten.data = filteredByMonth;
      })
      Object.values(this.allKindergartenInClicked).forEach(kindergarten => {
        let sum = 0;
        for (let key in kindergarten.data) {
          if (kindergarten.data[key][type]) {
            sum += kindergarten.data[key][type];
          }
        }
        this.amountOfTypeAttendance.push({
          'name': kindergarten.name,
          'sum': sum
        })
      });
      this.isKindergartenAttendance = true;
    },
    closeAttendance(){
      this.isKindergartenAttendance = false;
    },
    getKindergarten(item) {
      if((Object.keys(item)).includes('child')) {
        Object.values(item.child).forEach( insideItem => {
          this.getKindergarten(insideItem);
        });
      } else {
        this.allKindergartenInClicked.push(item);
      }
    },
    tableHeaderStyle(header){
      if (header == 'ФИО') {
        return "width: 15%;"
      } else if (typeof(header) == 'string') {
        return "width: 3%;"
      } else if (typeof(header) == 'number') {
        return "width: 2% !important;"
      }
    },
    headerDay() {
      return 'colspan:' + this.amountDays;
    },
    dayClass(dayType) {
      if (dayType.type_of_day == 2) {
        return "relax_day";
      }
    },
    attendanceClass(attendance) {
      if (attendance.attendance == 6 && attendance.type_of_day != 2) {
        return "missing_day";
      } else if (attendance.attendance == 4) {
        return "sick_day";
      } else if (attendance.attendance == 5) {
        return "wekeend_day";
      } else if (attendance.attendance == 14) {
        return "edited_sick_day";        
      } else if (attendance.attendance == 15) {
        return "edited_wekeend_day";
      } else if (attendance.attendance == 16) {
        return "edited_missing_day";
      } else if (attendance.attendance == 12) {
        return "edited_attended_day";
      } else{
        return "attended_day";
      }
    },
    async showTable(item){
      if ( !item.child ){
        if (this.currentPage === 'budget') {
          this.isShowTarify = true;
          const tarify = await axios.get('api/get-tarify-data?bin=' + item.bin + '&month=' + this.kindergartenMonth);
          this.budgetData = tarify.data;
        } else if (this.currentPage === 'amount'){
          this.isShowTable = true;
          this.getGroups(item.bin);
          const table = await axios.get('api/get-table-data?bin=' + item.bin + '&month=' + this.kindergartenMonth + '&group=' + this.selectedGroup);
          this.fetchedHeaders = table.data.headers;
          this.fetchedKeys = table.data.keys;
          
          if (this.searchChild) {
            const filteredChild = Object.entries(table.data.data).reduce((acc, [key, value]) => {
              if ( value.name.toLowerCase().includes(this.searchChild.toLowerCase())) {
                acc[key] = value;
              }
              return acc;
            }, {});
            this.fetchedRows = filteredChild
          } else {
            this.fetchedRows = table.data.data;
          }
        }
      }
    },
    closeTarify() {
      this.isShowTarify = false;
    },
    closeTable() {
      this.isShowTable = false;
    },
    onChange(event) {
      this.selectedRegion = event.target.value;
    },
    async getRegions(){
      const res = await axios.get('/get-regions');

      if (this.user.government_id !== 0) {
        Object.values(res.data).forEach(region => {
          if (region.id == this.user.government_id) {
            this.regions.push(region);
            this.selectedRegion = region.id;
          }
        });
      } else {
        this.regions = res.data
      }
    },
    showModal(chart) {
      if ( chart == 'isChartsChildToDay') {
        this.isChartsChildToDay = true;
      } else if ( chart == 'isChartsAttendanceChildToDay' ) {
        this.isChartsAttendanceChildToDay = true;
      } else if ( chart == 'isChartsBudgetChildToDay' ) {
        this.isChartsBudgetChildToDay = true;
      } else if ( chart == 'isChartsBudgetDifferenceChildToDay' ) {
        this.isChartsBudgetDifferenceChildToDay = true;
      }
    },
    closeModal(chart) {
      if ( chart == 'isChartsChildToDay') {
        this.isChartsChildToDay = false;
      } else if ( chart == 'isChartsAttendanceChildToDay' ) {
        this.isChartsAttendanceChildToDay = false;
      } else if ( chart == 'isChartsBudgetChildToDay' ) {
        this.isChartsBudgetChildToDay = false;
      } else if ( chart == 'isChartsBudgetDifferenceChildToDay' ) {
        this.isChartsBudgetDifferenceChildToDay = false;
      }
    },
    async exportToExcel() {
      const res = await axios.get('api/get-data-export?start_date=' + this.startDate + '-01&end_date=' + this.endDate + '-01&government=' + this.selectedRegion + '&page=' + this.currentPage);
      this.dataToExport = res.data;
      console.log(this.dataToExport);
      var wb = XLSX.utils.table_to_book(document.querySelector(".c_d_table"));
      var ws = XLSX.utils.json_to_sheet(this.dataToExport);
      let prop = {'!rows': [], '!cols' : []}
      let worksheet = Object.assign(ws,prop);
      wb.Sheets.Sheet1 = worksheet;
      var wscols = [{wch:50}, {wch:-1}, {wch:15}, {wch:15}, {wch:15}, {wch:15}, {wch:15}, {wch:15}, {wch:15}, {wch:15}, {wch:15}, {wch:15}, {wch:15}, {wch:15}, {wch:15}, {wch:15}, {wch:15}, {wch:15}, {wch:15}];
      var range = XLSX.utils.decode_range(wb.Sheets.Sheet1['!ref']);
      wb.Sheets.Sheet1["!cols"] = wscols;
      for (let R = range.s.r; R <= range.e.r; ++R) {
        let row = {          
          level: 0
        }
        wb.Sheets.Sheet1['!rows'].push(row);
        for (let C = range.s.c; C <= range.e.c; ++C) {
              var cell = { c: C, r: R };
              var cell_ref = XLSX.utils.encode_cell(cell);
              if(wb.Sheets.Sheet1[cell_ref]){
                wb.Sheets.Sheet1[cell_ref].s = {
                  font: {
                    name: "Calibri",
	                  sz: 10,
	                  color: { rgb: "000000" },
	                },
                  border:{
                    top:{ style: "thin", color: { rgb: "black" }},
                    bottom:{ style: "thin", color: { rgb: "black" }},
                    left:{ style: "thin", color: { rgb: "black" }},
                    right:{ style: "thin", color: { rgb: "black" }},
                  },
                  alignment:{
                    vertical: "center",
                    horizontal: "center",
                    wrapText: true
                  }
                }
              }         
              if(C == 1 && wb.Sheets.Sheet1['!rows'][R]){
                wb.Sheets.Sheet1['!rows'][R].level = wb.Sheets.Sheet1[cell_ref].v; 
              }                                                    
          }
      }
      if (this.currentPage == 'amount') {
        return XLSX.writeFile(wb, `Таблица по посещениям.xlsx`);
      } else if( this.currentPage == 'budget') {
        return XLSX.writeFile(wb, `Таблица по бюджету.xlsx`);
      }
    },
    formatter(item, monthNumber, index) {
      if (item.data[monthNumber]) {
        if (index % 2 === 0) {
          return (this.currentPage == 'amount') ? item.data[monthNumber].plan_child_to_day.toLocaleString(undefined, {maximumFractionDigits: 1}) : (item.data[monthNumber].budget_plan / 1000).toLocaleString(undefined, {maximumFractionDigits: 1});
        } else {
          return (this.currentPage == 'amount') ? item.data[monthNumber].fact_child_to_day.toLocaleString(undefined, {maximumFractionDigits: 1}) : (item.data[monthNumber].budget_fact / 1000).toLocaleString(undefined, {maximumFractionDigits: 1});
        }
      }
    },
    classOfAttribute(item, index){
      if (index % 2 === 0) {
        return "child_to_day_month_plan_"+item.level;
      } else {
        return "child_to_day_month_fact_"+item.level;
      }
    },
    budgetGraph() {
      if (this.currentPage == 'budget') {
        return 'child_to_day_select_graph_clicked';
      } else {
        return '';
      }
    },
    amountGraph() {
      if (this.currentPage == 'amount') {
        return 'child_to_day_select_graph_clicked';
      } else {
        return '';
      }
    },
    graphClass() {
      if (this.currentComponent == 'graph') {
        return 'child_to_day_select_graph_clicked';
      } else {
        return '';
      }
    },
    tableClass() {
      if (this.currentComponent == 'table') {
        return 'child_to_day_select_graph_clicked';
      } else {
        return '';
      }
    },
    changeBudgetToAmount(page) {
      this.currentPage = page;
    },
    changeTableToGraph(component) {
      this.currentComponent = component;
    },  
    putInsideParentElement(obj, newObj, level, itemId, isExpend) {
      let vm = this;
      if(obj){
        for (let prop in obj) {
          let o = obj[prop];
          if (o.child) {
            o.level = level;
            o.leaf = false;              
            if (o.id == itemId) {
              o.expend = isExpend;
            }
            newObj.push(o);
            if (o.expend == true) {
              vm.putInsideParentElement(o.child, newObj, o.level + 1, itemId, isExpend);
            }
          } else {
            o.level = level;
            o.leaf = true;
            newObj.push(o);
          }
        }
      }
    },
    classByLevel(item, index){
      if (index === this.selectedRowIndex) {
        return "clicked_row";
      }
      
      for(let i = 0; i < 8; i++){
        if (item.level == i) {
          return "table_row_level_"+i;
        }
      }      
    },
    setClassOnclick(item, index, event) {
      this.clickedGroup = item;
      this.clickedKindergarten = item;
      if (this.clickedGroup) {
        if ( this.selectedRowIndex !== index) {
          let circleElement = event.target.closest('table').querySelector(".green-circle");
          if (circleElement) {
            circleElement.remove();
          }
          const clickedRows =  event.target.closest('table').querySelectorAll(".clicked_row");

          if (clickedRows) {
            clickedRows.forEach( td => {
              td.classList.remove("clicked_row");
            });
          }
        }
        this.selectedTitle = event.currentTarget.querySelector(".child_to_day_table_name_title").textContent;

        const trElement =  event.target.closest('tr');
        const tds = trElement.querySelectorAll('td');
        tds.forEach( td => {
          td.classList.add("clicked_row");
        });

        this.selectedRowIndex = index;
        const firstTdElement = event.currentTarget.querySelector("td:first-child");
        let circleElement = firstTdElement.querySelector(".green-circle");

        if (!circleElement) {
          circleElement = document.createElement("div");
          circleElement.classList.add("green-circle");
          circleElement.style.width = "10px";
          circleElement.style.height = "10px";
          circleElement.style.borderRadius = "50%";
          circleElement.style.backgroundColor = "green";
          firstTdElement.appendChild(circleElement);
        }
      }
    },
    toggle(item) {
      let vm = this;
      vm.itemId = item.id;
      this.clicked(item);
      let key = item.name
      this.chartDataFiltered = {};
      this.chartAttendanceDataFiltered = {};
      this.chartData[key] = item;
      this.chartAttendanceDataFiltered[key] = item;

      item.leaf = false;
      if (
        item.leaf == false &&
        item.expend == undefined &&
        item.child != undefined
      ) {
        if (item.child.length != 0) {
          vm.putInsideParentElement(item.child, [], item.level + 1, item.id, true);
        }
      }


      if (item.expend == true && item.child != undefined) {
        var __subindex = 0;
        for (let key in item.child) {
          item.child[key].expend = false;
        };

        vm.$set(item, "expend", undefined);
        vm.$set(item, "leaf", false);
        vm.itemId = null;
      }
    },
    setPadding(item) {
      return `padding-left: ${item.level * 35}px;`;
    },
    iconName(item) {
      if (item.expend == true) {
        return "arrow-up";
      } 

      if ( item.expend == undefined && item.child && item.child.length == 0) {
        return "done"
      }
      
      if (!item.child) {
        return "done";
      }

      return "arrow-down";
    },
    monthFormatter(month){
      const dateParts = month.split('.');
      const date = new Date(dateParts[2], dateParts[1], dateParts[0]);
      const options = { month: 'long', timeZone: 'UTC', locale: 'ru' };
      return (new Intl.DateTimeFormat('ru-RU', options).format(date)).toLocaleUpperCase();
    },
    clicked(item) {
      if (item.id == this.itemId) {
        return "clicked_row"
      }
    },
    async updateData() {
      try {
        this.mainData = await axios.get('api/insert-to-table-analytics?start_date=' + this.startDate + '-01&end_date=' + this.endDate + '-01');
      } catch {
        location.reload();
      }
    },
    async getData() {
      this.mainData = await axios.get('api/get-data-child-to-day?start_date=' + this.startDate + '-01&end_date=' + this.endDate + '-01&government=' + this.selectedRegion);
      
      let filteredBySearch;
      if(this.searchKindergarten != '') {
        this.selectedKindergarten = [];
        Object.values(this.mainData.data).forEach( item => {
          this.checkInsideElement(item);
        })
        filteredBySearch = this.selectedKindergarten;
      } else {
        filteredBySearch = this.mainData.data;
      }

      this.childToDayFiltered = filteredBySearch;
      this.chartDataFiltered = filteredBySearch;
      this.chartAttendanceDataFiltered = filteredBySearch;
    },
    async fetchOptions() {
      let startKey = new Date(this.startDate).getMonth();
      let endKey = new Date(this.endDate).getMonth();

      this.filteredMonthKeys = {};

      Object.keys(this.listOfMonthKeys).forEach( key => {
        if ( startKey <= key  && key <= endKey) {
          this.$set(this.filteredMonthKeys, key, this.listOfMonthKeys[key]);
        }
      })

      this.clickedGroup = '';
      await this.updateData();
      this.getData();
    },
    checkInsideElement(item) {
      if (Object.keys(item).length != 0 && Object.values(item)[0].length != 0) {
        Object.entries(item.child).reduce((acc, [key, value]) => {
          if (value['child']) {
            Object.keys(value['child']).forEach(key =>{
              this.checkInsideElement(value['child'][key]);
            })
          } else {
            if ( value.name.toLowerCase().includes(this.searchKindergarten.toLowerCase()) ) {
              this.selectedKindergarten.push(value);
            }
          }
        }, {});
      }
    }
  },
  async created() {
    this.endDate = this.currentDate;
    this.getRegions();
    await this.updateData();
    this.getData();
    window.addEventListener('resize', this.updateScreenWidth);
  },
  beforeUnmount() {
    window.removeEventListener('resize', this.updateScreenWidth);
  },
}