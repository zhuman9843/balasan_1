import Vue from "vue";
import Router from "vue-router";
import store from "./vuex";

Vue.use(Router);

export const router = new Router({
    mode: "history",
    routes: [
        {
            path: "/",
            name: "home",
            component: () => import("./views/home/index.vue"),
            meta: {
                requiresAuth: true
            }
        },
        {
            path: "/profile",
            name: "profile",
            component: () => import("./views/profile/index.vue"),
            meta: {
                requiresAuth: true
            }
        },
        {
            path: "/profile-settings",
            name: "profile-settings",
            component: () => import("./views/profile/ProfileSettings.vue"),
            meta: {
                requiresAuth: true
            }
        },
        {
            path: "/profile-change-password",
            name: "profile-change-password",
            component: () => import("./views/profile/ProfileChangePassword.vue"),
            meta: {
                requiresAuth: true
            }
        },
        {
            path: "/send-error",
            name: "/send-error",
            component: () => import("./views/profile/SendError.vue"),
            meta: {
                requiresAuth: true
            }
        },
        {
            path: "/teacher-login",
            name: "teacher-login",
            component: () => import("./views/login/login.vue")
        },
        {
            path: "/groups",
            name: "groups",
            component: () => import("./views/groups/index.vue"),
            meta: {
                requiresAuth: true
            }
        },
        {
            path: "/teachers",
            name: "teachers",
            component: () => import("./views/teachers/index.vue"),
            meta: {
                requiresAuth: true
            }
        },
        {
            path: "/employees",
            name: "employees",
            component: () => import("./views/employees/index.vue"),
            meta: {
                requiresAuth: true
            }
        },
        {
            path: "/employees-to-day",
            name: "employees-to-day",
            component: () => import("./views/employees_to_day/index.vue"),
            meta: {
                requiresAuth: true
            }
        },
        {
            path: "/childs",
            name: "children",
            component: () => import("./views/children/index.vue"),
            meta: {
                requiresAuth: true
            }
        },
        {
            path: "/stats",
            name: "stats",
            component: () => import("./views/stats/index.vue"),
            meta: {
                requiresAuth: true
            }
        },
        {
            path: "/dashboard",
            name: "dashboard",
            component: () => import("./views/dashboard/index.vue"),
            meta: {
                requiresAuth: true
            }
        },
        {
            path: "/import-tarify",
            name: "import-tarify",
            component: () => import("./views/tarify/index.vue"),
            meta: {
                requiresAuth: true
            }
        },
        {
            path: "/privacy",
            name: "privacy",
            component: () => import("./views/privacy/index.vue"),
            meta: {
                requiresAuth: false
            }
        },
        {
            path: "/report-problems",
            name: "report-problems",
            component: () => import("./views/errors/index.vue"),
            meta: {
                requiresAuth: false
            }
        },
        {
            path: "/pdf-to-csv",
            name: "pdf-to-csv",
            component: () => import("./views/pdf-to-csv/index.vue"),
            meta: {
                requiresAuth: false
            }
        },
        {
            path: "/login/:user_id?",
            name: "login",
            component: () => import("./views/login/index.vue"),
            meta: {
                requiresAuth: false
            }
        },
        {
            path: "/registration",
            name: "registration",
            component: () => import("./views/registration/index.vue"),
            meta: {
                requiresAuth: false
            }
        },
        {
            path: "/analytic-users-registration",
            name: "analytic-registration",
            component: () => import("./views/registration/TableAnalyticUser.vue"),
            meta: {
                requiresAuth: false
            }
        },
        {
            path: "/verify/user/:id",
            name: "verify",
            props: true,
            component: () => import("./views/verify/index.vue")
        },
        // { 
        //   path: '/reset-password', 
        //   name: 'reset-password', 
        //   component: import("./views/reset/index.vue"), 
        //   meta: { 
        //     requiresAuth: false 
        //   } 
        // },
        { 
          path: '/reset-password/:token', 
          name: 'reset-password', 
          component: () =>import("./views/reset/index.vue"), 
          meta: { 
            requiresAuth: false 
          } 
        },
        {
            path: "/forgot-password",
            name: "forgot",
            component: () => import("./views/forgot/index.vue")
        },
        {
            path: "/admin-login",
            name: "admin-login",
            component: () => import("./views/admin/login/index.vue")
        },
        {
            path: "/admin-organizations",
            name: "admin-organizations",
            component: () => import("./views/admin/organizations/index.vue"),
            meta: {
                requiresAuth: true
            }
        },
        {
            path: "/admin-questions",
            name: "admin-questions",
            component: () => import("./views/admin/questions/index.vue"),
            meta: {
                requiresAuth: true
            }
        },
        {
            path: "/admin-managers",
            name: "admin-managers",
            component: () => import("./views/admin/managers/index.vue"),
            meta: {
                requiresAuth: true
            }
        },
        {
          path: '/:catchAll(.*)', 
          component: () => import("./views/errors/NotFound.vue"),
        },
    ]
});

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (store.getters.user) {
            if (store.getters.user.role == 2) {
                if ( to.matched[0].path == "/profile-change-password" || to.matched[0].path == "/login" || to.matched[0].path == "/dashboard" || to.matched[0].path == "/send-error") {
                    next()
                    return
                } else {
                    next("/dashboard");
                    return;
                }
            } else if ( store.getters.user.role == 0 ){
                if ( to.matched[0].path == "/dashboard") {
                    next("/")
                    return
                } else {
                    next();
                    return;
                }
            } else if (store.getters.user.role == 10) {
                if ( to.matched[0].path == "/admin-organizations" || to.matched[0].path == "/admin-managers" || to.matched[0].path == "/admin-questions" || to.matched[0].path == "/admin-teachers") {
                    next()
                    return
                } else {
                    next("/admin-organizations");
                    return;
                }
            }
        }
        next("/login");
    } else {
        if (store.getters.user) {
            if ( to.matched[0].path == "/registration" || to.matched[0].path == "/login" ) {
                if (store.getters.user.role == 2) {
                    next("/dashboard");
                    return;
                } else {
                    next("/");
                    return;
                } 
            } else {
                next();
            }
        } else {
            next();
        }
    }
});

export default router;
